-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2019 at 05:40 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cj_coinbase_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

-- CREATE TABLE IF NOT EXISTS `admins` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `first_name` varchar(255) DEFAULT NULL,
--   `last_name` varchar(255) DEFAULT NULL,
--   `password` varchar(255) DEFAULT NULL,
--   `email` varchar(255) DEFAULT NULL,
--   `referral_id` int(11) DEFAULT NULL,
--   `user_name` varchar(255) DEFAULT NULL,
--   `dob` datetime DEFAULT NULL,
--   `address` text,
--   `contact_no` varchar(255) DEFAULT NULL,
--   `city` varchar(255) DEFAULT NULL,
--   `state` varchar(255) DEFAULT NULL,
--   `country_id` int(11) DEFAULT NULL,
--   `postal_code` varchar(255) DEFAULT NULL,
--   `about_me` text,
--   `image` varchar(255) DEFAULT NULL,
--   `identity_proof` varchar(255) DEFAULT NULL,
--   `status` int(11) DEFAULT NULL,
--   `activation_key` varchar(255) DEFAULT NULL,
--   `createdAt` datetime NOT NULL,
--   `updatedAt` datetime NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `answers`
-- --

-- CREATE TABLE IF NOT EXISTS `answers` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `question_id` int(11) DEFAULT NULL,
--   `option_id` int(11) DEFAULT NULL,
--   `createdAt` datetime NOT NULL,
--   `updatedAt` datetime NOT NULL,
--   `user_id` int(11) NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=186 ;

-- --
-- -- Dumping data for table `answers`
-- --

-- INSERT INTO `answers` (`id`, `question_id`, `option_id`, `createdAt`, `updatedAt`, `user_id`) VALUES
-- (134, 1, 1, '2018-05-08 12:58:49', '2018-05-08 12:58:49', 50),
-- (135, 2, 7, '2018-05-08 12:58:49', '2018-05-08 12:58:49', 50),
-- (136, 3, 11, '2018-05-08 12:58:49', '2018-05-08 12:58:49', 50),
-- (137, 4, 13, '2018-05-08 12:58:49', '2018-05-08 12:58:49', 50),
-- (178, 1, 3, '2018-07-25 10:53:13', '2018-07-25 10:53:13', 1),
-- (179, 2, 7, '2018-07-25 10:53:13', '2018-07-25 10:53:13', 1),
-- (180, 3, 10, '2018-07-25 10:53:13', '2018-07-25 10:53:13', 1),
-- (181, 4, 12, '2018-07-25 10:53:13', '2018-07-25 10:53:13', 1),
-- (182, 1, 3, '2018-07-25 12:16:58', '2018-07-25 12:16:58', 58),
-- (183, 2, 8, '2018-07-25 12:16:58', '2018-07-25 12:16:58', 58),
-- (184, 3, 11, '2018-07-25 12:16:58', '2018-07-25 12:16:58', 58),
-- (185, 4, 14, '2018-07-25 12:16:58', '2018-07-25 12:16:58', 58);

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `authors`
-- --

-- CREATE TABLE IF NOT EXISTS `authors` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `author_name` varchar(255) DEFAULT NULL,
--   `author_bio` text,
--   `author_image` varchar(255) DEFAULT NULL,
--   `createdAt` datetime NOT NULL,
--   `updatedAt` datetime NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

-- --
-- -- Dumping data for table `authors`
-- --

-- INSERT INTO `authors` (`id`, `author_name`, `author_bio`, `author_image`, `createdAt`, `updatedAt`) VALUES
-- (1, 'Jhon Doe 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta sit, expedita labore, quaerat exercitationem provident voluptatem eveniet possimus odit repellendus ipsa nemo, reiciendis aliquam ab neque pariatur ut explicabo necessitatibus! Blanditiis, nesciunt sed aliquam quam distinctio perspiciatis soluta in molestiae atque. Eius officia itaque consequuntur tempore, temporibus neque blanditiis velit.2', '20-1533213982376.jpg', '2018-08-02 12:21:00', '2018-08-02 12:46:34'),
-- (11, 'Tamashree', 'Tamashree Bio', '20-1533205911136.jpg', '2018-08-02 10:31:51', '2018-08-02 10:31:51'),
-- (13, 'Nilesh Sanyal', 'Nilesh Bio Test', '20-1533213297579.jpg', '2018-08-02 12:34:57', '2018-08-02 12:47:58'),
-- (14, 'Nasir', 'Nasir Bio', '20-1533214029211.jpg', '2018-08-02 12:47:09', '2018-08-02 12:47:09');

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `bank_details`
-- --

-- CREATE TABLE IF NOT EXISTS `bank_details` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `user_id` varchar(255) DEFAULT NULL,
--   `acc_holder_name` varchar(255) DEFAULT NULL,
--   `bank_name` varchar(255) DEFAULT NULL,
--   `bank_address` varchar(255) DEFAULT NULL,
--   `acc_no` varchar(255) DEFAULT NULL,
--   `swift_code` varchar(255) DEFAULT NULL,
--   `branch_no` varchar(255) DEFAULT NULL,
--   `institution_no` varchar(255) DEFAULT NULL,
--   `createdAt` datetime NOT NULL,
--   `updatedAt` datetime NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --
-- -- Dumping data for table `bank_details`
-- --

-- INSERT INTO `bank_details` (`id`, `user_id`, `acc_holder_name`, `bank_name`, `bank_address`, `acc_no`, `swift_code`, `branch_no`, `institution_no`, `createdAt`, `updatedAt`) VALUES
-- (1, '1', 'Nilesh', 'HSBC', 'HSBC Test Address', '123', '456', '789', '666', '2018-05-28 11:25:20', '2018-05-28 11:25:20'),
-- (2, '1', 'hyhyh', 'fggft', 'fgbbb', '1111111', 'fefef', 'rfgvr', 'vgfgvfvg', '2018-07-03 12:04:21', '2018-07-03 12:04:21'),
-- (3, '1', 'Nilesh Sanyal', 'West Bank', '270 Park Ave, New York', '1111111', '4562', '328', '324', '2018-07-25 11:17:03', '2018-07-25 11:17:03'),
-- (4, '1', 'Tamashree Dey', 'ssa', 'Dumdum', '11111111111111111', '345555', '34555', '456', '2018-11-09 07:03:54', '2018-11-09 07:03:54');

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `blog_categories`
-- --

-- CREATE TABLE IF NOT EXISTS `blog_categories` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `category_name` varchar(255) DEFAULT NULL,
--   `status` int(11) DEFAULT NULL COMMENT '1: Active, 0: Inactive',
--   `createdAt` datetime NOT NULL,
--   `updatedAt` datetime NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

-- --
-- -- Dumping data for table `blog_categories`
-- --

-- INSERT INTO `blog_categories` (`id`, `category_name`, `status`, `createdAt`, `updatedAt`) VALUES
-- (1, 'Featured', 1, '2018-06-27 06:21:54', '2018-07-26 08:14:54'),
-- (3, 'Latest News', 1, '2018-06-27 07:10:09', '2018-06-28 05:55:14'),
-- (17, 'Miscellaneous', 1, '2018-06-27 07:20:09', '2018-06-27 07:20:09');

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `blog_posts`
-- --

-- CREATE TABLE IF NOT EXISTS `blog_posts` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `post_title` varchar(255) DEFAULT NULL,
--   `post_description` text,
--   `post_slug` varchar(255) DEFAULT NULL,
--   `featured_image` varchar(255) DEFAULT NULL,
--   `meta_title` varchar(255) DEFAULT NULL,
--   `meta_description` text,
--   `post_author` varchar(255) DEFAULT NULL,
--   `createdAt` datetime NOT NULL,
--   `updatedAt` datetime NOT NULL,
--   `author_id` int(11) DEFAULT NULL,
--   `post_category_id` int(11) DEFAULT NULL,
--   `status` int(11) DEFAULT NULL COMMENT '1: Active, 0: Inactive',
--   `meta_keywords` text,
--   `post_author_description` text,
--   `post_author_image` varchar(255) DEFAULT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

-- --
-- -- Dumping data for table `blog_posts`
-- --

-- INSERT INTO `blog_posts` (`id`, `post_title`, `post_description`, `post_slug`, `featured_image`, `meta_title`, `meta_description`, `post_author`, `createdAt`, `updatedAt`, `author_id`, `post_category_id`, `status`, `meta_keywords`, `post_author_description`, `post_author_image`) VALUES
-- (19, 'Chamath Palihapitiya, Billionaire Venture Capitalist and Owner of the Golden State Warriors on Bitcoin', '<p><img alt="" class="alignright size-medium wp-image-3907" src="https://coinjolt.com/wp-content/uploads/2018/06/chamath-palihapitiya-soho-conference-300x300.png" style="float:right; height:300px; width:300px" />&quot;I&#39;m a huge fan of Bitcoin, I&rsquo;m very, very long Bitcoin. Particularly in the developing markets is huge.&quot; &ndash; Chamath Palihapitiya We sat down with billionaire Chamath Palihaptiya, owner of the Golden State Warriors and billionaire investor known for his early position in Facebook and founder of Social Capital which manages $2.2 billion dollars in assets. Palihaptiya, pronounced (pah-le-hap-pe-tee-ah) went on saying, &ldquo;If you don&rsquo;t think this thing is going to rip, when Brazil goes through a devaluation, when India Rupee continues to get crushed, when you have all of this money trying to get in and out of these countries where there&rsquo;s massive political instability or monetary or financial instability, you are being na&iuml;ve. It doesn&rsquo;t matter what happens in the United States, it doesn&rsquo;t matter what happens in Japan or the EU. It doesn&rsquo;t matter. It matters what happens in Argentina, Venezuala, Kenya, Brazil, India, Russia, China and when you look at where all of the activity is, it&rsquo;s in all of those markets. This will be born out of a people&rsquo;s desire to have untethered access to capital. Bitcoin and things like it, is equivalent to the red pill. We are entering a world of unchartered water. It is a huge deal. It&rsquo;s a huge, huge deal. What you&rsquo;re talking about right now, for the next 3 to 5 years, it is an unbelievably better store of value. It is gold 2.0. In a portfolio, I think that something like Bitcoin is really important. Why? Because it is not correlated to the rest of the market. The reality is things change. It should be what is the trail of breadcrumbs that a smart, introspective, intellectual curious investor says, is oh, I should pay attention. And when one goes in and actually reads and understand the technical logic behind Bitcoin and the actual fundamental product it&rsquo;s creating and the value it offers, at the end of the day, here&rsquo;s what you have to believe: In a world of autocracy, currency curbs, quantitative easing, all forms of central bank created or implemented value destruction, you need a fundamentally distributed store of value. Bitcoin is the only one. Bitcoin does not exist as a company, it is not an entity, it is already been a thing in which the genie has been let out of the bottle. You cannot get it back in.&rdquo; If you enjoyed reading this post, <a href="/sign-up">click here to sign up and start buying or investing in digital currencies instantly with ZERO fees.</a></p>\r\n', 'chamath-palihapitiya-on-bitcoin', '20-1530882392335.png', 'meta test', 'meta description test', 'Roy Martin', '2018-06-27 09:02:39', '2018-07-06 13:06:32', NULL, 3, 1, 'Test', NULL, NULL),
-- (21, 'TruePlay - A Blockchain Platform For Gambling Projects', '<p><img alt="" class="alignright wp-image-3930" src="https://coinjolt.com/wp-content/uploads/2018/06/initial-coin-offering-wheel-1.png" style="height:378px; width:341px" />Ilya Gnoensky, CEO of <a href="http://trueplay-a-blockchain-platform-for-gambling-projects">TruePlay</a> says, &quot;ICO&#39;s are the new big thing. So many projects to explore, so many opportunities to invest in, so how does one know which one to pick? At least what industry to look into at.&quot; When you think about online gambling, what usually comes to mind? Something shady, dishonest, crooked, even a scam right? That&#39;s what half of the internet users think and it&#39;s really bad for the industry. Players are afraid of gambling and casinos lose a lot of money. <strong>TruePlay</strong> has been working in this industry for 15 years now, and they know it inside out. They say they&#39;re one hundred percent sure that if it becomes more transparent, everyone is going to benefit. The players, casino owners, affiliates and game content providers. It&#39;s going to be a clear win for everyone. The online gambling market is growing at a tremendous rate and in the next 5 years it will increase two fold, and what else is progressing so fast? Blockchain. By 2022 it will increase 5 fold. So what is <em>TruePay</em>? A unique software solution that uses blockchain as it&#39;s core technology and they assure that it&#39;s going to change the online gambling market and make it trustable, transparent and more profitable for everyone. Now it&#39;s not just another blockchain casino, think of it as, all in one, full integrated state of the art platform that supports gambling projects everywhere, it&#39;s like an ecosystem that has in it hundreds of verifiable and trustable online casinos. And millions of loyal and satisfied players all over the world. <u>TruePlay</u> is a real company with 15 years of experience in online gambling, they&#39;ve created casinos from scratch, developed software and payment systems, provided technical support and helped attract customers. That&#39;s what makes TruePlay uniquely qualified to do this really well.</p>\r\n\r\n<h2>TruePlay &ndash; A Blockhain Platform For Gambling Projects</h2>\r\n\r\n<p><img alt="" class="aligncenter size-full wp-image-3931" src="https://coinjolt.com/wp-content/uploads/2018/06/trueplay-exchange.jpg" style="height:683px; width:1024px" />Solutions:</p>\r\n\r\n<ol>\r\n	<li>Receiving payments in different cryptocurrencies. Utility token Tplay is a way into the cryptocurrency community and acceptance of cryptocurrency in any gambling project.</li>\r\n	<li>KYC/AML participants verification. Verified gamers stay logged in all the projects.</li>\r\n	<li>Transparency in all transactions. Transparency in all transactions and control over finances of all gamers brought in.</li>\r\n	<li>Games honesty control based on the blockchain technology. TruePlay in games honestly control on the blockchain technology. All games results are generated before the start and their hash is saved on the blockchain. After the game is played, gamers can verify has results with those on the blockchain.</li>\r\n	<li>Turn key solutions for all projects, including that of the platform itself. TruePlay also offers turnkey solutions with integrated games. TPlay honestly controls. Gamingtec alsready launched a fully functional project based on the TruePlay platform. FairPlay.io.</li>\r\n	<li>Licensing and games from top international providers.</li>\r\n</ol>\r\n\r\n<p>If you enjoyed reading this post, <a href="/sign-up">click here to sign up and start buying or investing in digital currencies instantly with ZERO fees.</a></p>\r\n', 'trueplay-a-blockchain-platform-for-gambling-projects', '', 'test', 'test meta description', 'Jane Doe', '2018-06-28 05:51:30', '2018-07-06 13:03:49', NULL, 1, 1, 'Test', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of theContrary to popular belief, Lorem Ipsum is not simply random text. ', NULL),
-- (22, 'If You Aren''t Using Blockchain, You Won''t Exist Says FedEx CEO', '<p><img alt="" class="alignright size-medium wp-image-3585" src="https://coinjolt.com/wp-content/uploads/2018/05/fedex-ceo-blockchain-technology-300x300.png" style="height:300px; width:300px" />According to Coindesk, CEO of FedEx Fred Smith stated that &ldquo;Blockchain has the potential to completely revolutionize what&rsquo;s across the border,&rdquo; at CoinDesk&rsquo;s Consensus 2018 in New York. Smith stated that an issue in the logistics and transportation industry is the &ldquo;massive amount of friction&rdquo; as a result of wildly different standards, regulations, and terminologies in different countries. He also said: <em>&ldquo;For cross-border shipments, &lsquo;trust&rsquo; is legal requirement for every transaction. What blockchain has is a potential for the first time ever to make the information available for everybody.&rdquo;</em> FedEx&rsquo;s CIO and executive vice president of information services said that FedEx will explore the use of blockchain in the freight industry. He said: <em>&ldquo;We move easily 12 million shipments a day and that more than doubles during the peak seasons. While we absolutely believe this technology is going to scale, right now it makes sense for us to do this in our freight world&hellip;The application of these custody chains &hellip; is so critical to the information aspect. We&rsquo;re operating on this plane between the physical world and the digital world.&rdquo;</em> On FedEx embracing blockchain technology, Smith concluded: <em>&ldquo;If you are not operating at the edge of new technologies, you will surely be disrupted. If you are not willing to embrace new technologies like internet of things and blockchain to face those new threats, you are, maybe subtly, at some point &hellip; going to extinction.&rdquo;</em> <strong>Deloitte&rsquo;s Head Of Blockchain Leaves To Build Ethereum-based Supply Chain</strong> According to Forbes, Eric Piscini helped Deloitte&rsquo;s global blockchain practice into a $50 million operation and grew the account firm&rsquo;s blockchain team from three people in 2012 to 1,200 people today. Now, Piscini has decided to leave Deloitte to work on startup Citizens Reserve which is attempting to raise $300 million to move the world&rsquo;s fragmented supply chain networks to a blockchain. In an effort to comply with regulations, Citizens Reserve&rsquo;s $300 million fundraising effort is being broken up into three separate parts that could be considered a mix between a traditional fundraising round, an Initial Public Offering (IPO) and an ICO. The first part is a $2.3 million friends and family round in the form of convertible notes, and the rest consists of selling tokens. Piscini says Citizens Reserve is working with three companies who are currently providing the defense industry supply chain software that are looking to &ldquo;tokenize their platform&rdquo;. Piscini said: &ldquo;We have decided to start with the defense industry because we have relationships with key participants in the defense industry, and it&rsquo;s a very broken supply chain across the globe&hellip;we have market participants from that industry that are being onboarded into the consortium now.&rdquo; <strong>HSBC Performed The World&rsquo;s First Commercially Viable Trade Finance Transaction Using Blockchain</strong> According to CNBC, HSBC claims that it had performed the world&rsquo;s first commercially viable trade finance transaction using blockchain technology. The trade finance transaction involved a bulk shipment of soybeans from Argentina to Malaysia. A letter of credit was issued from HSBC to Dutch lender ING. A Letter of credit is issued by one bank to another to guarantee that a payment will be received by a seller under a set of conditions. Traditionally this process would take a considerable amount of time and paperwork between the parties involved. One potential that blockchain technology has is that it can cut down on time and costs for issuing letters of credit. Vivek Ramachandran, head of growth and innovation at HSBC, said of the transaction: &ldquo;The need for paper reconciliation is removed because all parties are linked on the platform and updates are instantaneous&hellip;the quick turnaround could mean unlocking liquidity for businesses.&rdquo; HSBC and ING said the transaction took 24 hours, as opposed to 5&ndash;10 days transactions like these normally take. If you enjoyed reading this post, <a href="/sign-up">click here to sign up and start buying or investing in digital currencies instantly with ZERO fees.</a></p>\r\n', 'fedex-ceo-talks-blockchain-2', '20-1530882245649.jpg', 'test', 'test', 'Jhon Doe', '2018-09-05 06:07:00', '2018-09-03 11:48:21', 1, 3, 1, 'Test', 'fedex-ceo-talks-blockchain', NULL),
-- (33, 'Post Title', '<p>Post Description</p>\r\n', 'new-testing', '20-1533207042809.jpg', 'Meta Title', 'Meta Description', NULL, '2018-09-05 10:50:00', '2018-09-03 11:47:32', 11, 3, 1, 'Meta Keywords', NULL, NULL),
-- (34, 'Demo2', '<p>Demo Post Description2</p>\r\n', 'testing', '20-1533208247226.jpg', 'Meta Title2', 'Meta Description2', NULL, '2018-08-02 11:00:01', '2018-08-02 11:13:54', 1, 17, 1, 'Meta Keywords2', NULL, NULL),
-- (35, 'New Post Demo', '<p>New Post Desc</p>\r\n', 'new-post-demo', '20-1533214228752.jpg', 'meta title', 'meta description', NULL, '2018-08-02 12:50:28', '2018-08-02 12:52:33', 14, 3, 1, 'meta keywords', NULL, NULL),
-- (36, 'Testing', '<p>bhhjhjhjkh</p>\r\n', 'fedex-ceo-talks-blockchain', '20-1533214513861.jpg', 'rytyt', 'tytyt', NULL, '2018-08-02 12:55:13', '2018-08-03 09:33:12', 11, 3, 1, 'fhgjh', NULL, NULL),
-- (38, 'dsdsddsdsa', '<p>gdfdsd</p>\r\n', 'demo-post2', '', 'as', 'aaa', NULL, '2018-08-03 09:00:51', '2018-08-03 09:20:45', 13, 1, 1, 'aaa', NULL, NULL),
-- (39, 'fedf', '<p>fedf</p>\r\n', 'gambling-projects', '20-1533290269721.jpg', 'dcc', 'dc', NULL, '2018-11-07 09:57:00', '2018-08-10 08:01:55', 13, 3, 1, 'dswdxsw', NULL, NULL),
-- (40, 'test post nil', '<p>test post nil</p>\r\n', 'test-post-nil', '', 'test nil', 'test post nil', NULL, '2018-08-03 12:09:07', '2018-08-03 12:09:26', 13, 1, 1, 'test post nil', NULL, NULL),
-- (45, 'ttttttttt', '<p>ddddddd</p>\r\n', 'szwzwasz', '20-1533713873558.jpg', 'asza', 'sza', NULL, '2018-08-09 08:12:00', '2018-08-08 08:13:07', 11, 1, 1, 'sx', NULL, NULL),
-- (46, 'tsssssssss', '<p>jlkjlo</p>\r\n', 'hkjkj', '20-1533714048854.jpg', 'xsx', 'xsx', NULL, '2018-08-16 09:42:00', '2018-08-08 07:41:46', 14, 1, 1, 'xsx', NULL, NULL),
-- (47, 'New Post', '<p>New Post&nbsp;New Post&nbsp;New Post</p>\r\n', 'new-post', '20-1535971016649.jpg', 'New Post', 'New Post', NULL, '2018-09-03 11:36:00', '2018-09-03 11:48:59', 11, 1, 1, 'New Post', NULL, NULL);

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `cms_about_us`
-- --

-- CREATE TABLE IF NOT EXISTS `cms_about_us` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `about_us_header_desc` text,
--   `about_us_header_image` varchar(255) DEFAULT NULL,
--   `about_us_description` text,
--   `createdAt` datetime NOT NULL,
--   `updatedAt` datetime NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --
-- -- Dumping data for table `cms_about_us`
-- --

-- INSERT INTO `cms_about_us` (`id`, `about_us_header_desc`, `about_us_header_image`, `about_us_description`, `createdAt`, `updatedAt`) VALUES
-- (1, ' About Us Details.', '20-1531465715170.png', '<div class="about-section">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-12">\r\n<h4>About Us</h4>\r\n\r\n<p>Brought to you by a group of the first few Bitcoin investors since 2009, as well as leading blockchain developers with over 10 years of experience in portfolio management, now focused entirely on digital currency or cryptocurrency.</p>\r\n\r\n<h4>Our Mission</h4>\r\n\r\n<p>Our mission is to be able to deliver the highest returns in comparison to any other asset class in a constantly developing new sector. Generate at a bare minimum 200% annual returns with our managed cryptocurrency portfolio available to both institutional and individual investors. Capitalize off a brand new hyper adopting technology known as blockchain.</p>\r\n\r\n<p>Invest in a managed cryptocurrency portfolio with a proven track record of providing unparalleled returns. We utilize marginalized trading in established capitalized markets with high volatility, utilizing real-time market price tracking instruments and actively trade across a variety of high capitalized assets.</p>\r\n\r\n<p>In addition, we buy tokens on promising initial coin offerings backed by reputable companies with a strong corporate standing and previous recorded experience and history of success resulting in incredible returns.</p>\r\n\r\n<h4>Utilizing Leveraged Positions</h4>\r\n\r\n<p>By investing in our managed cryptocurrency portfolio, we&rsquo;re able to execute trades with leveraged positions on users behalf, significantly pushing the leverage on positions through collective resources and marginalized trading.</p>\r\n\r\n<p>An example would be a $100,000 investment could be leveraged up to 20x for a $2,000,000 capital investment in Bitcoin.</p>\r\n\r\n<p>These margins become extremely profitable, especially during times of high volatility.</p>\r\n\r\n<p>For example, as an individual investor, if you were to invest $100,000 at $10,000 per coin, you would only be able to purchase 10 Bitcoins.</p>\r\n\r\n<p>In contrast, if you decide to invest in our managed cryptocurrency portfolio, that same $100,000 could be leveraged up to 20 times for a $2,000,000 position across a variety of digital currencies, or 20 Bitcoins.</p>\r\n\r\n<p>As Bitcoin prices increase, at $50,000 per coin, a small investment of $100,000 would generate a return from 20 Bitcoins, a total $10,000,000 market value, as opposed to with 10 Bitcoins worth $500,000.</p>\r\n\r\n<p>The difference in profit is $400,000 from a $100,000 investment for 10 Bitcoins as an individual investor as opposed to $8,000,000 for the same $100,000 for 20 Bitcoins. Our fees are 50% of profits. Settlements are paid out annually in either fiat or cryptocurrency, as per user preference as selected through user dashboard.</p>\r\n\r\n<p>One of the benefits of investing in our managed cryptocurrency portfolio is that any losses from all investments made towards our managed cryptocurrency portfolio are covered by our capital reserves. The returns we&rsquo;re seeing are so incredible, we&rsquo;re willing to take all of the risks.</p>\r\n\r\n<h4>Historical Trend and Potential Future Gains</h4>\r\n\r\n<p>In 2011, Bitcoin had a market value per coin of $0.30. If you had purchased $100 worth of just Bitcoin, you would have 333 of them, which at a current market value between $10,000 &ndash; $20,000 would be equivalent to the value between $3,330,000 &ndash; $6,660,000.</p>\r\n\r\n<p>Our analysts forecast the value of each Bitcoin reaching upwards of $1,000,000 per coin by 2021. Despite what you hear, the steady trend of Bitcoin has been exponential in the long-term perspective, this is due to the fundamental technology and underlying principles of what it stands for. The increasing interest from institutional investors, high net worth individuals, government adoption as well rapid expansion in blockchain technology across multiple high capitalized industries like financial services, eCommerce, delivery services, gaming, virtual reality and the overall global economy is bound to create many multiples in value for the price of each coin. In addition to being increasingly scarce as an asset.</p>\r\n\r\n<p>There will only ever be 21,000,000 Bitcoins created, all of which are expected to be digitally mined by 2040. Investing in our portfolio allows you to secure a stake with exceptional odds in your favor</p>\r\n\r\n<h4>Additional Platform Features</h4>\r\n\r\n<p>Get instant access to leading market instruments, market research tools, latest news and current prices across a variety of cryptocurrency.</p>\r\n\r\n<h4>Safety and Security</h4>\r\n\r\n<p>We uphold the highest standard of security and enable multiple security protocols to ensure confidentiality and safety of assets.</p>\r\n\r\n<p>THanks,</p>\r\n\r\n<p>Sobhan Das.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', '2018-06-12 10:05:38', '2018-07-16 05:28:12');

-- --------------------------------------------------------

--
-- Table structure for table `cms_home_pages`
--
SET GLOBAL innodb_default_row_format='dynamic';
SET SESSION innodb_strict_mode=OFF;

CREATE TABLE IF NOT EXISTS `cms_home_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `home_page_banner_image` varchar(255) DEFAULT NULL,
  `how_it_works_image` varchar(255) DEFAULT NULL,
  `how_is_works_description` text,
  `hot_wallet_image` varchar(255) DEFAULT NULL,
  `hot_wallet_desc` text,
  `cold_wallet_image` varchar(255) DEFAULT NULL,
  `cold_wallet_desc` text,
  `video_upload` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `how_is_works_reg_description` text,
  `how_is_works_deposit_funds_description` text,
  `how_is_works_safe_and_secure_description` text,
  `industry_leading_div1_image` varchar(255) DEFAULT NULL,
  `industry_leading_div2_image` varchar(255) DEFAULT NULL,
  `industry_leading_div3_image` varchar(255) DEFAULT NULL,
  `industry_leading_div4_image` varchar(255) DEFAULT NULL,
  `industry_leading_div5_image` varchar(255) DEFAULT NULL,
  `industry_leading_div6_image` varchar(255) DEFAULT NULL,
  `div1_heading` varchar(255) DEFAULT NULL,
  `div2_heading` varchar(255) DEFAULT NULL,
  `div3_heading` varchar(255) DEFAULT NULL,
  `div4_heading` varchar(255) DEFAULT NULL,
  `div5_heading` varchar(255) DEFAULT NULL,
  `div6_heading` varchar(255) DEFAULT NULL,
  `div1_desc` text,
  `div2_desc` text,
  `div3_desc` text,
  `div4_desc` text,
  `div5_desc` text,
  `div6_desc` text,
  `buy_sell_div1_image` varchar(255) DEFAULT NULL,
  `buy_sell_div3_image` varchar(255) DEFAULT NULL,
  `buy_sell_div2_image` varchar(255) DEFAULT NULL,
  `buy_sell_div4_image` varchar(255) DEFAULT NULL,
  `buy_sell_div1_heading` varchar(255) DEFAULT NULL,
  `buy_sell_div2_heading` varchar(255) DEFAULT NULL,
  `buy_sell_div3_heading` varchar(255) DEFAULT NULL,
  `buy_sell_div4_heading` varchar(255) DEFAULT NULL,
  `buy_sell_div1_desc` text,
  `buy_sell_div2_desc` text,
  `buy_sell_div3_desc` text,
  `buy_sell_div4_desc` text,
  `risk_disclousure_div1_image` varchar(255) DEFAULT NULL,
  `risk_disclousure_div2_image` varchar(255) DEFAULT NULL,
  `risk_disclousure_div1_heading` varchar(255) DEFAULT NULL,
  `risk_disclousure_div3_image` varchar(255) DEFAULT NULL,
  `risk_disclousure_div2_heading` varchar(255) DEFAULT NULL,
  `risk_disclousure_div3_heading` varchar(255) DEFAULT NULL,
  `risk_disclousure_div1_desc` varchar(255) DEFAULT NULL,
  `risk_disclousure_div2_desc` varchar(255) DEFAULT NULL,
  `risk_disclousure_div3_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cms_home_pages`
--

INSERT INTO `cms_home_pages` (`id`, `home_page_banner_image`, `how_it_works_image`, `how_is_works_description`, `hot_wallet_image`, `hot_wallet_desc`, `cold_wallet_image`, `cold_wallet_desc`, `video_upload`, `createdAt`, `updatedAt`, `how_is_works_reg_description`, `how_is_works_deposit_funds_description`, `how_is_works_safe_and_secure_description`, `industry_leading_div1_image`, `industry_leading_div2_image`, `industry_leading_div3_image`, `industry_leading_div4_image`, `industry_leading_div5_image`, `industry_leading_div6_image`, `div1_heading`, `div2_heading`, `div3_heading`, `div4_heading`, `div5_heading`, `div6_heading`, `div1_desc`, `div2_desc`, `div3_desc`, `div4_desc`, `div5_desc`, `div6_desc`, `buy_sell_div1_image`, `buy_sell_div3_image`, `buy_sell_div2_image`, `buy_sell_div4_image`, `buy_sell_div1_heading`, `buy_sell_div2_heading`, `buy_sell_div3_heading`, `buy_sell_div4_heading`, `buy_sell_div1_desc`, `buy_sell_div2_desc`, `buy_sell_div3_desc`, `buy_sell_div4_desc`, `risk_disclousure_div1_image`, `risk_disclousure_div2_image`, `risk_disclousure_div1_heading`, `risk_disclousure_div3_image`, `risk_disclousure_div2_heading`, `risk_disclousure_div3_heading`, `risk_disclousure_div1_desc`, `risk_disclousure_div2_desc`, `risk_disclousure_div3_desc`) VALUES
(1, '20-1532507950464.png', '20-1532089280948.png', NULL, '20-1534141317886.png', '<ul>\r\n	<li>Instantly connect to dozens of cryptocurreny networks to send and receive funds with the most secure wallet in the industry.</li>\r\n	<li>Actively monitored assets with accurately calculated market values.</li>\r\n	<li>Transfer funds to a cold wallet for secure offline storage.</li>\r\n	<li>Multiple enabled security protocols and automatic lockdown signals to prevent unauthorized access.</li>\r\n</ul>\r\n', '2934-1532433343017.png', '<ul>\r\n	<li>Secure and monitor assets offline to guarantee safety and prevent attempts to breach security.</li>\r\n	<li>Accumulate digital assets seamlessly through your back office.</li>\r\n	<li>Request to transfer digital currency to your Hot Wallet to readily engage in cryptocurrency.</li>\r\n</ul>\r\n', '20-1531746463715.mp4', '2018-07-16 13:07:43', '2018-08-13 06:21:59', '<p>Available for both individuals and institutions.</p>\r\n', '<p>All major credit cards,&nbsp;wire transfer, cryptocurrency. Deposits must be greater than $500 USD.</p>\r\n', '<p>Instant transactions and zero fees. A managed cryptocurrency portfolio with a projected 200% minimum annual returns.</p>\r\n', '2934-1532433343019.png', '20-1534141317889.png', '20-1534141317890.png', '20-1534141317891.png', '20-1534141317892.png', '20-1534141317895.png', 'Payment Options', 'Security Intelligence', 'Global Coverage', 'Advanced Reporting', 'New Standard of Returns', 'An Innovative Approach', '<p>Most popular methods: All major credit cards, wire transfer&nbsp;and cryptocurrency accepted.</p>\r\n', '<p>Protection against DDoS attacks, full data encryption.</p>\r\n', '<p>Providing services in 99%&nbsp;countries around the globe.</p>\r\n', '<p>Downloadable reports, real time balance and transaction history.</p>\r\n', '<p>Proprietary industry leading technology can make a significant difference with your investments.</p>\r\n', '<p>Connected to every major exchange our platform allows users and clients to buy and sell digital assets at guaranteed best prices.</p>\r\n', '20-1534141317896.png', '20-1534141317898.png', '20-1534141317897.png', '20-1534141317899.png', 'Access to Market Data', 'Getting Started Within Seconds', 'Security intelligence', 'Benefit as an Early Adopter', '<p>Get access to leading market data, latest news, tools and resources.</p>\r\n', '<p>You are only a few moments away from creating an account.</p>\r\n', '<p>Digital assets are safe and secure and user balances are insured by corporate reserves.</p>\r\n', '<p>Volatility means opportunity. Current market growth is expected to be at a 40x multiple.</p>\r\n', '20-1534141317900.png', '20-1534141317901.png', 'Fully Regulated', '20-1534141317902.png', 'Global Coverage', 'Confidentiality', '<p>Working with top government regulators and regulatory officials.</p>\r\n', '<p>The majority of assets are monitored and routinely stored in safe distributed, inaccessible storage vaults.</p>\r\n', '<p>We will never share your information Full account data encryption.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `cms_privacy_policies`
--

CREATE TABLE IF NOT EXISTS `cms_privacy_policies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `privacy_policy_header_image` varchar(255) DEFAULT NULL,
  `privacy_policy_header_desc` text,
  `privacy_policy_content` text,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cms_privacy_policies`
--

INSERT INTO `cms_privacy_policies` (`id`, `privacy_policy_header_image`, `privacy_policy_header_desc`, `privacy_policy_content`, `createdAt`, `updatedAt`) VALUES
(1, '', 'Privacy Policy', '<div class="about-section">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-12">\r\n<h4>COINJOLT WEBSITE PRIVACY POLICY</h4>\r\n\r\n<p>This Website Privacy Policy (&ldquo;Privacy Policy&rdquo;) is effective as of November 11, 2017. Last ammended by Ken Russell, Chief Executive Officer.</p>\r\n\r\n<p>CoinJolt.com (&ldquo;CoinJolt,&rdquo; &ldquo;we&rdquo; or &ldquo;us&rdquo;) respects your (&ldquo;you,&rdquo; &ldquo;your&rdquo; or &ldquo;User&rdquo;) concerns about privacy. This Privacy Policy describes the types of personal information we collect on CoinJolt.com, CoinJolt.com and other CoinJolt-owned websites (collectively, &ldquo;Website&rdquo;), how we may use the information and the purposes for which we may use it, with whom we may share it and the choices available to you. We also describe the measures we take to safeguard the information and tell you how to contact us about our privacy practices. This Privacy Policy covers only information collected through this Website. By visiting or submitting information to this Website, you are accepting and consenting to the provisions set forth in this Privacy Policy. If you do not agree to this Privacy Policy, please do not use this Website.</p>\r\n\r\n<h4>Information We Obtain</h4>\r\n\r\n<p>CoinJolt may obtain personal information through this Website, including:</p>\r\n\r\n<ul>\r\n	<li>Contact information, such as your name, postal address, e-mail address, and telephone number;</li>\r\n	<li>Username and password to access CoinJolt products and services online;</li>\r\n	<li>Information that you provide in the course of applying for a job with us, such as information about your education, work and military history, legal work eligibility status, and other information about you; and</li>\r\n	<li>Other personal information you voluntarily provide to us, such as your preferences or data provided through online forms.</li>\r\n</ul>\r\n\r\n<p>During your use of this Website, CoinJolt will also, itself or through a third party vendor, automatically collect technical and usage data pertaining to your device and/or Internet connection. Such data includes the number and frequency of your visits to this Website, information regarding websites accessed immediately before and after your visit to this Website, your Internet browser type, your preferences on this Website, your IP address and your device characteristics. We use this data to (1) remember your information so you will not have to re-enter it; (2) track and understand how you use and interact with the Website; (3) personalize your visit to the Website; (4) measure the usability of the Website and the effectiveness of our communications; and (5) otherwise manage and enhance the Website. This technical and usage data is obtained using &ldquo;cookies&rdquo; or other similar technologies. Cookies are small text files that websites send to your computer or other Internet-connected device to identify your browser and to store information or settings in the browser. Your browser may tell you how to be notified when you receive certain types of cookies or how to restrict or disable certain types of cookies. Please note, however, that without cookies you may not be able to use all of the features of our Website. For mobile devices, you can manage how your device and browser share certain device data by adjusting the privacy and security settings on your mobile device or browsers.</p>\r\n\r\n<p>We use third-party web analytics services on this Website, such as those of Google Analytics. The analytics providers that administer these services use technologies such as cookies, web server logs and web beacons to help us analyze how visitors use this Website. The information collected through these means (including IP addresses) is disclosed to these analytics providers, who use data to evaluate use of this Website. To learn more about Google Analytics and Wistia and how to opt out, please visit: <a href="https://support.google.com/analytics/answer/6004245" target="_blank">www.google.com/analytics/learn/privacy.html.</a></p>\r\n\r\n<p>We may combine information we collect from you with information we receive from third parties and use it for the purposes described in this Privacy Policy. We also may use information we obtain about you in other ways for which we provide specific notice at the time of collection.</p>\r\n\r\n<h4>How We Use the Information We Obtain</h4>\r\n\r\n<p>We may use the information we obtain about you to:</p>\r\n\r\n<ul>\r\n	<li>Communicate with you (and, where required by applicable law, we will obtain your explicit consent to do so);</li>\r\n	<li>Manage career opportunities at CoinJolt;</li>\r\n	<li>Personalize your experience on our Website;</li>\r\n	<li>Perform analytics (including market research, trend analysis, financial analysis, and anonymization of personal information);</li>\r\n	<li>Operate, evaluate, develop, manage and improve our business (including operating, administering, analyzing and improving our Website, products and services; developing new products and services; managing and evaluating the effectiveness of our communications</li>\r\n	<li>Maintain and enhance the safety and security of our Website and prevent misuse;</li>\r\n	<li>Protect against, identify and prevent fraud and other criminal activity, claims and other liabilities; and</li>\r\n	<li>Comply with and enforce applicable legal requirements, relevant industry standards and policies.</li>\r\n</ul>\r\n\r\n<p>In compliance with relevant law, we may retain information as needed or appropriate to serve our legitimate business purposes or as required by law.</p>\r\n\r\n<p>This Website is not designed to respond to &ldquo;do not track&rdquo; signals received from browsers.</p>\r\n\r\n<h4>Disclosures of Information</h4>\r\n\r\n<p>CoinJolt will not sell, rent or disclose your personal information to anyone except as follows: We share your personal information with our affiliates for the purposes identified in this Privacy Policy. We also share your personal information with agents and service providers. We do not authorize these agents or service providers to use or disclose the personal information except as necessary to perform services on our behalf or comply with legal requirements.</p>\r\n\r\n<p>We also may disclose personal information about you (1) if we are required to do so by law or legal process (such as a court order or subpoena); (2) in response to requests by government agencies, such as law enforcement authorities; (3) to establish, exercise or defend our legal rights; (4) when we believe disclosure is necessary or appropriate to prevent physical or other harm or financial loss; (5) in connection with an investigation of suspected or actual illegal activity; or (6) otherwise with your consent.</p>\r\n\r\n<p>We reserve the right to share or transfer any personal information we have about you in connection with a prospective or actual sale, merger, transfer or other reorganization of all or parts of our business (including in the event of a divestiture, restructuring, reorganization, dissolution or liquidation).</p>\r\n\r\n<h4>Your Choices</h4>\r\n\r\n<p>To opt out of receiving direct marketing communications, such as commercial emails, or to unsubscribe from our services, please contact us at support@coinjolt.com.</p>\r\n\r\n<p>You may have the right to request access to and correction of personal information that pertains to you. To exercise this right, contact us at support@coinjolt.com. We reserve the right to verify your identity or take other actions that we believe are appropriate.</p>\r\n\r\n<h4>For Residents of the European Economic Area</h4>\r\n\r\n<p>CoinJolt will store your personal information on servers that are located in Isle of Man or other jurisdictions outside the European Economic Area, which may not have the same data protection laws as the country from which you initially provided the information. Your use of the Website is evidence of consent to transfer your personal information to countries outside of the European Economic Area.</p>\r\n\r\n<p>If you are a resident of the EEA, you may request access to the personal information we maintain about you. You also may withdraw any consent you previously provided to us or object at any time on compelling legitimate grounds to the processing of your personal information, and we will apply your preferences going forward. If personal information you have submitted through this Website is no longer accurate, current, or complete, you may request that we update, amend, delete or block it to the extent required by law. Upon appropriate request, and where reasonably feasible, we will provide you with access to your personal information and the ability to update, amend, delete or block your personal information. We reserve the right to use personal information previously obtained to verify your identity or take other actions that we believe are appropriate. To the extent permitted by applicable law, a charge may apply before we provide you with a copy of any of your personal information that we maintain.</p>\r\n\r\n<h4>Security</h4>\r\n\r\n<p>CoinJolt maintains administrative, technical and physical safeguards designed to protect personal information from loss, misuse, and unauthorized access, disclosure, alteration, and destruction.</p>\r\n\r\n<h4>Links To Other Websites</h4>\r\n\r\n<p>This Website may contain links to other websites for your convenience and information. These websites may be operated by companies not affiliated with CoinJolt. Linked websites may have their own privacy policies or notices, which we strongly suggest you review if you visit any linked websites. We are not responsible for the content of any websites that are not affiliated with CoinJolt, any use of those websites, or the privacy practices of those websites.</p>\r\n\r\n<h4>Updates To This Privacy Policy</h4>\r\n\r\n<p>This Privacy Policy may be updated periodically and without prior notice to you to reflect changes in our information practices and applicable law. We suggest that you periodically review the Privacy Policy for amendments.</p>\r\n\r\n<h4>How To Contact Us</h4>\r\n\r\n<p>If you have any questions about this Privacy Policy, please contact us at support@coinjolt.com.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', '2018-06-12 12:00:00', '2018-07-26 08:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `cms_risk_disclosures`
--

CREATE TABLE IF NOT EXISTS `cms_risk_disclosures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `risk_disclosures_header_image` varchar(255) DEFAULT NULL,
  `risk_disclosures_header_desc` text,
  `risk_disclosures_content` text,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cms_risk_disclosures`
--

INSERT INTO `cms_risk_disclosures` (`id`, `risk_disclosures_header_image`, `risk_disclosures_header_desc`, `risk_disclosures_content`, `createdAt`, `updatedAt`) VALUES
(1, '', 'Risk Disclosures', '<div class="col-md-8">\r\n<p>Any referrals using unethical methods of promotion, violate FTC guidelines, spam or practice frowned upon marketing will be instantly terminated from promoting our services with a chance of forfeiting commissions. Please refer to the following page to ensure you&rsquo;re updated with the latest email compliance regulations: <a href="https://www.ftc.gov/tips-advice/business-center/guidance/can-spam-act-compliance-guide-business">https://www.ftc.gov/tips-advice/business-center/guidance/can-spam-act-compliance-guide-business</a> Any income claims are not a guarantee of returns. Results vary and are typically determined by experience and skills.</p>\r\n\r\n<p>Bitcoin and other cryptocurrencies are a very speculative investment and involves a high degree of risk. Investors must have the financial ability, sophistication/experience and willingness to bear the risks of an investment, and a potential total loss of their investment.</p>\r\n\r\n<p>Information provided by CoinJolt is not intended to be, nor should it be construed or used as investment, tax or legal advice, a recommendation, or an offer to sell, or a solicitation of an offer to buy, an interest in cryptocurrency. An investment in cryptocurrency is not suitable for all investors.</p>\r\n\r\n<p>&bull; An investor could lose all or a substantial portion of his/her investment in cryptocurrency.<br />\r\n&bull; An investment in cryptocurrency should be discretionary capital set aside strictly for speculative purposes.<br />\r\n&bull; An investment in cryptocurrency is not suitable or desirable for all investors.<br />\r\n&bull; Cryptocurrency has limited operating history or performance.<br />\r\n&bull; Fees and expenses associated with a cryptocurrency investment may be substantial.</p>\r\n\r\n<p>The above summary is not a complete list of the risks and other important disclosures involved in investing in cryptocurrency. Any investment in cryptocurrency is subject to all the risks and disclosures set forth in the Customer Transaction Agreement and other definitive customer agreements.</p>\r\n\r\n<p>The information on this website is for general information purposes only. It is not intended as legal, financial or investment advice and should not be construed or relied on as such. Before making any commitment of a legal or financial nature you should seek advice from a qualified and registered legal practitioner or financial or investment adviser.</p>\r\n</div>\r\n', '2018-06-12 09:00:00', '2018-07-26 08:11:03');

-- --------------------------------------------------------

--
-- Table structure for table `cms_terms_of_services`
--

CREATE TABLE IF NOT EXISTS `cms_terms_of_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terms_of_service_header_image` varchar(255) DEFAULT NULL,
  `terms_of_service_header_desc` text,
  `terms_of_service_content` text,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cms_terms_of_services`
--

INSERT INTO `cms_terms_of_services` (`id`, `terms_of_service_header_image`, `terms_of_service_header_desc`, `terms_of_service_content`, `createdAt`, `updatedAt`) VALUES
(8, '', 'Terms of Service', '<div class="about-section">\r\n<div class="container">\r\n<div class="row">\r\n<div class="col-md-12">\r\n<h4>COINJOLT WEBSITE TERMS AND CONDITIONS OF USE</h4>\r\n\r\n<p>These Website Terms and Conditions of Use (&ldquo;Terms&rdquo;) are effective as of November 11, 2017.</p>\r\n\r\n<p>COINJOLT.COM (&ldquo;CoinJolt,&rdquo; &ldquo;we&rdquo; or &ldquo;us&rdquo;) reserves the right to change, modify, add or remove portions of the Terms at any time for any reason and in our sole discretion. We suggest that you (&ldquo;you,&rdquo; &ldquo;your&rdquo; or &ldquo;User&rdquo;) periodically review the Terms for amendments.</p>\r\n\r\n<p>You acknowledge that by accessing or using this website (or other webpages with links to or that utilize the Terms) (collectively, &ldquo;Website&rdquo;) you are agreeing to the Terms as modified from time to time. We will alert you if changes have been made by indicating on the Terms the date they were last amended.</p>\r\n\r\n<p>Please feel free to contact us with any questions you might have regarding these Terms. You may send us an e-mail at support(at)coinjolt.com.</p>\r\n\r\n<h4>WEBSITE TERMS AND CONDITIONS OF USE</h4>\r\n\r\n<p>PLEASE READ THESE TERMS CAREFULLY BEFORE VISITING THE WEBSITE OR USING THE ONLINE SERVICES AND PRODUCTS PROVIDED THEREON. BY ACCESSING OR UTILIZING THE WEBSITE, YOU ACKNOWLEDGE THAT YOU HAVE READ THESE TERMS AND THAT YOU AGREE TO BE BOUND BY THEM. YOUR COMPLIANCE WITH AND ACCEPTANCE OF THESE TERMS IS A CONDITION TO YOUR RIGHT TO ACCESS AND UTILIZE THE WEBSITE. IF YOU DO NOT AGREE TO ALL OF THE TERMS, YOU ARE NOT AN AUTHORIZED USER OF THESE SERVICES AND PRODUCTS AND YOU SHOULD NOT USE THIS WEBSITE. IF YOU DO NOT AGREE TO THESE TERMS, YOU AGREE THAT YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE WEBSITE.</p>\r\n\r\n<p>UNAUTHORIZED USE OF THIS WEBSITE, INCLUDING BUT NOT LIMITED TO MISUSE OF PASSWORDS OR POSTED INFORMATION, IS STRICTLY PROHIBITED. AUTHORIZED USERS OF THIS WEBSITE INCLUDE ANY USERS VISITING THIS WEBSITE FOR PERSONAL AND/OR INFORMATIONAL PURPOSES, CLIENTS AND POTENTIAL CLIENTS OF COINJOLT,COINJOLT EMPLOYEES, CANDIDATES FOR EMPLOYMENT OR USERS SEEKING INFORMATION ABOUT EMPLOYMENT OPPORTUNITIES AT COINJOLT, AND ANY OTHER USERS AUTHORIZED BY COINJOLT.</p>\r\n\r\n<p>USE OF THIS WEBSITE BY RECRUITERS, &ldquo;HEAD HUNTERS,&rdquo; EMPLOYMENT CONSULTANTS OR AGENCIES, PERSONNEL PLACEMENT AGENCIES, PROFESSIONAL SERVICES COMPANIES, EMPLOYMENT CONTRACTORS AND STAFFING AGENCIES SEEKING CURRENT COINJOLT EMPLOYEES AS CANDIDATES FOR PLACEMENT IS STRICTLY PROHIBITED.</p>\r\n\r\n<h4>No Offer/Local Restrictions</h4>\r\n\r\n<p>Nothing contained in or on the Website should be construed as a solicitation of an offer to buy or offer, or recommendation, to acquire or dispose of any security, commodity, or investment or to engage in any other transaction. CoinJolt offers a number of products and services designed specifically for various categories of investors in various countries and regions. Not all products will be available to all investors. These products or services are only offered to such investors, in those countries and regions, in accordance with applicable laws and regulations. The information provided on the Website is not intended for distribution to, or use by, any person or entity in any jurisdiction or country where such distribution or use would be contrary to law or regulation. All persons and entities accessing the Website do so of their own initiative and are responsible for compliance with applicable local laws and regulations. The Website is not directed to any person in any jurisdiction where the publication or availability of the Website is prohibited by reason of that person&rsquo;s nationality, residence or otherwise. Any User subject to these restrictions must not access the Website.</p>\r\n\r\n<h4>Copyrights</h4>\r\n\r\n<p>All content displayed or otherwise contained on or available via this Website, including without limitation all images, text, articles, research reports, white papers, CoinJolt Daily Observations, programs, photographs, illustrations, and graphics, constitutes the proprietary intellectual property of CoinJolt or its licensors, and are protected by U.S. and international copyright laws. By accessing or utilizing this Website, the User agrees not to directly or indirectly copy, modify, recast, create derivative works, post, publish, display, redistribute, disclose, or make available the content displayed or otherwise contained on or available via this Website, in whole or in part, to any third parties, or assist others to do the same, or otherwise make any commercial use of the materials without the prior written consent of CoinJolt. The User also agrees not to publish or maintain a hyperlink to any portion or page of this Website other than to the homepage unless such hyperlink is for personal use AND is created and transmitted using the &ldquo;email this page&rdquo; function of the Website.</p>\r\n\r\n<h4>Trademarks and Service Marks</h4>\r\n\r\n<p>All trademarks, service marks, trade names, and logos displayed on this Website, including without limitation, CoinJolt&reg; (collectively referred to as the &ldquo;Marks&rdquo;), are proprietary to CoinJolt or their respective owners, and are protected by U.S. and international trademark laws. Any use of the Marks, or any other Marks owned by or licensed to CoinJolt without CoinJolt&rsquo;s express written consent, is strictly prohibited.</p>\r\n\r\n<h4>No Warranty, Limitation of Liability</h4>\r\n\r\n<p>THE INFORMATION ON THE WEBSITE IS PROVIDED &ldquo;AS IS&rdquo;. COINJOLT DOES NOT WARRANT THE ACCURACY OF THE MATERIALS PROVIDED HEREIN, EITHER EXPRESSLY OR IMPLIED, FOR ANY PARTICULAR PURPOSE AND EXPRESSLY DISCLAIMS ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.</p>\r\n\r\n<p>YOU ACKNOWLEDGE THAT COINJOLT SHALL HAVE NO LIABILITY, CONTINGENT OR OTHERWISE, TO YOU OR TO ANY THIRD PARTIES, OR ANY RESPONSIBILITY WHATSOEVER, FOR THE FAILURE OF ANY CONNECTION OR COMMUNICATION SERVICE, TO PROVIDE OR MAINTAIN YOUR ACCESS TO ONLINE SERVICES OR PRODUCTS, OR FOR ANY INTERRUPTION OR DISRUPTION OF SUCH ACCESS OR ANY ERRONEOUS COMMUNICATION BETWEEN COINJOLT AND YOU. COINJOLT SHALL HAVE NO LIABILITY, CONTINGENT OR OTHERWISE, TO YOU OR TO THIRD PARTIES, FOR THE ACCURACY, QUALITY, TIMELINESS, PERFORMANCE, RELIABILITY, OR COMPLETENESS OF THE INFORMATION OR SERVICES CONTAINED ON THE WEBSITE, OR DELAYS, OMISSIONS, OR INTERRUPTIONS IN THE DELIVERY OF THE DATA OR SERVICES OR PRODUCTS AVAILABLE ON THIS WEBSITE OR FOR ANY OTHER ASPECT OF THE PERFORMANCE OF THESE SERVICES AND PRODUCTS. IN NO EVENT WILL COINJOLT BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES THAT MAY BE INCURRED OR EXPERIENCED ON ACCOUNT OF THE USE OF ANY DATA OR SERVICES OR PRODUCTS MADE AVAILABLE ON THIS WEBSITE, EVEN IF COINJOLT HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. COINJOLT SHALL HAVE NO RESPONSIBILITY TO INFORM OR NOTIFY YOU OF ANY DIFFICULTIES EXPERIENCED BY COINJOLT OR ANY THIRD PARTIES WITH RESPECT TO THE USE OF THE SERVICES, PRODUCTS OR DATA PROVIDED ON OR BY THE WEBSITE.</p>\r\n\r\n<p>YOU FURTHER ACKNOWLEDGE THAT NOTHING CONTAINED ON THIS WEBSITE (INCLUDING, BUT NOT LIMITED TO, STRATEGIES AND RESEARCH, DAILY WIRES, DAILY OBSERVATIONS AND EDUCATIONAL ARTICLES) CONSTITUTES INVESTMENT, LEGAL, TAX OR OTHER ADVICE, NOR IS IT TO BE RELIED UPON IN MAKING ANY INVESTMENT OR OTHER DECISIONS. YOU SHOULD SEEK PROFESSIONAL ADVICE PRIOR TO MAKING ANY INVESTMENT DECISIONS.</p>\r\n\r\n<p>WE MAY REVOKE YOUR ACCESS TO OR UTILIZATION OF THE WEBSITE FOR ANY REASON WITHOUT PRIOR NOTICE.</p>\r\n\r\n<h4>Severability</h4>\r\n\r\n<p>If any of the provisions of these Terms are deemed unlawful or for any reason unenforceable, the same shall be inoperative only to the extent necessary to achieve compliance with applicable law and shall not affect the remaining Terms, which shall be given full effect, without regard to the invalid portion(s).</p>\r\n\r\n<h4>Governing Law/Individual Arbitration of Claims</h4>\r\n\r\n<p>The laws of the United States of America, State of California shall govern these Terms, without giving effect to any choice of law or conflict of law rules or provisions that would cause the application of any other jurisdiction&rsquo;s laws. YOU AND COINJOLT AGREE THAT ALL CLAIMS OR DISPUTES CONCERNING THE WEBSITE SHALL PROCEED SOLELY ON AN INDIVIDUAL, NOT CLASS ACTION OR REPRESENTATIVE, BASIS IN BINDING ARBITRATION BEFORE AND SUBJECT TO THE RULES OF THE AMERICAN ARBITRATION ASSOCIATION. ANY PARTY MAY APPLY TO A COURT OF COMPETENT JURISDICTION FOR TEMPORARY CONJUNCTIVE OR OTHER EQUITABLE RELIEF IN AID OF ARBITRATION.</p>\r\n\r\n<h4>Privacy Policy</h4>\r\n\r\n<p>Please review our <a href="http://ec2-54-224-110-112.compute-1.amazonaws.com/coinjolt-html/terms-of-service.html">Privacy Policy</a> for information on how we collect, use, disclose and otherwise manage personal information we collect on the Website, and the measures we take to safeguard the information.</p>\r\n\r\n<h4>Unauthorized Use</h4>\r\n\r\n<p>You may not use any hardware or software intended to damage or interfere with the proper working of the Website or to surreptitiously intercept any system, data or personal information from the Website, including but not limited to (i) use of any robot, spider or other automatic device, process or means to access the Website for any purpose, including monitoring or copying any of the material on the Website; (ii) any attempt to gain unauthorized access to, interfere with, damage or disrupt any parts of the Website, the server on which the Website is hosted, or any server, computer or database connected to the Website; (iii) attack the Website via a denial-of-service attack or a distributed denial-of-service attack; (iv) use any manual process to monitor or copy any of the material on the Website or for any other unauthorized purpose without our prior written consent; or (v) cause the Website or portions of it to be displayed, or appear to be displayed by, for example, framing, deep linking or in-line linking, on any other site. You agree not to interrupt or attempt to interrupt the operation of the Website in any way. CoinJolt reserves the right, in its sole discretion, to limit or terminate your access to or use of the Website at any time without notice. Termination of your access or use will not waive or affect any other right or relief to which CoinJolt may be entitled at law or in equity.</p>\r\n\r\n<h4>Third Party References</h4>\r\n\r\n<p>References and links on the Website to any names, trademarks, products, services or content of third parties are provided solely as a convenience to you and do not in any way imply CoinJolt&rsquo;s endorsement of sponsorship of or affiliation with such third party or their services, goods or content.</p>\r\n\r\n<p>Throughout this agreement the terms &ldquo;us&rdquo;, &ldquo;our&rdquo;, and &ldquo;we&rdquo; refer to CoinJolt.com and it&rsquo;s operator(s) and owner(s). The terms &ldquo;you&rdquo;, &ldquo;your&rdquo;, and &ldquo;user&rdquo; refer to the person or entity accessing CoinJolt.com or using its services. The terms &ldquo;coin&rdquo; and &ldquo;coins&rdquo; are synonyms for cryptocurrencies.</p>\r\n\r\n<p>Accessing this website or any services provided by it in any way indicates your agreement to the complete terms and conditions of this agreement. Any section or item found to be unenforceable by a court of law does not invalidate any other section or item.</p>\r\n\r\n<h4>Refund Policy &ndash; Completed Transactions</h4>\r\n\r\n<p>It is the nature of Bitcoin, Litecoin, and the other cryptocurrencies on our website that all transactions are final with no method of chargeback or recourse for the sender of the funds.</p>\r\n\r\n<p>As such we are unable to reverse or provide refunds for any payment made through our payment system. This also applies if you contact us before the payment is completed but it reaches completion before we are able to respond.</p>\r\n\r\n<p>We do provide a feedback system so buyers can leave ratings for sellers (and vice versa).</p>\r\n\r\n<h4>Refund Policy &ndash; Incomplete Transactions</h4>\r\n\r\n<p>If your coins were not received and/or confirmed in time, we will gladly refund them to you. Simply email <a href="mailto:support@coinjolt.com">support@coinjolt.com</a> with your CoinJolt transaction ID, receipt of purchase, and refund address. The ticket must be opened with the email address used in the transaction.</p>\r\n\r\n<p>We will in most cases email you to ask if you would like a refund, but this is not guaranteed. Refunds will be either (at our discretion): a) the original amount of coins received, or b) an amount equivalent to the USD value at the time of the transaction. All refunds must be claimed within 30 days of us receiving your coins or they will be forfeited. Refunds are sent minus the unsubsidized coin/miner TX fee to send them to you.</p>\r\n\r\n<h4>Refund Policy &ndash; Tiny Amounts</h4>\r\n\r\n<p>For a refund to be honored it must be at least the network transaction fee (TX fee) for that coin times two, otherwise it will be forfeited.</p>\r\n\r\n<h4>Funds Recovery &ndash; Sends to wrong coin/chain, missing tags, delisted coins, etc.</h4>\r\n\r\n<p>If coins are sent to the wrong coin or blockchain or to a delisted coin and need to be recovered by us manually there will be an 8% recovery fee; if the coins can be recovered at all. This would apply if for example you sent Ether to an Ether Classic address or Digitalcoin to a Dogecoin address, etc. This would also apply to coins that require a destination tag, payment ID, memo, etc. and they were not set or were set to an incorrect value. We will not recover tokens we do not support for example from Ether-based ICOs that aren&rsquo;t on our Supported Coins list. You must contact us within 90 days for us to recover your funds for you or they will be forfeited.</p>\r\n\r\n<h4>Restricted Items/Services</h4>\r\n\r\n<p>The following items and/or services may not be bought or sold with our service(s):</p>\r\n\r\n<ul>\r\n	<li>Illegal drugs and substances.</li>\r\n	<li>Pornography featuring underage or unconsenting performers.</li>\r\n	<li>Software or websites with malware, viruses, trojans, spoofing, etc.</li>\r\n	<li>High-yield investment programs (HYIP)</li>\r\n	<li>Content which may be:\r\n	<ul>\r\n		<li>Libelous or maliciously false;</li>\r\n		<li>Infringe any copyright, moral right, database right, trademark right, design right, right in passing off, or other intellectual property rights;</li>\r\n		<li>Be in contempt of any court, or in breach of any court order;</li>\r\n		<li>Be in breach of racial or religious hatred or discrimination legislation;</li>\r\n		<li>Be untrue, false, inaccurate or misleading;</li>\r\n		<li>Constitute spam;</li>\r\n		<li>Be offensive, deceptive, fraudulent, threatening, abusive, harassing, anti-social, menacing, hateful, discriminatory or inflammatory</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<h4>Affiliates/Referrals</h4>\r\n\r\n<p>We offer an affiliate system providing referring users a percentage of the transaction fees earned by the merchants they referred for a period of 5 years. For full information see <a href="http://ec2-54-224-110-112.compute-1.amazonaws.com/coinjolt-html/terms-of-service.html">this help entry.</a></p>\r\n\r\n<h4>Security</h4>\r\n\r\n<p>We take many security measures, including but not limited to:</p>\r\n\r\n<ul>\r\n	<li>Implemented TREZOR login.</li>\r\n	<li>Two-factor authentication (2FA) via Email or Google Authenticator.</li>\r\n	<li>System emails from CoinJolt are PGP signed</li>\r\n	<li>All passwords are stored as a uniquely salted script hash, which is then AES encrypted.</li>\r\n	<li>All sensitive and checkout information such as first and last name, shipping addresses, item names, cryptocurrency payout addresses, etc., are stored under AES encryption.</li>\r\n	<li>TLS encryption between you and our servers to protect against eavesdroppers.</li>\r\n	<li>Account lockouts to prevent brute force password guessing.</li>\r\n	<li>Protection against cross-site scripting (XSS) and SQL Injection attacks.</li>\r\n	<li>Immediate payouts means there are minimal coins on the payment server at any one time, reducing hacking incentive.</li>\r\n	<li>Hot wallet is secured by BitGo&trade;</li>\r\n</ul>\r\n\r\n<h4>ICO Services</h4>\r\n\r\n<p>Any merchants looking to use our system for Initial Coin Offerings (aka ICOs), ICO pre-sales, etc. must do so through our Escrow Service Provider Contract. The Terms and Conditions Agreement of our Escrow Services are thorough and non-negotiable. This is to protect our company as a third party service provider and relinquish any and all liabilities that may occur once the funds are released. If any merchants or users are providing escrow services for an ICO without first contacting our Escrow department at <a href="mailto:payments@coinjolt.com">payments@coinjolt.com</a> they will be subject to the relative charges reflected in our terms and service agreement as outlined in our escrow service provider contract.</p>\r\n\r\n<h4>Interoperability with 3rd Party Services</h4>\r\n\r\n<p>We may also offer &ldquo;payment passthru&rdquo;, &ldquo;IPN translation&rdquo;, and other features or services via 3rd parties such as PayPal(tm), Changelly, ShapeShift, etc. Any dispute resolution, refunds, or issues regarding those services or payments made through them would have to be taken up directly with those services.</p>\r\n\r\n<h4>Liability</h4>\r\n\r\n<p>We make our best effort to ensure the reliability and safety of our services however we can not be held liable in any way for funds lost or fluctuations in market exchange rates. Examples where funds could be lost (but not limited to these examples) are technical issues, 3rd party interference such as hacking or DDoS, untrustworthy and/or fraudulent merchants, or issues with the cryptocurrencies themselves such as software errors or 51% attacks.</p>\r\n\r\n<h4>Changes &amp; Updates</h4>\r\n\r\n<p>We reserve the right to update, change or replace any part of these Terms &amp; Conditions by posting updates and/or changes to our website.</p>\r\n\r\n<p>It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.</p>\r\n\r\n<h4>Additional Terms</h4>\r\n\r\n<p>Certain webpages and portals on this Website may contain terms and conditions in addition to these Terms. In the event of a conflict, the additional terms and conditions will govern such webpages.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', '2018-06-12 05:00:12', '2018-07-04 11:20:50');

-- --------------------------------------------------------

--
-- Table structure for table `cold_wallet_balances`
--

CREATE TABLE IF NOT EXISTS `cold_wallet_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `balance` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_settings`
--

CREATE TABLE IF NOT EXISTS `company_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL,
  `facebook_url` varchar(255) DEFAULT NULL,
  `twitter_url` varchar(255) DEFAULT NULL,
  `linkedin_url` varchar(255) DEFAULT NULL,
  `instagram_url` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company_settings`
--

INSERT INTO `company_settings` (`id`, `phone_number`, `email`, `website_url`, `facebook_url`, `twitter_url`, `linkedin_url`, `instagram_url`, `createdAt`, `updatedAt`) VALUES
(1, '1-888-998-9980', 'support@coinjolt.com', '', 'https://www.facebook.com/coinjolt', 'https://twitter.com/coinjolt', 'https://www.linkedin.com/in/coinjolt/', 'https://instagram.com/coinjolt', '2018-06-13 14:00:00', '2018-07-04 08:57:06');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` varchar(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` varchar(3) DEFAULT NULL,
  `numcode` int(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=478 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`, `createdAt`, `updatedAt`) VALUES
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(240, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(241, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(242, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(243, 'CN', 'CHINA', 'China', 'CHN', 156, 86, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(244, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(245, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(246, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(247, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(248, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(249, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(250, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(251, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(252, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(253, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(254, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(255, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(256, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(261, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(285, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, 'CG', 'CONGO', 'Congo', 'COG', 178, 242, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(292, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(293, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(294, 'CI', 'COTE D''IVOIRE', 'Cote D''Ivoire', 'CIV', 384, 225, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(295, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(296, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(297, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(298, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(299, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(300, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(301, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(302, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(303, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(304, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(305, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(306, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(307, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(308, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(309, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(310, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(311, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(312, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(313, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(314, 'FR', 'FRANCE', 'France', 'FRA', 250, 33, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(315, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(316, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(317, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(318, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(319, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(320, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(321, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(322, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(323, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(324, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(325, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(326, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(327, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(328, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(329, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(330, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(331, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(332, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(333, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(334, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(335, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(336, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(337, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(338, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(339, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(340, 'IN', 'INDIA', 'India', 'IND', 356, 91, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(341, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(342, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(343, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(344, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(345, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(346, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(347, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(348, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(349, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(350, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(351, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(352, 'KP', 'KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF', 'Korea, Democratic People''s Republic of', 'PRK', 408, 850, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(353, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(354, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(355, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(356, 'LA', 'LAO PEOPLE''S DEMOCRATIC REPUBLIC', 'Lao People''s Democratic Republic', 'LAO', 418, 856, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(357, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(358, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(359, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(360, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(361, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(362, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(363, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(364, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(365, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(366, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(367, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(368, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(369, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(370, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(371, 'ML', 'MALI', 'Mali', 'MLI', 466, 223, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(372, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(373, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(374, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(375, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(376, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(377, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(378, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(379, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(380, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(381, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(382, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(383, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(384, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(385, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(386, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(387, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(388, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(389, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(390, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(391, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(392, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(393, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(394, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(395, 'NE', 'NIGER', 'Niger', 'NER', 562, 227, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(396, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(397, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(398, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(399, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(400, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(401, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(402, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(403, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(404, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(405, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(406, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(407, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(408, 'PE', 'PERU', 'Peru', 'PER', 604, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(409, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(410, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(411, 'PL', 'POLAND', 'Poland', 'POL', 616, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(412, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(413, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(414, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(415, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(416, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(417, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(418, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(419, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(420, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(421, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(422, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(423, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(424, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(425, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(426, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(427, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(428, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(429, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(430, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(431, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(432, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(433, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(434, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(435, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(436, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(437, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(438, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(439, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(440, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(441, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(442, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(443, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(444, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(445, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(446, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(447, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(448, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(449, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(450, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(451, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(452, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(453, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(454, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(455, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(456, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(457, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(458, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(459, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(460, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(461, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(462, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(463, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(464, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(465, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(466, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(467, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(468, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(469, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(470, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(471, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(472, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(473, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(474, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(475, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(476, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(477, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alt_name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `currency_id` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `alt_name`, `display_name`, `createdAt`, `updatedAt`, `currency_id`) VALUES
(1, 'btc', 'Bitcoin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'BTC'),
(2, 'eth', 'Ethereum', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ETH'),
(3, 'ltc', 'Litecoin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'LTC'),
(4, 'xrp', 'Ripple', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'XRP'),
(5, 'bch', 'Bitcoin Cash', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'BCH'),
(6, 'ada', 'Cardano', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ADA'),
(7, 'neo', 'NEO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'NEO'),
(8, 'xlm', 'Stellar', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'XLM'),
(9, 'eos', 'EOS', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'EOS'),
(10, 'iota', 'IOTA', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'MIOTA'),
(11, 'dash', 'Dash', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'DASH'),
(12, 'xmr', 'Monero', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'XMR'),
(13, 'xem', 'NEM', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'XEM'),
(14, 'lsk', 'Lisk', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'LSK'),
(15, 'trx', 'Tron', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'TRX'),
(16, 'ven', 'VeChain', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'VEN'),
(17, 'qtum', 'Qtum', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'QTUM'),
(18, 'usdt', 'Tether', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'USDT'),
(19, 'icx', 'ICON', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ICX'),
(20, 'omg', 'OmiseGO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'OMG'),
(21, 'zec', 'Zcash', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ZEC'),
(22, 'xvg', 'Verge', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'XVG'),
(23, 'steem', 'Steem', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'STEEM'),
(24, 'ppt', 'Populous', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'PPT'),
(25, 'sc', 'Siacoin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'SC'),
(26, 'strat', 'Stratis', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'STRAT'),
(27, 'rhoc', 'RChain', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'RHOC'),
(28, 'waves', 'Waves', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'WAVES'),
(29, 'snt', 'Status', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'SNT'),
(30, 'doge', 'Dogecoin', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'DOGE'),
(31, 'bts', 'BitShares', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'BTS'),
(32, 'mkr', 'Maker', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'MKR'),
(33, 'zcl', 'ZClassic', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ZCL'),
(34, 'ae', 'Aeternity', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'AE'),
(35, 'dgd', 'DigixDAO', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'DGD'),
(36, 'zrx', '0x', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ZRX'),
(37, 'rep', 'Augur', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'REP'),
(38, 'dcr', 'Decred', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'DCR'),
(39, 'veri', 'Veritaseum', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'VERI'),
(40, 'hsr', 'Hshare', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'HSR'),
(41, 'etn', 'Electroneum', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ETN'),
(42, 'kmd', 'Komodo', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'KMD'),
(43, 'ardr', 'Ardor', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ARDR'),
(44, 'ark', 'Ark', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ARK'),
(45, 'gas', 'Gas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'GAS'),
(46, 'royal-mint-gold', 'Royal Mint Gold', '2018-05-22 00:00:00', '2018-05-22 00:00:00', 'RMG'),
(47, 'bnb', 'Binance Coin', '2018-06-11 00:00:00', '2018-06-11 00:00:00', 'BNB');

-- --------------------------------------------------------

--
-- Table structure for table `currency_balances`
--

CREATE TABLE IF NOT EXISTS `currency_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `balance` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `currency_balances`
--

INSERT INTO `currency_balances` (`id`, `user_id`, `currency_id`, `balance`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, '10.002017132902132', '2018-05-10 11:15:18', '2018-08-30 11:28:57'),
(2, 1, 3, '0.8047332980185367', '2018-05-10 11:16:02', '2018-07-05 12:06:57'),
(3, 1, 5, '3.0049081142869682', '2018-05-10 12:01:33', '2018-05-10 12:03:48'),
(4, 1, 9, '449.8128010621724', '2018-05-14 08:38:19', '2018-05-14 08:38:40'),
(5, 1, 2, '3.5418685145107744', '2018-07-30 07:37:35', '2018-08-30 11:28:45'),
(6, 58, 1, '0.00026755852842809364', '2018-08-07 09:56:45', '2018-08-07 09:56:45'),
(7, 1, 45, '1256.6880940364622', '2018-08-14 11:11:43', '2018-08-14 11:11:43'),
(9, 1, 6, '5', '2018-08-29 07:45:12', '2018-08-29 07:45:26'),
(10, 70, 7, '.06', '2018-08-29 07:46:04', '2018-08-29 07:46:04'),
(12, 1, 16, '8.5', '2018-08-29 09:01:27', '2018-08-31 10:10:57'),
(13, 17, 17, '.9', '2018-08-29 09:03:40', '2018-08-29 09:05:03'),
(14, 17, 9, '0.523', '2018-08-29 09:05:43', '2018-08-29 09:05:43'),
(15, 1, 47, '3.0280274419729523', '2018-08-30 10:53:53', '2018-08-30 10:54:43'),
(16, 1, 21, '4', '2018-08-31 10:10:27', '2018-08-31 10:10:27'),
(17, 59, 14, '5', '2018-09-05 09:58:39', '2018-09-05 09:58:39'),
(18, 58, 18, '6', '2018-09-05 10:15:38', '2018-09-05 10:15:38'),
(19, 14, 9, '20', '2018-09-05 10:37:11', '2018-09-05 10:37:11');

-- --------------------------------------------------------

--
-- Table structure for table `deposits`
--

CREATE TABLE IF NOT EXISTS `deposits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `checkout_id` varchar(255) DEFAULT NULL,
  `account_id` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '0=Deposit,1=Buy,2=Sell,3=Withdraw,4=MCP Deposit,5=MCP Withdraw,6=MCP Invest',
  `description` text,
  `amount` varchar(255) DEFAULT NULL,
  `gross` varchar(255) DEFAULT NULL,
  `processing_fee` varchar(255) DEFAULT NULL,
  `payer_email` varchar(255) DEFAULT NULL,
  `payer_name` varchar(255) DEFAULT NULL,
  `current_rate` varchar(255) DEFAULT NULL,
  `converted_amount` varchar(255) DEFAULT NULL,
  `balance` varchar(255) NOT NULL,
  `payment_method` int(1) NOT NULL COMMENT '1=Credit card,2=Wire transfer deposit,3=''bank wire transfer withdraw'',4=''check by mail'',5=''MCP Withdraw'',6=''PayPal''',
  `credit_card_no` bigint(20) DEFAULT NULL,
  `card_expmonth` int(11) DEFAULT NULL,
  `card_expyear` varchar(255) DEFAULT NULL,
  `cvv` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `currency_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=219 ;

--
-- Dumping data for table `deposits`
--

INSERT INTO `deposits` (`id`, `user_id`, `transaction_id`, `checkout_id`, `account_id`, `type`, `description`, `amount`, `gross`, `processing_fee`, `payer_email`, `payer_name`, `current_rate`, `converted_amount`, `balance`, `payment_method`, `credit_card_no`, `card_expmonth`, `card_expyear`, `cvv`, `createdAt`, `updatedAt`, `currency_id`) VALUES
(1, 1, '37252695', '37252695', NULL, 0, NULL, '20000', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 4434565676578763, 4, '2020', 5454, '2018-04-25 08:32:48', '2018-04-25 08:32:48', 0),
(2, 1, '35716123', '35716123', NULL, 0, NULL, '5500', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 2332535343443646, 2, '2020', 2332, '2018-04-25 08:50:34', '2018-04-25 08:50:34', 0),
(3, 1, '30015428', '30015428', NULL, 0, NULL, '10000', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 4345775432357563, 2, '2019', 5464, '2018-04-25 08:51:42', '2018-04-25 08:51:42', 0),
(4, 1, '54063362', '54063362', NULL, 1, NULL, '9000', NULL, NULL, NULL, NULL, '9435.55', '9.53839468817398', '9.53839468817398', 0, NULL, NULL, NULL, NULL, '2018-04-25 08:53:07', '2018-04-25 08:53:07', 1),
(6, 1, '30758300', '30758300', NULL, 1, NULL, '5000', NULL, NULL, NULL, NULL, '639.2086399397285', '7.822172116558772', '7.822172116558772', 0, NULL, NULL, NULL, NULL, '2018-04-25 09:01:53', '2018-04-25 09:01:53', 2),
(8, 1, '66279431', '66279431', NULL, 0, NULL, '80', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 9898989898989898, 1, '2018', 852, '2018-04-25 09:54:25', '2018-04-25 09:54:25', 0),
(9, 1, '59239902', '59239902', NULL, 0, NULL, '100', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-04-25 09:55:26', '2018-04-25 09:55:26', 0),
(10, 1, '68215656', '68215656', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '9328.87', '0.0002143882378037211', '8.540214388237803', 0, NULL, NULL, NULL, NULL, '2018-04-30 07:10:00', '2018-04-30 07:10:00', 1),
(11, 1, '33426265', '33426265', NULL, 2, NULL, '18706.58', NULL, NULL, NULL, NULL, '9353.29', '2', '6.539999999999999', 0, NULL, NULL, NULL, NULL, '2018-04-30 07:13:50', '2018-04-30 07:13:50', 1),
(12, 1, '78866775', '78866775', NULL, 4, NULL, '10000', NULL, NULL, NULL, NULL, '1', '100000', '', 0, NULL, NULL, NULL, NULL, '2018-05-02 09:30:48', '2018-05-02 09:30:48', 0),
(13, 1, '27534751', '27534751', NULL, 4, NULL, '15000', NULL, NULL, NULL, NULL, '1', '15000', '', 0, NULL, NULL, NULL, NULL, '2018-05-02 09:33:50', '2018-05-02 09:33:50', 0),
(14, 1, '38278125', '38278125', NULL, 0, NULL, '50', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-05-02 09:38:41', '2018-05-02 09:38:41', 0),
(16, 1, '2926809', '2926809', NULL, 1, NULL, '1', NULL, NULL, NULL, NULL, '683.3867621926075', '0.001463300220787943', '7.821463300220788', 0, NULL, NULL, NULL, NULL, '2018-05-02 10:37:42', '2018-05-02 10:37:42', 2),
(17, 1, '83199081', '83199081', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '9189.38', '0.0002176425395402084', '8.539999999999999', 0, NULL, NULL, NULL, NULL, '2018-05-02 11:12:33', '2018-05-02 11:12:33', 1),
(18, 1, '49728323', '49728323', NULL, 1, NULL, '200', NULL, NULL, NULL, NULL, '9207.8', '0.021720715045939313', '8.561720715045938', 0, NULL, NULL, NULL, NULL, '2018-05-02 11:40:48', '2018-05-02 11:40:48', 1),
(19, 1, '47343259', '47343259', NULL, 2, NULL, '18415.6', NULL, NULL, NULL, NULL, '9207.80', '2', '6.561720715045938', 0, NULL, NULL, NULL, NULL, '2018-05-02 11:42:06', '2018-05-02 11:42:06', 1),
(23, 1, '62291580', '62291580', NULL, 4, NULL, '20000', NULL, NULL, NULL, NULL, '1', '20000', '', 0, NULL, NULL, NULL, NULL, '2018-05-02 13:24:15', '2018-05-02 13:24:15', 0),
(24, 1, '38122819', '38122819', NULL, 3, NULL, '80', NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, '2018-05-02 13:42:51', '2018-05-02 13:42:51', 0),
(25, 4, '65681662', '65681662', NULL, 4, NULL, '45242', NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, '2018-05-02 13:52:21', '2018-05-02 13:52:21', 0),
(26, 1, '12962271', '12962271', NULL, 0, NULL, '500', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-05-03 05:37:46', '2018-05-03 05:37:46', 0),
(27, 1, '46675981', '46675981', NULL, 4, NULL, '18000', NULL, NULL, NULL, NULL, '1', '18000', '', 0, NULL, NULL, NULL, NULL, '2018-05-03 06:15:44', '2018-05-03 06:15:44', 0),
(28, 1, '6541459', '6541459', NULL, 0, NULL, '50', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-05-03 06:42:32', '2018-05-03 06:42:32', 0),
(29, 1, '61025226', '61025226', NULL, 0, NULL, '6000', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-05-03 06:43:09', '2018-05-03 06:43:09', 0),
(30, 1, '23538935', '23538935', NULL, 3, NULL, '150', NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, '2018-05-03 07:25:34', '2018-05-03 07:25:34', 0),
(31, 1, '22812552', '22812552', NULL, 3, NULL, '453', NULL, NULL, NULL, NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, '2018-05-03 07:26:38', '2018-05-03 07:26:38', 0),
(33, 1, '29022457', '29022457', NULL, 2, NULL, '1457.46', NULL, NULL, NULL, NULL, '728.73', '2', '5.821463300220788', 0, NULL, NULL, NULL, NULL, '2018-05-09 09:14:23', '2018-05-09 09:14:23', 2),
(34, 1, '49410024', '49410024', NULL, 2, NULL, '728.73', NULL, NULL, NULL, NULL, '728.73', '1', '4.821463300220788', 0, NULL, NULL, NULL, NULL, '2018-05-09 09:14:45', '2018-05-09 09:14:45', 2),
(35, 1, '63745709', '63745709', NULL, 2, NULL, '728.66', NULL, NULL, NULL, NULL, '728.66', '1', '3.8214633002207883', 0, NULL, NULL, NULL, NULL, '2018-05-09 09:18:42', '2018-05-09 09:18:42', 2),
(36, 1, '4715405', '4715405', NULL, 4, NULL, '60000', NULL, NULL, NULL, NULL, '1', '60000', '', 0, NULL, NULL, NULL, NULL, '2018-05-09 09:21:04', '2018-05-09 09:21:04', 0),
(37, 1, '80105026', '80105026', NULL, 4, NULL, '10000', NULL, NULL, NULL, NULL, '1', '10000', '', 0, NULL, NULL, NULL, NULL, '2018-05-09 09:21:16', '2018-05-09 09:21:16', 0),
(38, 1, '18494096', '18494096', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '9109.918767365885', '0.00021954092578350135', '6.561940255971722', 0, NULL, NULL, NULL, NULL, '2018-05-09 09:22:05', '2018-05-09 09:22:05', 1),
(39, 1, '83061551', '83061551', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '9291.557949113716', '0.0002152491553034733', '6.562155505127025', 0, NULL, NULL, NULL, NULL, '2018-05-09 12:16:07', '2018-05-09 12:16:07', 1),
(41, 1, '53633397', '53633397', NULL, 1, NULL, '20', NULL, NULL, NULL, NULL, '746.251298063084', '0.026800623398459143', '3.8482639236192475', 0, NULL, NULL, NULL, NULL, '2018-05-09 12:18:37', '2018-05-09 12:18:37', 2),
(42, 1, '89669089', '89669089', NULL, 1, NULL, '1', NULL, NULL, NULL, NULL, '9292.532406080438', '0.00010761329165187136', '6.562263118418677', 0, NULL, NULL, NULL, NULL, '2018-05-09 12:36:03', '2018-05-09 12:36:03', 1),
(43, 1, '22820965', '22820965', NULL, 1, NULL, '100', NULL, NULL, NULL, NULL, '158.1245311594413', '0.6324129422977847', '0.6324129422977847', 0, NULL, NULL, NULL, NULL, '2018-05-09 12:38:48', '2018-05-09 12:38:48', 3),
(44, 1, '32411039', '32411039', NULL, 1, NULL, '1000', NULL, NULL, NULL, NULL, '225.1497717755842', '4.441487957610457', '4.441487957610457', 0, NULL, NULL, NULL, NULL, '2018-05-09 12:39:42', '2018-05-09 12:39:42', 12),
(45, 1, '49584240', '49584240', NULL, 1, NULL, '2000', NULL, NULL, NULL, NULL, '225.1497717755842', '8.882975915220914', '13.324463872831371', 0, NULL, NULL, NULL, NULL, '2018-05-09 12:40:50', '2018-05-09 12:40:50', 12),
(46, 1, '90907183', '90907183', NULL, 2, NULL, '9292.5', NULL, NULL, NULL, NULL, '9292.50', '1', '5.562263118418677', 0, NULL, NULL, NULL, NULL, '2018-05-09 12:59:28', '2018-05-09 12:59:28', 1),
(47, 1, '10152525', '10152525', NULL, 1, NULL, '20', NULL, NULL, NULL, NULL, '3.285153941839413', '6.087994764958157', '6.087994764958157', 0, NULL, NULL, NULL, NULL, '2018-05-09 13:15:37', '2018-05-09 13:15:37', 44),
(48, 1, '5741024', '5741024', NULL, 2, NULL, '19.740000000000002', NULL, NULL, NULL, NULL, '3.29', '6', '0.08799476495815739', 0, NULL, NULL, NULL, NULL, '2018-05-09 13:17:01', '2018-05-09 13:17:01', 44),
(49, 1, '48166485', '48166485', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '2.312', '0.8650519031141869', '0.8650519031141869', 0, NULL, NULL, NULL, NULL, '2018-05-10 05:30:24', '2018-05-10 05:30:24', 10),
(50, 1, '15741272', '15741272', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '9372.66', '0.00021338659462735233', '5.562476505013304', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:13:44', '2018-05-10 11:13:44', 1),
(51, 1, '67025357', '67025357', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '9372.66', '0.00021338659462735233', '5.562689891607931', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:15:18', '2018-05-10 11:15:18', 1),
(52, 1, '54055244', '54055244', NULL, 1, NULL, '5', NULL, NULL, NULL, NULL, '9372.66', '0.0005334664865683808', '5.5632233580945', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:15:40', '2018-05-10 11:15:40', 1),
(53, 1, '25506251', '25506251', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '158.31585014992226', '0.012632973881680425', '0.645045916179465', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:16:02', '2018-05-10 11:16:02', 3),
(54, 1, '96139685', '96139685', NULL, 1, NULL, '56', NULL, NULL, NULL, NULL, '158.494', '0.35332567794364456', '0.9983715941231096', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:16:18', '2018-05-10 11:16:18', 3),
(55, 1, '88420096', '88420096', NULL, 2, NULL, '46839.15', NULL, NULL, NULL, NULL, '9367.83', '5', '0.5632233580944996', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:18:19', '2018-05-10 11:18:19', 1),
(56, 1, '30455302', '30455302', NULL, 1, NULL, '1', NULL, NULL, NULL, NULL, '455.217', '0.002196754514879717', '0.002196754514879717', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:30:26', '2018-05-10 11:30:26', 11),
(57, 1, '8173500', '8173500', NULL, 1, NULL, '10000', NULL, NULL, NULL, NULL, '9365.33', '1.067768033801265', '1.6309913918957646', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:30:53', '2018-05-10 11:30:53', 1),
(58, 1, '20978574', '20978574', NULL, 1, NULL, '5000', NULL, NULL, NULL, NULL, '455.217', '10.983772574398584', '10.985969328913464', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:31:12', '2018-05-10 11:31:12', 11),
(59, 1, '26494805', '26494805', NULL, 2, NULL, '1514.86', NULL, NULL, NULL, NULL, '757.43', '2', '1.8482639236192475', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:35:39', '2018-05-10 11:35:39', 2),
(60, 1, '19243562', '19243562', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '17.971483069126606', '0.11128742087155959', '0.11128742087155959', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:45:11', '2018-05-10 11:45:11', 9),
(61, 1, '84635508', '84635508', NULL, 1, NULL, '1', NULL, NULL, NULL, NULL, '157.19059177193742', '0.006361703895426939', '1.0047332980185366', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:45:25', '2018-05-10 11:45:25', 3),
(62, 1, '73126429', '73126429', NULL, 1, NULL, '20', NULL, NULL, NULL, NULL, '0.080346', '248.92340626789138', '248.92340626789138', 0, NULL, NULL, NULL, NULL, '2018-05-10 11:55:12', '2018-05-10 11:55:12', 15),
(63, 1, '15278924', '15278924', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '1664.53', '0.0012015403747604428', '0.0012015403747604428', 0, NULL, NULL, NULL, NULL, '2018-05-10 12:01:33', '2018-05-10 12:01:33', 5),
(64, 1, '81621847', '81621847', NULL, 1, NULL, '5000', NULL, NULL, NULL, NULL, '1664.61', '3.0037065739122077', '3.0049081142869682', 0, NULL, NULL, NULL, NULL, '2018-05-10 12:03:48', '2018-05-10 12:03:48', 5),
(65, 1, '28933026', '28933026', NULL, 2, NULL, '1.5499', NULL, NULL, NULL, NULL, '14.09', '.11', '0.0012874208715595858', 0, NULL, NULL, NULL, NULL, '2018-05-14 08:38:19', '2018-05-14 08:38:19', 9),
(66, 1, '41467851', '41467851', NULL, 1, NULL, '6336', NULL, NULL, NULL, NULL, '14.0859', '449.81151364130085', '449.8128010621724', 0, NULL, NULL, NULL, NULL, '2018-05-14 08:38:39', '2018-05-14 08:38:39', 9),
(68, 1, '21401692', '21401692', NULL, 4, NULL, '10000', NULL, NULL, NULL, NULL, '1', '10000', '', 0, NULL, NULL, NULL, NULL, '2018-05-14 10:50:24', '2018-05-14 10:50:24', 0),
(69, 1, '89646774', '89646774', NULL, 4, NULL, '20000', NULL, NULL, NULL, NULL, '1', '20000', '', 0, NULL, NULL, NULL, NULL, '2018-05-14 10:54:37', '2018-05-14 10:54:37', 0),
(70, 1, '86416060', '86416060', NULL, 1, NULL, '1000', NULL, NULL, NULL, NULL, '8412.91', NULL, '1.6309913918957646', 0, NULL, NULL, NULL, NULL, '2018-05-16 13:10:18', '2018-05-16 13:10:18', 1),
(71, 1, '89502199', '89502199', NULL, 1, NULL, '1000', NULL, NULL, NULL, NULL, '8352.016988502231', '0.11973155722463756', '1.7507229491204022', 0, NULL, NULL, NULL, NULL, '2018-05-17 06:19:57', '2018-05-17 06:19:57', 1),
(72, 1, '29907196', '29907196', NULL, 2, NULL, '1000', NULL, NULL, NULL, NULL, '8380.07', '0.11933074544723374', '1.6313922036731685', 0, NULL, NULL, NULL, NULL, '2018-05-17 07:01:40', '2018-05-17 07:01:40', 1),
(73, 1, '9121602', '9121602', NULL, 1, NULL, '0', NULL, NULL, NULL, NULL, '8054.61', '0.12415250396977633', '1.755544707642945', 0, NULL, NULL, NULL, NULL, '2018-05-18 06:14:48', '2018-05-18 06:14:48', 1),
(76, 1, '31002809', '31002809', NULL, 1, NULL, '1000', NULL, NULL, NULL, NULL, '8128.15', '0.12302922559253951', '1.8785739332354845', 0, NULL, NULL, NULL, NULL, '2018-05-18 10:12:28', '2018-05-18 10:12:28', 1),
(77, 1, '1527080208983795833', '681329415', '1595033018', 0, NULL, '210', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 8, '2018', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(78, 1, '1527082641300294164', '336032709', '1595033018', 0, NULL, '19', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 8, '2018', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(79, 1, '1527082960583177440', '430866496', '1595033018', 0, NULL, '63', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 8, '2018', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(80, 1, '1527083148170808900', '741487773', '1595033018', 0, NULL, '31', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 8, '2018', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(81, 1, '1527083743360130162', '136522162', '1595033018', 0, NULL, '153', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 8, '2018', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(82, 1, '21611860', '21611860', NULL, 0, NULL, '153', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 4111, 8, '2018', 123, '2018-05-23 13:55:53', '2018-05-23 13:55:53', 0),
(83, 1, '46084335', '46084335', NULL, 4, NULL, '10000', NULL, NULL, NULL, NULL, '1', '10000', '', 0, NULL, NULL, NULL, NULL, '2018-05-24 05:59:48', '2018-05-24 05:59:48', 0),
(84, 1, '32739300', '32739300', NULL, 4, NULL, '20000', NULL, NULL, NULL, NULL, '1', '20000', '', 0, NULL, NULL, NULL, NULL, '2018-05-24 06:02:59', '2018-05-24 06:02:59', 0),
(85, 1, '1527151935591041009', '292974853', '1595033018', 0, NULL, '200', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 10, '2028', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(86, 1, '1527151940661971750', '588671875', '1595033018', 0, NULL, '200', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 10, '2028', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(87, 1, '1527152027118880952', '604299926', '1595033018', 0, NULL, '2000', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 11, '2030', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(88, 1, '1527152679823758707', '268695068', '1595033018', 0, NULL, '50', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 1, '2033', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(89, 1, '1527152763821028987', '629953002', '1595033018', 0, NULL, '20', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 1, '2023', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(90, 1, '1527153327338869998', '919788871', '1595033018', 0, NULL, '129', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 7, '2018', 123, '2018-05-24 09:15:32', '0000-00-00 00:00:00', 0),
(91, 11, '43542480', '43542480', NULL, 0, NULL, '129', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 4111, 7, '2018', 123, '2018-05-24 09:15:32', '2018-05-24 09:15:32', 0),
(92, 4, '1527153343696441265', '505505371', '1595033018', 0, NULL, '666', '0.00', '0.00', 'nilesh@wrctpl.com', 'Nilesh Sanyal', NULL, NULL, '', 1, 4111111111111111, 1, '2024', 123, '2018-05-24 11:15:32', '2018-05-24 11:15:32', 0),
(96, 1, '38639882', '38639882', '38639882', 0, NULL, '20', NULL, '0.88', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-06-08 07:28:34', '2018-06-08 07:28:34', 0),
(97, 1, '66748306', '66748306', NULL, 0, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-06-08 07:30:20', '2018-06-08 07:30:20', 0),
(98, 1, '89222660', '89222660', NULL, 3, NULL, '6000', NULL, NULL, NULL, NULL, NULL, NULL, '', 3, NULL, NULL, NULL, NULL, '2018-06-08 07:30:33', '2018-06-08 07:30:33', 0),
(99, 1, '55641692', '55641692', NULL, 3, NULL, '450', NULL, NULL, NULL, NULL, NULL, NULL, '', 3, NULL, NULL, NULL, NULL, '2018-06-08 07:31:11', '2018-06-08 07:31:11', 0),
(101, 1, 'PAY-85R32241B2153743GLMNDF2A', '53044075', '53044075', 0, NULL, '35', NULL, '1.32', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-06-08 07:40:51', '2018-06-08 07:40:51', 0),
(102, 1, 'PAY-9Y537641C39471348LMNECIA', '22835524', '22835524', 0, NULL, '20', NULL, '0.88', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-06-08 08:41:43', '2018-06-08 08:41:43', 0),
(103, 1, '65975440', '65975440', NULL, 4, NULL, '10000', NULL, NULL, NULL, NULL, '1', '10000', '', 0, NULL, NULL, NULL, NULL, '2018-06-08 09:04:16', '2018-06-08 09:04:16', 0),
(104, 1, 'PAY-97T121768X267982JLMNEUBA', '16341273', '16341273', 0, NULL, '10', NULL, '0.59', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-06-08 09:19:53', '2018-06-08 09:19:53', 0),
(105, 1, 'PAY-9AC68096PG037390NLMNEWKA', '91639970', '91639970', 0, NULL, '5', NULL, '0.45', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-06-08 09:25:07', '2018-06-08 09:25:07', 0),
(106, 1, 'PAY-29H031774L059270ELMNE5NI', '83391885', '83391885', 0, NULL, '1', NULL, '0.33', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-06-08 09:39:49', '2018-06-08 09:39:49', 0),
(107, 1, '97581065', '97581065', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '7566.2', '0.0002643334831223071', '1.8788382667186068', 0, NULL, NULL, NULL, NULL, '2018-06-08 12:25:08', '2018-06-08 12:25:08', 1),
(108, 1, '42055973', '42055973', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '6536.000000000001', '0.0003059975520195838', '1.8791442642706264', 0, NULL, NULL, NULL, NULL, '2018-07-03 11:44:06', '2018-07-03 11:44:06', 1),
(109, 1, '49737515', '49737515', NULL, 2, NULL, '6536', NULL, NULL, NULL, NULL, '6536.00', '1', '0.8791442642706264', 0, NULL, NULL, NULL, NULL, '2018-07-03 11:45:16', '2018-07-03 11:45:16', 1),
(110, 1, '1165113', '1165113', NULL, 4, NULL, '10000', NULL, NULL, NULL, NULL, '1', '10000', '', 0, NULL, NULL, NULL, NULL, '2018-07-03 12:00:44', '2018-07-03 12:00:44', 0),
(111, 1, '29304875', '29304875', NULL, 4, NULL, '20000', NULL, NULL, NULL, NULL, '1', '20000', '', 0, NULL, NULL, NULL, NULL, '2018-07-03 12:03:34', '2018-07-03 12:03:34', 0),
(112, 1, '94266811', '94266811', NULL, 3, NULL, '3000', NULL, NULL, NULL, NULL, NULL, NULL, '', 4, NULL, NULL, NULL, NULL, '2018-07-04 10:58:19', '2018-07-04 10:58:19', 0),
(113, 1, '36259121', '36259121', NULL, 2, NULL, '669.8330000000001', NULL, NULL, NULL, NULL, '6698.33', '.1', '0.7791442642706264', 0, NULL, NULL, NULL, NULL, '2018-07-05 12:05:53', '2018-07-05 12:05:53', 1),
(114, 1, '19842379', '19842379', NULL, 1, NULL, '3', NULL, NULL, NULL, NULL, '6698.33', '0.00044787282800339787', '0.7795921370986298', 0, NULL, NULL, NULL, NULL, '2018-07-05 12:06:09', '2018-07-05 12:06:09', 1),
(115, 1, '31650896', '31650896', NULL, 2, NULL, '16.858', NULL, NULL, NULL, NULL, '84.29', '.2', '0.8047332980185367', 0, NULL, NULL, NULL, NULL, '2018-07-05 12:06:57', '2018-07-05 12:06:57', 3),
(117, 66, '77160616', '77160616', NULL, 0, NULL, '9999', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 08:00:03', '2018-07-12 08:00:03', 0),
(118, 66, '20824722', '20824722', NULL, 0, NULL, '9999', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 08:19:50', '2018-07-12 08:19:50', 0),
(119, 66, '12882413', '12882413', NULL, 0, NULL, '1224', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 08:31:15', '2018-07-12 08:31:15', 0),
(120, 66, '55568981', '55568981', NULL, 0, NULL, '1562', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 09:07:21', '2018-07-12 09:07:21', 0),
(121, 66, '4825552', '4825552', NULL, 0, NULL, '1399', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 09:15:23', '2018-07-12 09:15:23', 0),
(122, 66, '55715781', '55715781', NULL, 0, NULL, '1988', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 09:38:24', '2018-07-12 09:38:24', 0),
(123, 66, '39484860', '39484860', NULL, 0, NULL, '1699', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 09:43:48', '2018-07-12 09:43:48', 0),
(124, 66, '12967681', '12967681', NULL, 0, NULL, '1999', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 09:54:01', '2018-07-12 09:54:01', 0),
(125, 66, '67235294', '67235294', NULL, 0, NULL, '1899', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 10:02:56', '2018-07-12 10:02:56', 0),
(126, 66, '94217107', '94217107', NULL, 0, NULL, '1299', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 10:12:34', '2018-07-12 10:12:34', 0),
(127, 66, '50365782', '50365782', NULL, 0, NULL, '1299', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 10:15:02', '2018-07-12 10:15:02', 0),
(128, 66, '29289366', '29289366', NULL, 0, NULL, '1299', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-12 10:25:15', '2018-07-12 10:25:15', 0),
(129, 1, '35074608', '35074608', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '6262.79', '0.000319346489344206', '0.779911483587974', 0, NULL, NULL, NULL, NULL, '2018-07-13 12:46:41', '2018-07-13 12:46:41', 1),
(130, 1, '82084998', '82084998', NULL, 4, NULL, '10000', NULL, NULL, NULL, NULL, '1', '10000', '0', 0, NULL, NULL, NULL, NULL, '2018-07-16 05:19:21', '2018-07-16 05:19:21', 0),
(131, 1, 'PAY-32151889NS2530139LNIX2VA', '94839035', '94839035', 0, NULL, '5', NULL, '0.45', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-07-20 06:13:03', '2018-07-20 06:13:03', 0),
(132, 1, 'PAY-6FW386058C151812YLNIX4SI', '45995349', '45995349', 0, NULL, '7', NULL, '0.5', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-07-20 06:17:07', '2018-07-20 06:17:07', 0),
(133, 1, 'PAY-6MW51777TY306142ELNIZW3Q', '29132876', '29132876', 0, NULL, '29', NULL, '1.14', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-07-20 08:22:08', '2018-07-20 08:22:08', 0),
(134, 1, 'PAY-8XA95450RN353334ELNI2TRI', '39357380', '39357380', 0, NULL, '27', NULL, '1.08', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-07-20 09:23:35', '2018-07-20 09:23:35', 0),
(135, 1, 'PAY-6TX85022BL6340811LNI2ZCI', '35113437', '35113437', 0, NULL, '28', NULL, '1.11', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-07-20 09:35:11', '2018-07-20 09:35:11', 0),
(136, 1, 'PAY-36C614864F1678241LNI237Y', '39757859', '39757859', 0, NULL, '17', NULL, '0.79', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-07-20 09:40:42', '2018-07-20 09:40:42', 0),
(137, 1, '31050445', '31050445', NULL, 1, NULL, '2000', NULL, NULL, NULL, NULL, '8180.51', '0.2444835346451505', '1.0243950182331245', 0, NULL, NULL, NULL, NULL, '2018-07-25 10:10:41', '2018-07-25 10:10:41', 1),
(138, 1, '51958151', '51958151', NULL, 1, NULL, '2', NULL, NULL, NULL, NULL, '8320.343', '0.0002403747057062431', '1.0246353929388308', 0, NULL, NULL, NULL, NULL, '2018-07-25 10:49:53', '2018-07-25 10:49:53', 1),
(139, 1, '76248848', '76248848', NULL, 2, NULL, '832.0340000000001', NULL, NULL, NULL, NULL, '8320.34', '0.1', '0.9246353929388308', 0, NULL, NULL, NULL, NULL, '2018-07-25 10:50:16', '2018-07-25 10:50:16', 1),
(140, 1, '62064012', '62064012', NULL, 4, NULL, '10000', NULL, NULL, NULL, NULL, '1', '10000', '0', 0, NULL, NULL, NULL, NULL, '2018-07-25 10:52:18', '2018-07-25 10:52:18', 0),
(141, 1, 'PAY-7J843531YJ212251ALNMFVRY', '8961633', '8961633', 0, NULL, '11', NULL, '0.62', 'manomitpersonal@gmail.com', 'Jane Doe', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-07-25 11:13:51', '2018-07-25 11:13:51', 0),
(142, 1, '58587754', '58587754', NULL, 0, NULL, '11', NULL, NULL, NULL, NULL, NULL, NULL, '', 2, NULL, NULL, NULL, NULL, '2018-07-26 07:18:33', '2018-07-26 07:18:33', 0),
(143, 1, '67066621', '67066621', NULL, 3, NULL, '100', NULL, NULL, NULL, NULL, NULL, NULL, '0', 3, NULL, NULL, NULL, NULL, '2018-07-26 07:25:32', '2018-07-26 07:25:32', 0),
(144, 1, '82869508', '82869508', NULL, 2, NULL, '826.0020000000001', NULL, NULL, NULL, NULL, '8260.02', '0.1', '0.8246353929388308', 0, NULL, NULL, NULL, NULL, '2018-07-26 10:06:54', '2018-07-26 10:06:54', 1),
(145, 1, '32940573', '32940573', NULL, 1, NULL, '1', NULL, NULL, NULL, NULL, '8260.016', '0.00012106514079391614', '0.8247564580796247', 0, NULL, NULL, NULL, NULL, '2018-07-26 10:07:30', '2018-07-26 10:07:30', 1),
(146, 1, '99711830', '99711830', NULL, 1, NULL, '11111', NULL, NULL, NULL, NULL, '8260.016', '1.3451547793612022', '2.169911237440827', 0, NULL, NULL, NULL, NULL, '2018-07-26 10:07:57', '2018-07-26 10:07:57', 1),
(147, 1, 'PAY-04P46947UN3180301LNNKZ7I', '49983000', '49983000', 0, NULL, '11', NULL, '0.62', 'tapas_me@gmail.com', 'Tapas Sutradhar', NULL, NULL, '', 6, NULL, NULL, NULL, NULL, '2018-07-27 05:26:50', '2018-07-27 05:26:50', 0),
(148, 1, '92725111', '92725111', NULL, 2, NULL, '1628.41', NULL, NULL, NULL, NULL, '8142.05', '.2', '10', 0, NULL, NULL, NULL, NULL, '2018-07-30 07:37:24', '2018-08-29 08:20:03', 1),
(149, 1, '67998749', '67998749', NULL, 1, NULL, '6', NULL, NULL, NULL, NULL, '465.126', '0.012899730395634731', '1.8611636540148822', 0, NULL, NULL, NULL, NULL, '2018-07-30 07:37:35', '2018-07-30 07:37:35', 2),
(150, 1, '73230996', '73230996', NULL, 2, NULL, '127.91399999999999', NULL, NULL, NULL, NULL, '426.38', '.3', '1.561163654014882', 0, NULL, NULL, NULL, NULL, '2018-08-01 09:22:23', '2018-08-01 09:22:23', 2),
(183, 1, '89952287', '89952287', NULL, 6, NULL, '7', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-08-07 12:04:10', '2018-08-07 12:04:10', 0),
(184, 1, '84367045', '84367045', NULL, 4, NULL, '7', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-08-07 12:04:10', '2018-08-07 12:04:10', 0),
(185, 1, '77655879', '77655879', NULL, 6, NULL, '3000', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-08-07 12:22:15', '2018-08-07 12:22:15', 0),
(186, 1, '87963551', '87963551', NULL, 4, NULL, '3000', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-08-07 12:22:15', '2018-08-07 12:22:15', 0),
(187, 1, '31530418', '31530418', NULL, 6, NULL, '2000', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-08-07 12:36:22', '2018-08-07 12:36:22', 0),
(188, 1, '36868627', '36868627', NULL, 4, NULL, '2000', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-08-07 12:36:23', '2018-08-07 12:36:23', 0),
(189, 1, '36088272', '36088272', NULL, 6, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-08-09 06:41:27', '2018-08-09 06:41:27', 0),
(190, 1, '74513877', '74513877', NULL, 4, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-08-09 06:41:27', '2018-08-09 06:41:27', 0),
(191, 1, '98866779', '98866779', NULL, 1, NULL, '5000', NULL, NULL, NULL, NULL, '3.978712', '1256.6880940364622', '1256.6880940364622', 0, NULL, NULL, NULL, NULL, '2018-08-14 11:11:43', '2018-08-14 11:11:43', 45),
(192, 17, '42196641', '42196641', NULL, 1, NULL, '', NULL, NULL, NULL, NULL, '', '6', '.9', 0, NULL, NULL, NULL, NULL, '2018-08-29 09:03:40', '2018-08-29 09:05:03', 17),
(193, 17, '97126590', '97126590', NULL, 1, NULL, '', NULL, NULL, NULL, NULL, '', '0.523', '0.523', 0, NULL, NULL, NULL, NULL, '2018-08-29 09:05:43', '2018-08-29 09:05:43', 9),
(194, 1, '56271466', '56271466', NULL, 1, NULL, '1000', NULL, NULL, NULL, NULL, '279.274623003', '3.580704860495892', '5.141868514510774', 0, NULL, NULL, NULL, NULL, '2018-08-30 10:46:43', '2018-08-30 10:46:43', 2),
(195, 1, '73952039', '73952039', NULL, 2, NULL, '279.15', NULL, NULL, NULL, NULL, '279.15', '1', '4.141868514510774', 0, NULL, NULL, NULL, NULL, '2018-08-30 10:47:21', '2018-08-30 10:47:21', 2),
(196, 1, '6071734', '6071734', NULL, 1, NULL, '1200', NULL, NULL, NULL, NULL, '10.6168357279', '113.02802744197295', '113.02802744197295', 0, NULL, NULL, NULL, NULL, '2018-08-30 10:53:52', '2018-08-30 10:53:52', 47),
(197, 1, '92942564', '92942564', NULL, 2, NULL, '1168.1999999999998', NULL, NULL, NULL, NULL, '10.62', '110', '3.0280274419729523', 0, NULL, NULL, NULL, NULL, '2018-08-30 10:54:43', '2018-08-30 10:54:43', 47),
(198, 1, '44726619', '44726619', NULL, 1, NULL, '4', NULL, NULL, NULL, NULL, '6940.02964146', '0.0005763664143599399', '10.00057636641436', 0, NULL, NULL, NULL, NULL, '2018-08-30 11:22:50', '2018-08-30 11:22:50', 1),
(199, 1, '78829067', '78829067', NULL, 2, NULL, '84.13199999999999', NULL, NULL, NULL, NULL, '280.44', '.3', '3.8418685145107743', 0, NULL, NULL, NULL, NULL, '2018-08-30 11:23:19', '2018-08-30 11:23:19', 2),
(200, 1, '67102581', '67102581', NULL, 2, NULL, '84.02699999999999', NULL, NULL, NULL, NULL, '280.09', '.3', '3.5418685145107744', 0, NULL, NULL, NULL, NULL, '2018-08-30 11:28:45', '2018-08-30 11:28:45', 2),
(201, 1, '19996838', '19996838', NULL, 1, NULL, '10', NULL, NULL, NULL, NULL, '6940.75', '0.0014407664877714944', '10.002017132902132', 0, NULL, NULL, NULL, NULL, '2018-08-30 11:28:57', '2018-08-30 11:28:57', 1),
(202, 1, '81358919', '81358919', NULL, 1, NULL, '', NULL, NULL, NULL, NULL, '', '4', '4', 0, NULL, NULL, NULL, NULL, '2018-08-31 10:10:28', '2018-08-31 10:10:28', 21),
(203, 1, '80276723', '80276723', NULL, 1, NULL, '', NULL, NULL, NULL, NULL, '', '8.5', '8.5', 0, NULL, NULL, NULL, NULL, '2018-08-31 10:10:57', '2018-08-31 10:10:57', 16),
(204, 59, '47873069', '47873069', NULL, 1, NULL, '', NULL, NULL, NULL, NULL, '', '5', '5', 0, NULL, NULL, NULL, NULL, '2018-09-05 09:58:39', '2018-09-05 09:58:39', 14),
(205, 58, '59192345', '59192345', NULL, 0, NULL, '10', NULL, NULL, NULL, NULL, NULL, NULL, '0', 2, NULL, NULL, NULL, NULL, '2018-09-05 10:14:51', '2018-09-05 10:14:51', 0),
(206, 58, '9699938', '9699938', NULL, 1, NULL, '', NULL, NULL, NULL, NULL, '', '6', '6', 0, NULL, NULL, NULL, NULL, '2018-09-05 10:15:38', '2018-09-05 10:15:38', 18),
(208, 16, '35867767', '35867767', NULL, 0, NULL, '0.2', NULL, NULL, NULL, NULL, NULL, NULL, '0', 2, NULL, NULL, NULL, NULL, '2018-09-05 10:36:19', '2018-09-05 10:36:19', 0),
(209, 14, '551297', '551297', NULL, 1, NULL, '', NULL, NULL, NULL, NULL, '', '20', '20', 0, NULL, NULL, NULL, NULL, '2018-09-05 10:37:11', '2018-09-05 10:37:11', 9),
(210, 1, '73769421', '73769421', NULL, 6, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-10-09 07:11:21', '2018-10-09 07:11:21', 0),
(211, 1, '79083267', '79083267', NULL, 4, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-10-09 07:11:21', '2018-10-09 07:11:21', 0),
(212, 1, '42902180', '42902180', NULL, 6, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-10-09 07:18:12', '2018-10-09 07:18:12', 0),
(213, 1, '1465054', '1465054', NULL, 4, NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, '2018-10-09 07:18:12', '2018-10-09 07:18:12', 0),
(214, 1, '98245804', '98245804', NULL, 3, NULL, '3000', NULL, NULL, NULL, NULL, NULL, NULL, '0', 4, NULL, NULL, NULL, NULL, '2018-11-09 07:21:14', '2018-11-09 07:21:14', 0),
(218, 1, '49649745', '49649745', NULL, 3, NULL, '6000', NULL, NULL, NULL, NULL, NULL, NULL, '0', 4, NULL, NULL, NULL, NULL, '2018-11-09 08:18:53', '2018-11-09 08:18:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `deposit_methods`
--

CREATE TABLE IF NOT EXISTS `deposit_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `method_name` varchar(40) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '1: Enabled, 0: Disabled',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `deposit_methods`
--

INSERT INTO `deposit_methods` (`id`, `method_name`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 'Credit Card', 1, '2018-06-06 10:00:00', '2018-07-05 09:32:08'),
(2, 'Bank Wire Transfer', 1, '2018-06-06 10:00:00', '2018-07-06 11:12:06'),
(3, 'PayPal', 1, '2018-06-06 10:00:00', '2018-07-09 06:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `deposit_method_types`
--

CREATE TABLE IF NOT EXISTS `deposit_method_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deposit_method_id` int(11) DEFAULT NULL,
  `ecorepay_account_id` varchar(255) DEFAULT NULL,
  `ecorepay_authentication_id` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `bank_address` text,
  `branch_number` varchar(255) DEFAULT NULL,
  `institution_number` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `routing_number` varchar(255) DEFAULT NULL,
  `swift_code` varchar(255) DEFAULT NULL,
  `reference_email` varchar(255) DEFAULT NULL,
  `paypal_payment_mode` varchar(255) DEFAULT NULL,
  `paypal_client_id` varchar(255) DEFAULT NULL,
  `paypal_client_secret` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `deposit_method_types`
--

INSERT INTO `deposit_method_types` (`id`, `deposit_method_id`, `ecorepay_account_id`, `ecorepay_authentication_id`, `bank_name`, `account_name`, `bank_address`, `branch_number`, `institution_number`, `account_number`, `routing_number`, `swift_code`, `reference_email`, `paypal_payment_mode`, `paypal_client_id`, `paypal_client_secret`, `createdAt`, `updatedAt`) VALUES
(5, 2, NULL, NULL, 'Bank of Nova Scotia', 'Kano Investments Inc', '11666 Steveston Hwy #3020, Richmond, BC, V7A 5J3', '51920', '002', '0091782', '026002532', 'NOSCCATT', 'support@coinjolt.com', NULL, NULL, NULL, '2018-07-19 11:34:28', '2018-07-20 09:52:01'),
(9, 1, '33628851dd', 'oEPOveFHGjcyCtPP33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-20 05:34:54', '2018-07-20 09:51:46'),
(10, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sandbox', 'test', 'test2', '2018-07-20 09:52:51', '2018-07-26 12:23:07');

-- --------------------------------------------------------

--
-- Table structure for table `email_drafts`
--

CREATE TABLE IF NOT EXISTS `email_drafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `user_type` text,
  `status` int(11) DEFAULT NULL COMMENT '1=''Send'',2=''Draft''',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `email_drafts`
--

INSERT INTO `email_drafts` (`id`, `subject`, `body`, `user_type`, `status`, `createdAt`, `updatedAt`) VALUES
(2, 'WOW', '<ol>\n	<li><em><strong>Hello good evening TO BOTH OF YOU....</strong></em></li>\n</ol>\n', '{"0":"sobhan@wrctpl.com"}', 1, '2018-06-06 10:49:22', '2018-06-06 10:59:26'),
(3, 'Testing draft email', '<p>Hello coinjolt team.</p>\n', '{"0":"nilesh@wrctpl.com","1":"sayan@wrctpl.com","2":"sobhan@wrctpl.com","3":"manomit@wrctpl.com","4":"suman@wrctpl.com","5":"koustabh@wrctpl.com","6":"tamashree@gmail.com","7":"amir@wrctpl.com","8":"tamashree@wrctpl.com","9":"nasir@wrctpl.com","10":"malini@wrctpl.com","11":"somenath@wrctpl.com"}', 1, '2018-06-06 11:01:14', '2018-06-06 11:01:52'),
(4, 'Testing as draft', '<p>Hello</p>\n', '{"0":"sobhan@wrctpl.com"}', 2, '2018-06-07 05:55:23', '2018-06-07 05:55:23'),
(5, 'Testing for deposit user only', '<p>Hi.</p>\n\n<p>How are you ?</p>\n', '{"0":"sayan@wrctpl.com","1":"sobhan@wrctpl.com"}', 1, '2018-06-07 09:54:24', '2018-06-07 09:55:06'),
(6, 'assds', '<p>sdasdasdasd</p>\n', '{"0":"sobhan@wrctpl.com"}', 1, '2018-06-07 10:04:05', '2018-06-07 10:04:36'),
(7, 'Testing draft', '<p>Hello all.</p>\n', '{"0":"nilesh@wrctpl.com","1":"sayan@wrctpl.com","2":"sobhan@wrctpl.com"}', 2, '2018-06-07 11:25:49', '2018-06-07 11:25:49'),
(8, 'Testing with all user', '<p>hello</p>\n', '{"0":"nilesh@wrctpl.com","1":"sayan@wrctpl.com","2":"sobhan@wrctpl.com","3":"manomit@wrctpl.com","4":"suman@wrctpl.com","5":"koustabh@wrctpl.com","6":"tamashree@gmail.com","7":"amir@wrctpl.com","8":"tamashree@wrctpl.com","9":"nasir@wrctpl.com","10":"malini@wrctpl.com","11":"somenath@wrctpl.com"}', 2, '2018-06-07 13:12:02', '2018-06-07 13:12:02'),
(9, 'Testing with individuals', '<p>Hello&nbsp;</p>\n', '{"0":"sayan@wrctpl.com","1":"sobhan@wrctpl.com","2":"manomit@wrctpl.com","3":"suman@wrctpl.com"}', 2, '2018-06-07 13:12:53', '2018-06-07 13:12:53'),
(10, 'Testing with deposit', '<p>Hello&nbsp;</p>\n', '{"0":"sobhan@wrctpl.com"}', 1, '2018-06-07 13:13:31', '2018-06-08 07:19:51'),
(11, 'Testing email marketing', '<p>Hello, good morning.<br />\nThanks.</p>\n', '{"0":"sayan@wrctpl.com","1":"sobhan@wrctpl.com"}', 1, '2018-07-05 06:15:54', '2018-07-05 06:16:17');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) DEFAULT NULL,
  `template_desc` text,
  `status` varchar(255) DEFAULT NULL COMMENT '1=''Active'',2=''Not Active''',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `template_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `template_name`, `template_desc`, `status`, `createdAt`, `updatedAt`, `template_type`) VALUES
(1, 'Test time', '<h2>What is Lorem Ipsum?</h2>\n\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n', '2', '2018-05-21 09:33:32', '2018-07-23 10:47:30', NULL),
(2, 'Test 1', '<h2>Why do we use it?</h2>\n\n<p><em><strong>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</strong></em></p>\n', '2', '2018-05-21 09:43:12', '2018-07-23 10:47:27', NULL),
(3, 'Test by sobhan', '<p><em><strong>Hello, my name is Sobhan Das.</strong></em></p>\n', '2', '2018-05-21 09:57:56', '2018-07-23 10:47:22', NULL),
(4, 'Hi', '<p>Test email from coinjolt.</p>\n', '2', '2018-05-21 11:01:22', '2018-07-23 10:47:18', NULL),
(5, 'Coinjolt Support', '<h2>Where does it come from?</h2>\n\n<blockquote>\n<ol>\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\n	<li>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</li>\n	<li>\n	<h2>Why do we use it?</h2>\n	</li>\n	<li>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</li>\n</ol>\n</blockquote>\n\n<p>Thanks &amp; Regards,</p>\n\n<p>Coinjolt Team.</p>\n', '2', '2018-05-23 09:09:54', '2018-07-26 13:22:10', 3),
(6, 'Registration Template', '<h2>Registration Successful</h2>\n\n<p><strong>Congratulation! </strong>You have successfully registered in Coin Jolt. Please click the button or copy and paste the link below into your browser to activate your account.</p>\n', '1', '2018-07-02 11:06:36', '2018-08-10 08:00:52', 1),
(9, 'Forgot Password template', '<h2>Hello,</h2>\n\n<p>We have received your request for a forgotten password. Please click the button or copy and paste the link below into your browser to reset your password.</p>\n', '1', '2018-07-04 11:38:01', '2018-07-11 11:22:13', 2),
(11, 'Test', '<p>Test</p>\n', '2', '2018-07-23 09:42:09', '2018-07-23 10:47:34', 3),
(12, 'Coinjolt Other', '<h2>Where does it come from?</h2>\n\n<blockquote>\n<ol>\n	<li>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</li>\n	<li>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</li>\n	<li>\n	<h2>Why do we use it?</h2>\n	</li>\n	<li>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</li>\n</ol>\n</blockquote>\n\n<p>Thanks &amp; Regards,</p>\n\n<p>Coinjolt Team.</p>\n', '1', '2018-07-26 13:23:49', '2018-08-10 07:14:36', 3),
(13, 'Account Activation Template', '<h2>Account Activation Pending</h2>\n\n<p><strong>Welcome!</strong><strong> </strong>to Coin Jolt.<strong> </strong>Please click the button or copy and paste the link below into your browser to activate your account.</p>\n', '1', '2018-09-04 09:42:06', '2018-09-04 09:42:06', 4);

-- --------------------------------------------------------

--
-- Table structure for table `email_template_types`
--

CREATE TABLE IF NOT EXISTS `email_template_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `email_template_types`
--

INSERT INTO `email_template_types` (`id`, `type`, `createdAt`, `updatedAt`) VALUES
(1, 'Registration', '2018-07-02 14:00:00', '2018-07-02 14:00:00'),
(2, 'Forgot Password', '2018-07-04 14:26:00', '2018-07-04 14:26:00'),
(3, 'Others', '2018-07-04 15:20:00', '2018-07-04 15:20:00'),
(4, 'Account Activation', '2018-09-04 15:00:00', '2018-09-04 16:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `forgot_passwords`
--

CREATE TABLE IF NOT EXISTS `forgot_passwords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text,
  `ip_address` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `status` int(11) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `forgot_passwords`
--

INSERT INTO `forgot_passwords` (`id`, `key`, `ip_address`, `createdAt`, `updatedAt`, `status`, `user_email`) VALUES
(2, '53a1fab9132279211a48', '::ffff:127.0.0.1', '2018-06-14 12:53:37', '2018-06-15 05:09:27', 1, 'nilesh@wrctpl.com'),
(3, '52acf59f32447f563653', '::ffff:127.0.0.1', '2018-06-15 05:10:47', '2018-06-15 05:11:49', 1, 'nilesh@wrctpl.com'),
(4, '3389829a12416c42346f', '::ffff:127.0.0.1', '2018-06-15 05:18:09', '2018-06-15 05:19:04', 1, 'nilesh@wrctpl.com'),
(5, '02a3d98838575177236a', '::ffff:127.0.0.1', '2018-06-15 05:25:23', '2018-06-15 05:26:02', 1, 'nilesh@wrctpl.com'),
(6, '208b86a173547a7e376e', '::ffff:127.0.0.1', '2018-06-15 05:36:27', '2018-06-15 05:37:16', 1, 'nilesh@wrctpl.com'),
(7, '328bc39f2c5c2c421c7f', '::ffff:127.0.0.1', '2018-06-15 05:45:02', '2018-06-15 05:46:31', 1, 'nilesh@wrctpl.com'),
(8, '2dafe389777d4149696a', '::ffff:127.0.0.1', '2018-06-15 06:01:55', '2018-06-15 06:02:41', 1, 'nilesh@wrctpl.com'),
(9, '579c8dc011602b291b4d', '::ffff:127.0.0.1', '2018-06-15 06:24:04', '2018-06-15 06:25:17', 1, 'nilesh@wrctpl.com'),
(10, '2f9dd2b8096020601644', '::ffff:127.0.0.1', '2018-06-15 06:34:18', '2018-06-15 06:35:01', 1, 'nilesh@wrctpl.com'),
(11, '0ca48d84757a4e490368', '::ffff:127.0.0.1', '2018-06-15 11:25:36', '2018-06-15 11:26:04', 1, 'nilesh@wrctpl.com'),
(12, '5386d1a1294252546544', '::ffff:127.0.0.1', '2018-06-15 11:26:31', '2018-06-15 11:26:51', 1, 'nilesh@wrctpl.com'),
(13, '50d8f183367859433451', '::1', '2018-07-05 05:40:34', '2018-07-05 05:40:34', 0, 'tamashree@wrctpl.com'),
(14, '2cbfdd9611777a68294b', '::1', '2018-07-05 07:40:06', '2018-07-05 07:40:06', 0, 'sobhan@wrctpl.com'),
(15, '1cdef0bc73424b621364', '::ffff:127.0.0.1', '2018-07-05 08:50:45', '2018-07-05 08:50:45', 0, 'nilesh@wrctpl.com'),
(16, '2a96f0bd30245a77004b', '::ffff:127.0.0.1', '2018-07-05 11:07:28', '2018-07-05 11:07:28', 0, 'nilesh@wrctpl.com'),
(17, '2b82c7ba195920676763', '::ffff:127.0.0.1', '2018-07-05 11:09:11', '2018-07-05 11:09:34', 1, 'nilesh@wrctpl.com'),
(18, '22a8e49e15692e21267c', '::ffff:127.0.0.1', '2018-07-05 11:10:57', '2018-07-05 11:11:39', 1, 'nilesh@wrctpl.com'),
(19, '1ca9ee8417422065031c', '::ffff:127.0.0.1', '2018-07-11 07:31:17', '2018-07-11 07:52:25', 1, 'nilesh@wrctpl.com'),
(20, '2683d9a9184268602a1f', '::ffff:127.0.0.1', '2018-07-11 08:35:44', '2018-07-11 08:39:57', 1, 'nilesh@wrctpl.com'),
(21, '25b6f8c771765c71216c', '::ffff:127.0.0.1', '2018-07-11 09:51:44', '2018-07-11 09:51:44', 0, 'nilesh@wrctpl.com'),
(22, '179bfca373492969656e', '::ffff:127.0.0.1', '2018-07-11 09:53:54', '2018-07-11 09:53:54', 0, 'nilesh@wrctpl.com'),
(23, '2b9af89c0f20427d1b44', '::ffff:127.0.0.1', '2018-07-11 10:04:06', '2018-07-11 10:04:06', 0, 'nilesh@wrctpl.com'),
(24, '51bcc38108655763294c', '::ffff:127.0.0.1', '2018-07-11 10:05:01', '2018-07-11 10:05:01', 0, 'nilesh@wrctpl.com'),
(25, '5488cc86064969443a53', '::ffff:127.0.0.1', '2018-07-11 10:07:52', '2018-07-11 10:07:52', 0, 'nilesh@wrctpl.com'),
(26, '2bafdeb90c232f46655d', '::ffff:127.0.0.1', '2018-07-11 10:25:30', '2018-07-11 10:25:30', 0, 'nilesh@wrctpl.com'),
(27, '0f9bc59b19442a64091d', '::ffff:127.0.0.1', '2018-07-11 10:26:53', '2018-07-11 10:26:53', 0, 'nilesh@wrctpl.com'),
(28, '31d9c3a1167b516a1c19', '::ffff:127.0.0.1', '2018-07-11 10:35:47', '2018-07-11 10:35:47', 0, 'nilesh@wrctpl.com'),
(29, '1d9ff0c40b7f51212465', '::ffff:127.0.0.1', '2018-07-11 10:37:43', '2018-07-11 10:37:43', 0, 'tamashree@wrctpl.com'),
(30, '259b828109245f280668', '::ffff:127.0.0.1', '2018-07-11 10:42:58', '2018-07-11 10:42:58', 0, 'tamashree@wrctpl.com'),
(31, '2d8dc496725f7f572640', '::ffff:127.0.0.1', '2018-07-11 10:45:36', '2018-07-11 10:45:36', 0, 'tamashree@wrctpl.com'),
(32, '31a1d5a733747c26354c', '::ffff:127.0.0.1', '2018-07-11 10:52:35', '2018-07-11 10:52:35', 0, 'tamashree@wrctpl.com'),
(33, '0a82fe9434477625025d', '::ffff:127.0.0.1', '2018-07-11 10:54:04', '2018-07-11 10:54:04', 0, 'nilesh@wrctpl.com'),
(34, '3d9b83a3392274673341', '::ffff:127.0.0.1', '2018-07-11 11:01:27', '2018-07-11 11:01:27', 0, 'tamashree@wrctpl.com'),
(35, '0ad681b31b5b72552840', '::ffff:127.0.0.1', '2018-07-11 11:03:13', '2018-07-11 11:03:13', 0, 'nilesh@wrctpl.com'),
(36, '2ca4e68675287d24281e', '::ffff:127.0.0.1', '2018-07-11 11:07:50', '2018-07-11 11:07:50', 0, 'nasir@wrctpl.com'),
(37, '50dafc8814785e426662', '::ffff:127.0.0.1', '2018-07-11 11:18:07', '2018-07-11 11:18:07', 0, 'tamashree@wrctpl.com'),
(38, '1294df80706b2f7f6319', '::ffff:127.0.0.1', '2018-07-11 11:22:36', '2018-07-11 11:22:36', 0, 'tamashree@wrctpl.com'),
(39, '289cc698267f2e572662', '::ffff:127.0.0.1', '2018-07-11 11:22:54', '2018-07-11 11:22:54', 0, 'nilesh@wrctpl.com'),
(40, '31abdd9e0d41727d2567', '::ffff:127.0.0.1', '2018-07-11 11:27:56', '2018-07-11 11:27:56', 0, 'tamashree@wrctpl.com'),
(41, '3ca0c2c604415c761d70', '::ffff:127.0.0.1', '2018-07-11 11:28:55', '2018-07-11 11:28:55', 0, 'tamashree@wrctpl.com'),
(42, '52bd8280097c4d582542', '::ffff:127.0.0.1', '2018-07-11 11:29:30', '2018-07-11 11:29:30', 0, 'tamashree@wrctpl.com'),
(43, '5580c0c2147c2958657a', '::ffff:127.0.0.1', '2018-07-11 11:30:45', '2018-07-11 11:30:45', 0, 'nilesh@wrctpl.com'),
(44, '06a18c9b1749775b3e11', '::ffff:127.0.0.1', '2018-07-11 11:36:20', '2018-07-11 11:36:20', 0, 'nasir@wrctpl.com'),
(45, '11a1e0a5112577242266', '::ffff:127.0.0.1', '2018-07-11 11:42:13', '2018-07-11 11:42:13', 0, 'nasir@wrctpl.com'),
(46, '2ea6feb204654d27251b', '::ffff:127.0.0.1', '2018-07-11 11:42:25', '2018-07-11 11:42:25', 0, 'tamashree@wrctpl.com'),
(47, '1ddbe1952f527e21204e', '::ffff:127.0.0.1', '2018-07-11 11:42:56', '2018-07-11 11:42:56', 0, 'nilesh@wrctpl.com'),
(48, '12878c982d452c211a5d', '::ffff:127.0.0.1', '2018-07-11 11:45:02', '2018-07-11 11:45:02', 0, 'nilesh@wrctpl.com'),
(49, '0fa7e0a615266b401864', '::ffff:127.0.0.1', '2018-07-11 11:52:46', '2018-07-11 11:52:46', 0, 'nasir@wrctpl.com'),
(50, '0c8ac5910567215e3a5d', '::ffff:127.0.0.1', '2018-07-11 12:00:20', '2018-07-11 12:00:20', 0, 'nasir@wrctpl.com'),
(51, '2ab9829224747c52375a', '::ffff:127.0.0.1', '2018-07-11 12:03:10', '2018-07-11 12:03:10', 0, 'nasir@wrctpl.com'),
(52, '1d9af59936225b59315f', '::ffff:127.0.0.1', '2018-07-11 12:04:01', '2018-07-11 12:04:01', 0, 'nasir@wrctpl.com'),
(53, '01b8e5b53661515c3c67', '::ffff:127.0.0.1', '2018-07-11 12:04:48', '2018-07-11 12:04:48', 0, 'nilesh@wrctpl.com'),
(54, '14ab85c117625447167f', '::ffff:127.0.0.1', '2018-07-11 12:07:12', '2018-07-11 12:07:12', 0, 'nasir@wrctpl.com'),
(55, '10a1c0c4257620253c1a', '::ffff:127.0.0.1', '2018-07-11 12:07:53', '2018-07-11 12:07:53', 0, 'nilesh@wrctpl.com'),
(56, '3582dc9c0e5562240653', '::1', '2018-07-12 05:06:30', '2018-07-12 05:06:30', 0, 'tamashree@wrctpl.com'),
(57, '2edee6a6092776646710', '::1', '2018-07-12 05:37:21', '2018-07-12 05:37:21', 0, 'tamashree@wrctpl.com'),
(58, '3d82fd840b782c5c087b', '::1', '2018-07-12 05:38:47', '2018-07-12 05:38:47', 0, 'tamashree@wrctpl.com'),
(59, '2787daaa75752168361d', '::1', '2018-07-12 05:44:21', '2018-07-12 05:44:21', 0, 'tamashree@wrctpl.com'),
(60, '0e84edbf0b237544211e', '::1', '2018-07-12 05:51:04', '2018-07-12 05:51:04', 0, 'tamashree@wrctpl.com'),
(61, '55daf6a105694b510753', '::1', '2018-07-12 05:58:25', '2018-07-12 05:58:25', 0, 'tamashree@wrctpl.com'),
(62, '228c80a9066653771e51', '::1', '2018-07-12 05:59:54', '2018-07-12 05:59:54', 0, 'nilesh@wrctpl.com'),
(63, '21dcc3b10e7670250763', '::1', '2018-07-12 06:00:55', '2018-07-12 06:00:55', 0, 'tamashree@wrctpl.com'),
(64, '2e998cbb154b4c5c2879', '::1', '2018-07-12 06:01:19', '2018-07-12 06:01:19', 0, 'tamashree@wrctpl.com'),
(65, '0dbfecc70a587560145a', '::1', '2018-07-12 06:01:45', '2018-07-12 06:01:45', 0, 'tamashree@wrctpl.com'),
(66, '14b685bc78287b211a73', '::1', '2018-07-12 06:02:24', '2018-07-12 06:02:24', 0, 'tamashree@wrctpl.com'),
(67, '32dceda670617f23357a', '::1', '2018-07-12 06:09:27', '2018-07-12 06:09:27', 0, 'tamashree@wrctpl.com'),
(68, '08d681b3367e55613c7b', '::1', '2018-07-12 06:10:10', '2018-07-12 06:10:10', 0, 'tamashree@wrctpl.com'),
(69, '01bcddb908455223605e', '::1', '2018-07-12 06:11:46', '2018-07-12 06:12:18', 1, 'tamashree@wrctpl.com'),
(70, '5cb6c4b8235e5d733f45', '::1', '2018-07-12 06:19:28', '2018-07-12 06:19:28', 0, 'tamashree@wrctpl.com'),
(71, '1cd8fea571276c7a0858', '::1', '2018-07-12 10:37:56', '2018-07-12 10:37:56', 0, 'tamashree@wrctpl.com'),
(72, '54a5f2be29547d673411', '::ffff:127.0.0.1', '2018-07-13 08:46:03', '2018-07-13 08:46:03', 0, 'nilesh1@wrctpl.com'),
(73, '0dadc3a4077372473f1c', '::ffff:127.0.0.1', '2018-07-13 08:46:54', '2018-07-13 08:46:54', 0, 'nilesh@wrctpl.com'),
(74, '1ebfdd88737f4126195c', '::ffff:127.0.0.1', '2018-07-13 08:49:59', '2018-07-13 08:49:59', 0, 'nilesh@wrctpl.com'),
(75, '3094fb8309624a681f6f', '::1', '2018-07-13 11:42:31', '2018-07-13 11:42:31', 0, 'tamashree@wrctpl.com'),
(76, '148883bb176148663942', '::ffff:127.0.0.1', '2018-08-07 08:34:52', '2018-08-07 08:34:52', 0, 'nilesh@wrctpl.com'),
(77, '0ea2d28808474a791e4b', '::ffff:127.0.0.1', '2018-08-07 08:41:46', '2018-08-07 08:41:46', 0, 'nilesh@wrctpl.com'),
(78, '2e8bfbbe06674d62361c', '::ffff:127.0.0.1', '2018-08-07 08:45:11', '2018-08-07 08:45:11', 0, 'nilesh@wrctpl.com'),
(79, '22bcf3b42547515b3850', '::ffff:127.0.0.1', '2018-08-07 09:58:45', '2018-08-07 09:58:45', 0, 'nilesh@wrctpl.com'),
(80, '3386e7830c566d45221f', '::ffff:127.0.0.1', '2018-08-07 14:36:50', '2018-08-07 14:36:50', 0, 'nilesh@wrctpl.com'),
(81, '2986d081725c69571d6e', '::ffff:127.0.0.1', '2018-08-07 14:37:53', '2018-08-07 14:37:53', 0, 'nilesh@wrctpl.com'),
(82, '3cbbc1a322657a61277f', '::ffff:127.0.0.1', '2018-08-08 04:52:41', '2018-08-08 04:52:41', 0, 'nilesh@wrctpl.com'),
(83, '529de4a235437e59677e', '::ffff:127.0.0.1', '2018-08-08 05:53:51', '2018-08-08 05:53:51', 0, 'nilesh@wrctpl.com'),
(84, '3da1f18000632f64157a', '::1', '2018-08-30 11:15:55', '2018-08-30 11:15:55', 0, 'tamashree@wrctpl.com');

-- --------------------------------------------------------

--
-- Table structure for table `kyc_details`
--

CREATE TABLE IF NOT EXISTS `kyc_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `files` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1-''KYC Sumitted'',2-''KYC Approved'',3-''KYC Rejected''',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `kyc_details`
--

INSERT INTO `kyc_details` (`id`, `user_id`, `files`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'https://s3.amazonaws.com/coinjoltdev2018/id-proof/1-1525693111961.jpg', 2, '2018-05-03 07:27:08', '2018-05-03 10:26:42'),
(2, 1, 'https://s3.amazonaws.com/coinjoltdev2018/id-proof/1-1525698391753.jpg', 1, '0000-00-00 00:00:00', '2018-07-12 11:13:59'),
(3, 1, 'https://s3.amazonaws.com/coinjoltdev2018/id-proof/1-1526977849943.png', 2, '2018-05-22 08:30:51', '2018-05-23 13:05:16'),
(4, 1, 'https://s3.amazonaws.com/coinjoltdev2018/id-proof/1-1526977891517.jpg', 2, '2018-05-22 08:31:34', '2018-07-04 09:56:08'),
(5, 63, 'https://s3.amazonaws.com/coinjoltdev2018/id-proof/63-1529574862340.png', 1, '2018-06-21 09:54:23', '2018-07-12 11:14:28');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(300) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `option`, `question_id`, `createdAt`, `updatedAt`) VALUES
(1, 'I want to have enough money to retire.', 1, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(2, 'I''m looking to create a strong financial asset portfolio.', 1, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(3, 'I want to have passive income paid out monthly as form of dividends.', 1, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(4, 'I''m looking to double my investment.', 1, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(5, 'I''m interested in creating wealth.', 1, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(6, 'Low', 2, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(7, 'Medium', 2, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(8, 'High', 2, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(9, 'Very high', 2, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(10, 'Short term', 3, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(11, 'Long term', 3, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(12, '$100', 4, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(13, '$10,000', 4, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(14, '$100,000', 4, '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(15, '$1,000,000 or more', 4, '2018-05-03 06:24:37', '2018-05-03 06:24:37');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_calculations`
--

CREATE TABLE IF NOT EXISTS `portfolio_calculations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `total_amount_invested` varchar(255) DEFAULT NULL,
  `first_year_earnings` varchar(255) DEFAULT NULL,
  `dividend_payouts` varchar(255) DEFAULT NULL,
  `interest_earned` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `portfolio_calculations`
--

INSERT INTO `portfolio_calculations` (`id`, `user_id`, `total_amount_invested`, `first_year_earnings`, `dividend_payouts`, `interest_earned`, `createdAt`, `updatedAt`) VALUES
(1, 1, '193000', '579000', '5211000', '289.5', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(2, 4, '45242', '135726', '1221534', '67.863', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(3, 11, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(4, 14, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(5, 16, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(6, 17, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(7, 20, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(8, 50, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(9, 51, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(10, 58, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(11, 59, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(12, 61, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06'),
(13, 62, '0', '0', '0', '0', '2018-06-01 05:19:06', '2018-06-01 05:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_compositions`
--

CREATE TABLE IF NOT EXISTS `portfolio_compositions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `investor_type` int(11) DEFAULT NULL COMMENT '1=Institutional Investor, 2=Individual Investor',
  `business_name` varchar(255) DEFAULT NULL,
  `business_number` varchar(255) DEFAULT NULL,
  `business_registration_country` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `residence_country` varchar(255) DEFAULT NULL,
  `investques` varchar(255) DEFAULT NULL,
  `settlement_currency` varchar(255) DEFAULT NULL,
  `street` varchar(5000) DEFAULT NULL,
  `city` varchar(5000) DEFAULT NULL,
  `postal_code` varchar(5000) DEFAULT NULL,
  `state` varchar(5000) DEFAULT NULL,
  `incorporation_certificate` varchar(255) DEFAULT NULL,
  `shareholders` varchar(255) DEFAULT NULL,
  `shareholders_address` varchar(255) DEFAULT NULL,
  `shareholders_id` varchar(255) DEFAULT NULL,
  `government_issued_id` varchar(255) DEFAULT NULL,
  `address_proof` varchar(255) DEFAULT NULL,
  `bank_statement` varchar(255) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `bank_country` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `routing_number` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1= reviewed, 0= not reviewed',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `portfolio_compositions`
--

INSERT INTO `portfolio_compositions` (`id`, `user_id`, `investor_type`, `business_name`, `business_number`, `business_registration_country`, `first_name`, `last_name`, `residence_country`, `investques`, `settlement_currency`, `street`, `city`, `postal_code`, `state`, `incorporation_certificate`, `shareholders`, `shareholders_address`, `shareholders_id`, `government_issued_id`, `address_proof`, `bank_statement`, `account_name`, `bank_country`, `account_number`, `routing_number`, `phone_number`, `email_address`, `status`, `createdAt`, `updatedAt`) VALUES
(11, 1, 2, '', '', '', 'Nilesh ', 'Sanyal', 'india', '10,000,000+', 'flat', '', '', '', '', 'https://s3.amazonaws.com/coinjoltdev2018/cert_incorp/voter-card.png', NULL, NULL, NULL, 'https://s3.amazonaws.com/coinjoltdev2018/govt_id/user-9.jpg', 'https://s3.amazonaws.com/coinjoltdev2018/address_proof/test.txt', 'https://s3.amazonaws.com/coinjoltdev2018/bank_statement/test.txt', 'Nilesh Sanyal', 'India', '7777777', '999999999', '', '', 0, '2018-05-10 06:26:37', '2018-10-09 07:24:05'),
(12, 58, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'SDGSZDGSDG', 'test', '1234567', '123456789', '1234567890', 'rj.test@gmail.com', 0, '2018-08-10 06:23:29', '2018-08-10 06:50:54');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(500) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `createdAt`, `updatedAt`) VALUES
(1, 'What are your financial goals?', '2018-05-03 06:24:37', '2018-05-03 06:24:37'),
(2, 'What sort of risk preference would you like for us to allocate your capital?', '2018-05-03 06:34:37', '2018-05-03 06:34:37'),
(3, 'Are you a short term or long term investor?', '2018-05-03 06:44:37', '2018-05-03 06:44:37'),
(4, 'How much are you willing to invest in cryptocurrency?', '2018-05-03 06:54:37', '2018-05-03 06:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `referral_data`
--

CREATE TABLE IF NOT EXISTS `referral_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `referral_id` int(11) DEFAULT NULL,
  `deposit_amount` decimal(10,2) DEFAULT NULL,
  `referral_amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `referral_data`
--

INSERT INTO `referral_data` (`id`, `createdAt`, `updatedAt`, `user_id`, `referral_id`, `deposit_amount`, `referral_amount`) VALUES
(5, '2018-04-24 07:32:54', '2018-04-24 07:32:54', 1, 16, '2000.00', '20.00'),
(6, '2018-04-25 08:32:48', '2018-04-25 08:32:48', 1, 16, '20000.00', '180.00'),
(7, '2018-04-26 10:28:27', '2018-04-26 09:36:38', 17, 1, '2000.00', '20.00'),
(8, '2018-04-26 09:42:22', '2018-04-26 07:24:37', 11, 1, '100.00', '10.00'),
(9, '2018-07-12 08:19:50', '2018-07-12 08:19:50', 20, NULL, '9999.00', '99.99'),
(10, '2018-07-12 08:31:15', '2018-07-12 08:31:15', 20, NULL, '1224.00', '12.24'),
(11, '2018-07-12 09:07:21', '2018-07-12 09:07:21', 20, NULL, '1562.00', '15.62'),
(12, '2018-07-12 09:15:23', '2018-07-12 09:15:23', 20, NULL, '1399.00', '13.99'),
(15, '2018-07-12 09:54:01', '2018-07-12 09:54:01', 20, NULL, '1999.00', '10.00'),
(16, '2018-07-12 10:12:34', '2018-07-12 10:12:34', 20, NULL, '1299.00', '12.99'),
(17, '2018-07-12 10:15:02', '2018-07-12 10:15:02', 20, NULL, '1299.00', '12.99'),
(18, '2018-07-12 10:25:15', '2018-07-12 10:25:15', 20, 1, '1299.00', '12.99'),
(19, '2018-07-26 07:18:33', '2018-07-26 07:18:33', 20, 16, '11.00', '0.11');

-- --------------------------------------------------------

--
-- Table structure for table `send_emails`
--

CREATE TABLE IF NOT EXISTS `send_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email_template_id` int(11) DEFAULT NULL,
  `email_sub` varchar(255) NOT NULL,
  `email_desc` text NOT NULL,
  `send_by_id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `send_email_address` varchar(255) DEFAULT NULL,
  `email_send_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `send_emails`
--

INSERT INTO `send_emails` (`id`, `user_id`, `email_template_id`, `email_sub`, `email_desc`, `send_by_id`, `createdAt`, `updatedAt`, `send_email_address`, `email_send_status`) VALUES
(1, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan@mailinator.com', NULL),
(2, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan1@mailinator.com', NULL),
(3, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan2@mailinator.com', NULL),
(4, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan3@mailinator.com', NULL),
(5, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan4@mailinator.com', NULL),
(6, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan5@mailinator.com', NULL),
(7, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan7@mailinator.com', NULL),
(8, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan6@mailinator.com', NULL),
(9, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan8@mailinator.com', NULL),
(10, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan9@mailinator.com', NULL),
(11, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan10@mailinator.com', NULL),
(12, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan11@mailinator.com', NULL),
(13, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan12@mailinator.com', NULL),
(14, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan13@mailinator.com', NULL),
(15, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan14@mailinator.com', NULL),
(16, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan15@mailinator.com', NULL),
(17, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan16@mailinator.com', NULL),
(18, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan17@mailinator.com', NULL),
(19, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan18@mailinator.com', NULL),
(20, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan19@mailinator.com', NULL),
(21, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan20@mailinator.com', NULL),
(22, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sobhan@wrctpl.com', NULL),
(23, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'suman@wrctpl.com', NULL),
(24, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'sayan@wrctpl.com', NULL),
(25, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'nilesh@wrctpl.com', NULL),
(26, 0, NULL, 'Test again', '<p>Hello</p>\n', 20, '2018-08-16 10:57:22', '2018-08-16 10:57:22', 'manomit@wrctpl.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sequelizemeta`
--

CREATE TABLE IF NOT EXISTS `sequelizemeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sequelizemeta`
--

INSERT INTO `sequelizemeta` (`name`) VALUES
('20180417060529-create-currency.js'),
('20180417061345-create-country.js'),
('20180417062115-changed_column_country_table.js'),
('20180417063132-add_and_delete_col_from_currencies.js'),
('20180417064137-delete_col_from_currencies.js'),
('20180417064549-add_and_delete_col_from_currencies.js'),
('20180417064846-changed_column_country_table.js'),
('20180417065125-delete_col_from_currencies.js'),
('20180417065553-delete_col_from_currencies.js'),
('20180417070338-bulk_insert_country_table.js'),
('20180417070435-delete_col_from_currencies.js'),
('20180418072114-create-user.js'),
('20180418121540-create-deposit.js'),
('20180419062631-create-support.js'),
('20180419063214-edited_deposits_table.js'),
('20180419094718-create-wire-transfer.js'),
('20180424054835-create-referral-data.js'),
('20180424055430-modified_referral_data_table_structure.js'),
('20180424060409-modified_referral_data_table_structure.js'),
('20180424085749-added_column_deposits_table.js'),
('20180425064738-removed_column_deposits.js'),
('20180425065212-removed_column_deposits.js'),
('20180425065318-removed_column_deposits.js'),
('20180425065546-removed_column_deposits.js'),
('20180425065810-removed_column_deposits.js'),
('20180425070014-removed_column_deposits.js'),
('20180425070438-removed_column_deposits.js'),
('20180425070813-removed_column_deposits.js'),
('20180425071029-removed_column_deposits.js'),
('20180425071119-removed_column_deposits.js'),
('20180425071238-removed_column_deposits.js'),
('20180425071357-removed_column_deposits.js'),
('20180425071606-removed_column_deposits.js'),
('20180425071802-removed_column_deposits.js'),
('20180425071954-removed_column_deposits.js'),
('20180425072328-removed_column_deposits.js'),
('20180425072604-removed_column_deposits.js'),
('20180425073214-removed_column_deposits.js'),
('20180425073250-removed_column_deposits.js'),
('20180425073519-added_column_deposits_table.js'),
('20180425073819-added_column_deposits_table.js'),
('20180425073939-added_column_deposits_table.js'),
('20180425115220-create-withdraw.js'),
('20180427105012-add-column-to-user-table.js'),
('20180427105353-add-column-to-user-table.js'),
('20180427105835-change-columntype-in-user-table.js'),
('20180502065018-add_field_to_users_table.js'),
('20180502130613-create-bank-details.js'),
('20180503071847-create-kyc-details.js'),
('20180503073054-create-kyc-details.js'),
('20180503121557-create-question.js'),
('20180503122117-modified_questions_table.js'),
('20180503122748-create-option.js'),
('20180503122923-modified_options_table.js'),
('20180503123452-insert_data_questions_table.js'),
('20180503124320-insert_data_answers_table.js'),
('20180503130612-create-answer.js'),
('20180504065554-add_field_to_support_table.js'),
('20180504085512-modified_answers_table.js'),
('20180508092801-add_field_to_user_table.js'),
('20180509061439-create-portfolio-composition.js'),
('20180509062050-modified_column_portfoliocomposition_table.js'),
('20180509062712-create-shareholder.js'),
('20180509062829-modified_column_shareholder_table.js'),
('20180510110654-create-currency-balance.js'),
('20180511114258-add-otp-to-user.js'),
('20180514082401-added_fields_portfolio_compositions_table.js'),
('20180521054558-create-wallet.js'),
('20180521055543-create-wallet-address.js'),
('20180521091247-create-email-template.js'),
('20180521102550-add_field_to_wallet_table.js'),
('20180522064841-create-send-email.js'),
('20180529091010-modified_countries_table.js'),
('20180531100250-create-wallet-transaction.js'),
('20180601063213-add_column_to_wallet_transaction.js'),
('20180601095524-create-portfolio-calculation.js'),
('20180606050533-create-deposit-method.js'),
('20180606051001-modified_column_deposit_methods.js'),
('20180606054045-create-email-draft.js'),
('20180611115819-create-cms-about-us.js'),
('20180611124657-create-cms-terms-of-service.js'),
('20180612085030-create-cms-risk-disclosures.js'),
('20180612100704-create-cms-privacy-policy.js'),
('20180613085340-create-company-setting.js'),
('20180613090153-modify_company_settings_table.js'),
('20180614065903-add_field_to_users_table.js'),
('20180614095122-create-forgot-password.js'),
('20180614113840-modified_forgot_password_table.js'),
('20180614115318-modified_forgot_password_table.js'),
('20180625052826-create-blog-post.js'),
('20180625093846-add_column_to_user.js'),
('20180626050019-modified_table__blog_posts.js'),
('20180626053712-modified_table_blog_posts.js'),
('20180626054033-modified_table_blog_posts.js'),
('20180626084928-create-blog-category.js'),
('20180702094026-modified_email_templates_table.js'),
('20180702094643-create-email-template-type.js'),
('20180703090453-added_column_blog_posts_table.js'),
('20180703090844-added_column_blog_posts_table.js'),
('20180703090942-added_column_blog_posts_table.js'),
('20180703091104-added_column_blog_posts_table.js'),
('20180712064331-create-cms-home-page.js'),
('20180716121832-add_field_home_page_table.js'),
('20180718070449-create-deposit-method-type.js'),
('20180720113941-add_filed_to_cms_home_table.js'),
('20180723112515-add_field_to_home_cms_table.js'),
('20180723120938-add_field_to_home_cms_table_again.js'),
('20180725094733-add_field_to_wallet_table.js'),
('20180725113637-add_field_to_wallet_table.js'),
('20180801122828-added_column_blog_posts_table.js'),
('20180802060009-added_column_authorimg_blog_posts.js'),
('20180802090140-create-author.js'),
('20180803051114-create-cold-wallet-balance.js'),
('20180804085239-add_field_to_userTable.js'),
('20180809065247-add_filed_for_2fa_on_user_table.js'),
('20180809124351-added_column_send_emails_table.js'),
('20180816070616-add_filed_to_send_emails_table.js');

-- --------------------------------------------------------

--
-- Table structure for table `shareholders`
--

CREATE TABLE IF NOT EXISTS `shareholders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(500) DEFAULT NULL,
  `shareholder_name` varchar(500) DEFAULT NULL,
  `address_proof` varchar(5000) DEFAULT NULL,
  `government_issued_id` varchar(5000) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `shareholders`
--

INSERT INTO `shareholders` (`id`, `user_id`, `shareholder_name`, `address_proof`, `government_issued_id`, `createdAt`, `updatedAt`) VALUES
(28, '1', 'Test 1', 'https://s3.amazonaws.com/coinjoltdev2018/address_proof/download.jpg', 'https://s3.amazonaws.com/coinjoltdev2018/govt_id/maxresdefault.jpg', '2018-08-01 09:59:49', '2018-08-01 09:59:49'),
(29, '1', 'Test 2', 'https://s3.amazonaws.com/coinjoltdev2018/address_proof/Jellyfish.jpg', 'https://s3.amazonaws.com/coinjoltdev2018/govt_id/Tulips.jpg', '2018-08-01 09:59:53', '2018-08-01 09:59:53');

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

CREATE TABLE IF NOT EXISTS `supports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `enquiry` text,
  `status` int(11) DEFAULT '0' COMMENT '0-''OPEN'',1-''CLOSED''',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=155 ;

--
-- Dumping data for table `supports`
--

INSERT INTO `supports` (`id`, `user_id`, `title`, `enquiry`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Test', 'Desc', 1, '2018-04-19 06:54:36', '2018-05-04 09:19:55'),
(2, 4, 'tt', 'AAAAAAAAAAAWWWWWWWWWWW', 1, '2018-04-19 06:57:05', '2018-05-04 07:38:30'),
(3, 1, 'Test Subject', 'Test Description Again', 0, '2018-04-19 07:10:56', '2018-04-19 07:10:56'),
(5, 1, 'Demo', 'Test', 0, '2018-04-19 11:13:16', '2018-04-19 11:13:16'),
(6, 1, 'Test', 'TESt', 0, '2018-05-02 09:31:39', '2018-05-02 09:31:39'),
(7, 1, 'Testing', 'What''s your name ? ', 0, '2018-05-03 10:31:23', '2018-05-04 07:16:04'),
(8, 1, 'sd', 'fdfdfdgfd', 0, '2018-05-10 05:57:50', '2018-05-10 05:57:50'),
(9, 1, 'v', 'xvcvx', 0, '2018-05-10 05:59:02', '2018-05-10 05:59:02'),
(10, 1, 'Testing Support', 'Submit via ajax.', 0, '2018-05-10 08:46:01', '2018-05-10 08:46:01'),
(11, 1, 'Testing Support', 'Submit via ajax.', 0, '2018-05-10 08:46:11', '2018-05-10 08:46:11'),
(12, 1, '', '', 0, '2018-05-10 08:47:55', '2018-05-10 08:47:55'),
(13, 1, 'Hi', 'Hello sobhan.', 0, '2018-05-10 08:50:04', '2018-05-10 08:50:04'),
(14, 1, 'Hello', 'Testing time', 0, '2018-05-10 08:55:02', '2018-05-10 08:55:02'),
(15, 1, 'd', 'fgfgfgf', 0, '2018-05-10 09:04:24', '2018-05-10 09:04:24'),
(16, 1, 'f', 'fgdfgdfgdfgdfgdf', 0, '2018-05-10 09:16:35', '2018-05-10 09:16:35'),
(17, 1, 'd', 'ddfdfsdfsdf', 0, '2018-05-10 09:18:53', '2018-05-10 09:18:53'),
(18, 1, 'SDFSFDFSDFSDF', 'DSFSDFDSFDFDSFSDFDS', 0, '2018-05-10 09:19:52', '2018-05-10 09:19:52'),
(19, 1, 'DAADDA', 'SDDDDSSDFSDFSDFSD', 0, '2018-05-10 09:32:31', '2018-05-10 09:32:31'),
(20, 1, 'ffgdfgf', 'gfdgdfgfdggfdfg', 0, '2018-05-10 09:36:13', '2018-05-10 09:36:13'),
(21, 1, 'fffg', 'dfdfgdfddfgdfgdfg', 0, '2018-05-10 09:40:57', '2018-05-10 09:40:57'),
(22, 1, 'Hey', 'Hola !', 0, '2018-05-10 09:43:00', '2018-05-10 09:43:00'),
(23, 1, 'Buy & Sell Info', 'What is the flow of this website ?', 0, '2018-05-10 09:50:15', '2018-05-10 09:50:15'),
(24, 1, 'football', 'worldcup', 0, '2018-05-10 09:51:41', '2018-05-10 09:51:41'),
(25, 1, 'Hi', 'Hola sobhan', 0, '2018-05-10 09:53:19', '2018-05-10 09:53:19'),
(26, 1, 'frt', 'hola', 0, '2018-05-10 09:57:15', '2018-05-10 09:57:15'),
(27, 1, 'xfffgfdg', 'jolllllllllllllllllllllllllllllllllllllllllll', 0, '2018-05-10 09:59:15', '2018-05-10 09:59:15'),
(28, 1, 'ffgfgfgfg', 'sayan', 0, '2018-05-10 10:00:43', '2018-05-10 10:00:43'),
(29, 1, 'Hi Admin', 'What is your name ?', 0, '2018-05-10 10:02:38', '2018-05-10 10:02:38'),
(30, 1, 'Hello', 'Hi Manomit Da .\nHow are you ?', 0, '2018-05-10 10:09:57', '2018-05-10 10:09:57'),
(31, 1, 'Hi', 'Hello, How are you ?', 0, '2018-05-10 10:10:58', '2018-05-10 10:10:58'),
(32, 1, 'Hi', 'Hello Manomit Da.', 0, '2018-05-10 10:12:28', '2018-05-10 10:12:28'),
(33, 1, 'Hi', 'Hey Sayan .', 0, '2018-05-10 10:13:34', '2018-05-10 10:13:34'),
(34, 1, 'Hola', 'Hey Sayan', 0, '2018-05-10 10:14:34', '2018-05-10 10:14:34'),
(35, 1, 'm', 'kjmkkjkjkjkjkjkjkjkjkj', 0, '2018-05-10 10:15:35', '2018-05-10 10:15:35'),
(36, 1, 'Hi', 'JHJHHJ', 0, '2018-05-10 10:16:36', '2018-05-10 10:16:36'),
(37, 1, 'fgfhghg', 'ghgfhfghfghghfhfhfghfghfg', 0, '2018-05-10 10:17:27', '2018-05-10 10:17:27'),
(38, 1, 'ghghgh', 'hgfhfghfghghghghghghf', 0, '2018-05-10 10:18:48', '2018-05-10 10:18:48'),
(39, 1, 'zczczzxc', 'xcxzczxzxczxc', 0, '2018-05-10 10:20:07', '2018-05-10 10:20:07'),
(40, 1, 'ddfdf', 'dfdfdfdfdfdf', 0, '2018-05-10 10:21:30', '2018-05-10 10:21:30'),
(41, 1, 'f', 'fgfdggdfdfgdfgdf', 0, '2018-05-10 10:22:12', '2018-05-10 10:22:12'),
(42, 1, 'Yup', 'jsdsdhsjd', 0, '2018-05-10 10:28:12', '2018-05-10 10:28:12'),
(43, 1, 'xxzcxxzc', 'xzcxzcxzcx', 0, '2018-05-10 10:28:30', '2018-05-10 10:28:30'),
(44, 1, 'xcv', 'cvcxcxvcx', 0, '2018-05-10 10:29:46', '2018-05-10 10:29:46'),
(45, 1, 'Hello', 'bnsjdsjhdjshds', 0, '2018-05-10 10:31:10', '2018-05-10 10:31:10'),
(46, 1, 's', 'sda', 0, '2018-05-10 10:33:03', '2018-05-10 10:33:03'),
(47, 1, 'Testing', 'Hi Dear', 0, '2018-05-10 10:35:04', '2018-05-10 10:35:04'),
(48, 1, 'f', 'fgdfgdfgdf', 0, '2018-05-10 10:37:54', '2018-05-10 10:37:54'),
(49, 1, 'Testing', 'Uhhh Ahaa', 0, '2018-05-10 10:40:28', '2018-05-10 10:40:28'),
(50, 1, 'sf', 'ffdgfg', 0, '2018-05-10 10:41:21', '2018-05-10 10:41:21'),
(51, 1, 'd', 'sd', 0, '2018-05-10 10:42:27', '2018-05-10 10:42:27'),
(52, 1, 's', 'dsadsadas', 0, '2018-05-10 10:43:38', '2018-05-10 10:43:38'),
(53, 1, 'x', 'csdds', 0, '2018-05-10 10:44:45', '2018-05-10 10:44:45'),
(54, 1, 'g', 'gfhgfhfghfg', 0, '2018-05-10 10:46:16', '2018-05-10 10:46:16'),
(55, 1, 'd', 'dddfd', 0, '2018-05-10 10:46:53', '2018-05-10 10:46:53'),
(56, 1, 'gh', 'cg', 0, '2018-05-10 10:48:13', '2018-05-10 10:48:13'),
(57, 1, 'cfg', 'f', 0, '2018-05-10 10:54:31', '2018-05-10 10:54:31'),
(58, 1, 'dfdfsdf', 'sfsdfsdfsdfsdf', 0, '2018-05-10 11:07:30', '2018-05-10 11:07:30'),
(59, 1, 'sdfdsfxvxcv', 'xcvxcvxvxcv', 0, '2018-05-10 11:10:05', '2018-05-10 11:10:05'),
(60, 1, 'dxgdvxcvxcv', 'xcvxcvxcvxcv', 0, '2018-05-10 11:16:55', '2018-05-10 11:16:55'),
(61, 1, 'sdfcx', 'xcvxcvxcv', 0, '2018-05-10 11:20:51', '2018-05-10 11:20:51'),
(62, 1, 'dgffxvxvxcv', 'xvxcvxcvxcv', 0, '2018-05-10 11:25:34', '2018-05-10 11:25:34'),
(63, 1, 'dsfsdf', 'xzcxzczxc', 0, '2018-05-10 11:27:24', '2018-05-10 11:27:24'),
(64, 1, 'zsxczc', 'zczczxc', 0, '2018-05-10 11:29:25', '2018-05-10 11:29:25'),
(65, 1, 'sdsadsa', 'zczxczxczxc', 0, '2018-05-10 11:30:33', '2018-05-10 11:30:33'),
(66, 1, 'szdszczxc', 'zxczxczxc', 0, '2018-05-10 11:31:10', '2018-05-10 11:31:10'),
(67, 1, 'adsadzc', 'zczczczc', 0, '2018-05-10 11:35:51', '2018-05-10 11:35:51'),
(68, 1, 'dsfsfsdf', 'xvxvcxv', 0, '2018-05-10 11:36:12', '2018-05-10 11:36:12'),
(69, 1, 'sdfxdzvcxv', 'xcvxcvxcv', 0, '2018-05-10 11:38:50', '2018-05-10 11:38:50'),
(70, 1, 'zsfsfd', 'xzczczx', 0, '2018-05-10 11:39:46', '2018-05-10 11:39:46'),
(71, 1, 'dsfsdf', 'zvxxcvxcv', 0, '2018-05-10 11:41:28', '2018-05-10 11:41:28'),
(72, 1, 'Hi Admin.', 'Testing push notification with socket.', 0, '2018-05-10 12:33:33', '2018-05-10 12:33:33'),
(73, 1, 'Hello', 'How are you ?', 0, '2018-05-10 12:34:11', '2018-05-10 12:34:11'),
(74, 1, 'Hi', 'Sayan', 0, '2018-05-10 12:37:21', '2018-05-10 12:37:21'),
(75, 1, 'Testing time', 'Hey sayan .', 0, '2018-05-10 12:42:39', '2018-05-10 12:42:39'),
(76, 1, 'hola', 'hukkkhaaaaaaaaaaaaaaaaaaaaa', 0, '2018-05-10 12:46:05', '2018-05-10 12:46:05'),
(77, 1, 'Hi test again', 'how are you?', 0, '2018-05-10 12:46:42', '2018-05-10 12:46:42'),
(78, 1, 'ghghg', 'ghchghghghgfhg', 0, '2018-05-10 12:48:07', '2018-05-10 12:48:07'),
(79, 1, 'Hello', 'Only admin can show.', 0, '2018-05-10 12:53:29', '2018-05-10 12:53:29'),
(80, 1, 'sd', 'dsdfsdfsdfdsfsd', 0, '2018-05-10 12:54:58', '2018-05-10 12:54:58'),
(81, 1, 'ddf', 'dfdfdsffdssdfsd', 0, '2018-05-10 12:55:36', '2018-05-10 12:55:36'),
(82, 1, 'ddfdsfdsfdsf', 'dfdsfsd', 0, '2018-05-10 12:56:40', '2018-05-10 12:56:40'),
(83, 1, 'asdffdsfsdf', 'sdfsdfdfdsfdsfsdfsdfsd', 0, '2018-05-10 12:57:47', '2018-05-10 12:57:47'),
(84, 1, 'sa', 'as', 0, '2018-05-10 12:58:41', '2018-05-10 12:58:41'),
(85, 1, 'Hello', 'daffdsfdsfdssd', 0, '2018-05-10 12:59:37', '2018-05-10 12:59:37'),
(86, 1, 'ifggg', 'ghghghfghfgfg', 0, '2018-05-10 13:04:40', '2018-05-10 13:04:40'),
(87, 1, 'ghgfhfgh', 'ghfghfghfghfg', 0, '2018-05-10 13:05:18', '2018-05-10 13:05:18'),
(88, 1, 'xfff', 'sfffgffdf', 0, '2018-05-10 13:06:19', '2018-05-10 13:06:19'),
(89, 1, 'dsdds', 'dsfdsfdfsdfsd', 0, '2018-05-10 13:14:06', '2018-05-10 13:14:06'),
(90, 1, 'asdasdasd', 'dsdssdfdsdsdds', 0, '2018-05-10 13:16:57', '2018-05-10 13:16:57'),
(91, 1, 'dfdfds', 'dfsdfdfsdfd', 0, '2018-05-10 13:27:00', '2018-05-10 13:27:00'),
(92, 1, 'c', 'x', 0, '2018-05-10 13:27:53', '2018-05-10 13:27:53'),
(93, 1, 'xzxzcx', 'zcxzcxzcxczxc', 0, '2018-05-10 13:29:53', '2018-05-10 13:29:53'),
(94, 1, 'Testing', 'Hey Amir.', 0, '2018-05-11 05:31:28', '2018-05-11 05:31:28'),
(95, 1, 'Hi', 'Holaaaaaaaaa', 0, '2018-05-11 05:34:59', '2018-05-11 05:34:59'),
(96, 1, 'hilk', 'fgfghfghfgh', 0, '2018-05-11 05:39:36', '2018-05-11 05:39:36'),
(97, 1, 'dfdfd', 'fdfdsfsdfsdfsdfsd', 0, '2018-05-11 05:43:00', '2018-05-11 05:43:00'),
(98, 1, 'dsd', 'ddddfdsfdfdssdsd', 0, '2018-05-11 05:44:06', '2018-05-11 05:44:06'),
(99, 1, 'aasdsd', 'dasdasdasdsadasdsadas', 0, '2018-05-11 05:57:10', '2018-05-11 05:57:10'),
(100, 1, 'rdff', 'fdgfgffgdf', 0, '2018-05-11 06:14:19', '2018-05-11 06:14:19'),
(101, 1, 'ddssdsdf', 'dsdsds', 0, '2018-05-11 06:15:15', '2018-05-11 06:15:15'),
(102, 1, 'Hello admin', 'Testing notification.', 0, '2018-05-14 06:40:13', '2018-05-14 06:40:13'),
(103, 1, 'Test Subject Test', 'Test Subject Test DESC', 0, '2018-05-14 10:49:49', '2018-05-14 10:49:49'),
(104, 1, 'hI', 'Sayan', 0, '2018-05-17 11:19:38', '2018-05-17 11:19:38'),
(105, 1, 'd', 'dfdffddssdf', 0, '2018-05-17 11:25:55', '2018-05-17 11:25:55'),
(106, 1, 'xcxcxcv', 'dsdfsdfsdfsdfsdfsdf', 0, '2018-05-17 11:26:55', '2018-05-17 11:26:55'),
(107, 1, 'Hello Admin', 'How are you ?', 0, '2018-05-17 11:31:14', '2018-05-17 11:31:14'),
(108, 1, 'Hi', 'Test', 0, '2018-05-23 10:16:08', '2018-05-23 10:16:08'),
(109, 1, 'asssds', 'assadasdasdsadsd', 0, '2018-05-23 10:17:12', '2018-05-23 10:17:12'),
(110, 1, 'Test', 'Notification', 0, '2018-05-23 10:32:32', '2018-05-23 10:32:32'),
(111, 1, 'wow', 'hola', 0, '2018-05-23 10:37:36', '2018-05-23 10:37:36'),
(112, 1, 'dvdd', 'fsdsdfsdsdsdf', 0, '2018-05-23 10:45:06', '2018-05-23 10:45:06'),
(113, 1, 'gbgh', 'ghgfhfghfghfg', 0, '2018-05-23 10:48:51', '2018-05-23 10:48:51'),
(114, 1, 'fgchfgfh', 'hghfghfghferwtrrtertetertr', 0, '2018-05-23 10:49:24', '2018-05-23 10:49:24'),
(115, 1, 'fg', 'ghfhfghfghfghfgh', 0, '2018-05-23 10:50:04', '2018-05-23 10:50:04'),
(116, 1, 'Query about bitcoin', 'What is Bitcoin ?', 0, '2018-05-23 10:52:06', '2018-05-23 10:52:06'),
(117, 1, 'Hello', 'Wow', 0, '2018-05-23 10:58:23', '2018-05-23 10:58:23'),
(118, 1, 'hi', 'lol', 0, '2018-05-23 10:58:39', '2018-05-23 10:58:39'),
(119, 1, 'd', 'dsfsdfs', 0, '2018-05-23 10:59:16', '2018-05-23 10:59:16'),
(120, 1, 'l', 'jl', 0, '2018-05-23 11:11:45', '2018-05-23 11:11:45'),
(121, 1, 'gh', 'dfdfds', 0, '2018-05-23 11:17:16', '2018-05-23 11:17:16'),
(122, 1, 'cvcxvv', 'cxxcvxcvcxvc', 0, '2018-05-23 11:17:24', '2018-05-23 11:17:24'),
(123, 1, 'h', 'hgghjghj', 0, '2018-05-23 12:52:44', '2018-05-23 12:52:44'),
(124, 1, 'hello', 'help me', 0, '2018-07-11 05:55:12', '2018-07-11 05:55:12'),
(125, 1, 'hello', 'ddfsfd', 0, '2018-07-11 05:55:27', '2018-07-11 05:55:27'),
(126, 1, 'sasasd', 'asdasdasdasds', 0, '2018-07-11 05:57:16', '2018-07-11 05:57:16'),
(127, 1, 'sdsadsadasd', 'asdasdasdasdasdasdasdasdasdsa', 0, '2018-07-11 05:57:37', '2018-07-11 05:57:37'),
(128, 1, 'ssadasdas', 'dasdasdasdas', 0, '2018-07-11 05:59:27', '2018-07-11 05:59:27'),
(129, 1, 'd', 's', 0, '2018-07-11 06:00:06', '2018-07-11 06:00:06'),
(130, 1, 'dsfdfdsfds', 'fdfsdfsdf', 0, '2018-07-11 06:02:25', '2018-07-11 06:02:25'),
(131, 1, 'sdfsdfsdfsdfsd', 'fsdfsdfdsfsdf', 0, '2018-07-11 06:02:37', '2018-07-11 06:02:37'),
(132, 1, 'test', 'tesese', 0, '2018-08-03 10:24:57', '2018-08-03 10:24:57'),
(133, 1, 'test', 'tesese', 0, '2018-08-03 10:26:04', '2018-08-03 10:26:04'),
(134, 1, 'test', 'test', 0, '2018-08-03 10:27:01', '2018-08-03 10:27:01'),
(135, 1, 'Test again', 'gtgtgt', 0, '2018-08-03 10:40:07', '2018-08-03 10:40:07'),
(136, 1, 'wsdswd', 'deded', 0, '2018-08-03 10:51:00', '2018-08-03 10:51:00'),
(137, 1, 'wsdswd', 'deded', 0, '2018-08-03 10:51:56', '2018-08-03 10:51:56'),
(138, 1, 'wsdswd', 'deded', 0, '2018-08-03 10:51:58', '2018-08-03 10:51:58'),
(139, 1, 'xsxs', 'xdsdx', 0, '2018-08-03 10:53:18', '2018-08-03 10:53:18'),
(140, 1, 'xsxs', 'xdsdx', 0, '2018-08-03 10:53:32', '2018-08-03 10:53:32'),
(141, 1, 'rgrtg', 'fgftgrfg', 0, '2018-08-03 10:53:50', '2018-08-03 10:53:50'),
(142, 1, 'rgrtg', 'fgftgrfg', 0, '2018-08-03 10:54:03', '2018-08-03 10:54:03'),
(143, 1, 'kiki', 'uku', 0, '2018-08-03 10:54:36', '2018-08-03 10:54:36'),
(144, 1, 'Test again', 'tgtgtg', 0, '2018-08-03 10:55:55', '2018-08-03 10:55:55'),
(145, 1, 'sss', 'swsws', 0, '2018-08-03 10:56:31', '2018-08-03 10:56:31'),
(146, 1, 'ttt', 'tttttt', 0, '2018-08-03 10:59:09', '2018-08-03 10:59:09'),
(147, 1, 'ttt', 'tttttt', 0, '2018-08-03 10:59:29', '2018-08-03 10:59:29'),
(148, 1, 'test', 'fd', 0, '2018-08-03 10:59:38', '2018-08-03 10:59:38'),
(149, 1, 'Test again', 'zasasa', 0, '2018-08-03 11:01:58', '2018-08-03 11:01:58'),
(150, 1, 'Test again', 'ass', 0, '2018-08-03 11:02:35', '2018-08-03 11:02:35'),
(151, 1, 'grtgtg', 'gtgtg', 0, '2018-08-03 11:05:24', '2018-08-03 11:05:24'),
(152, 1, 'Test again test', 'Test again test', 0, '2018-08-03 11:07:44', '2018-08-03 11:07:44'),
(153, 1, 'Test again demo', 'Test again demo', 0, '2018-08-03 11:08:28', '2018-08-03 11:08:28'),
(154, 1, 'New Support', 'New Support', 0, '2018-08-03 11:09:59', '2018-08-03 11:09:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL COMMENT '1=''admin'',2=''user''',
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `referral_id` int(11) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `address` text,
  `contact_no` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `about_me` text,
  `image` varchar(255) DEFAULT NULL,
  `identity_proof` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `activation_key` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `investor_type` int(11) DEFAULT NULL COMMENT '1=Institutional Investor, 2=Individual Investor',
  `otp` int(11) DEFAULT NULL,
  `two_factorAuth_secret_key` varchar(255) DEFAULT NULL,
  `two_factorAuth_qr_code_image` text,
  `two_factorAuth_status` int(11) DEFAULT '2' COMMENT '1=''Enable'',2=''Disable''',
  `raw_password` varchar(255) DEFAULT NULL,
  `two_factorAuth_verified` varchar(15) DEFAULT NULL,
  `two_factorAuth_scan_verified` int(11) DEFAULT NULL COMMENT '1="scan",0="Not Scan"',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `type`, `first_name`, `last_name`, `password`, `email`, `referral_id`, `user_name`, `dob`, `address`, `contact_no`, `city`, `state`, `country_id`, `postal_code`, `about_me`, `image`, `identity_proof`, `status`, `activation_key`, `createdAt`, `updatedAt`, `notes`, `investor_type`, `otp`, `two_factorAuth_secret_key`, `two_factorAuth_qr_code_image`, `two_factorAuth_status`, `raw_password`, `two_factorAuth_verified`, `two_factorAuth_scan_verified`) VALUES
(1, '2', 'Coin', 'Jolt', '$2a$10$ASxEsCZErIIO6PPJva4RBupOJJIWkq2vX1oYAZqLOR5dDpVPUsUIu', 'nilesh@wrctpl.com', 16, 'nilesh12', '1992-04-30 00:00:00', 'Ramrajatalaaa', '1234567890', 'Kolkataaa', 'MB', 240, '700014', 'Test bio data of test', 'https://s3.amazonaws.com/coinjoltdev2018/profile/1-1539069836603.jpg', 'javascript:void(0)', 1, '0a87d89532795867224af01dfead1f94be', '0000-00-00 00:00:00', '2018-11-09 08:08:01', 'This is for saving quick notes', 2, NULL, '', '', 2, '55142612', 'Inactive', 0),
(14, '2', 'Manomit', 'Mitra', '$2a$10$95imv/fYg2gisp2VmP/I5OgNQ6SelE5aX/2Y8nu5qW48QPfcFKGYa', 'manomit@wrctpl.com', 1, 'manomit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', 1, '098fda9f2c786c50275be719e2ef5298bc42', '2018-04-18 10:49:37', '2018-06-25 11:33:03', NULL, NULL, NULL, '', '', 2, '2846856', NULL, 0),
(16, '2', 'Suman', 'Das', '$2a$10$zvczS0HyUSBOxovl7DeZ7Oto155JFScdNMmfUBsMDLcnBu1LKniha', 'suman@wrctpl.com', 0, 'suman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', 1, '108bc78473516f62335df401bce01396', '2018-04-18 10:59:18', '2018-06-25 11:33:03', NULL, NULL, NULL, '', '', 2, '40931040', NULL, 0),
(17, '2', NULL, NULL, '$2a$10$pckarNOH1JZjGkX6t/ffm.JUhlCySXdoZl2/q1E6f10oqTz.1Cvb6', 'koustabh@wrctpl.com', 16, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', 1, '108bc78472516f62335df401bce01396', '2018-04-18 11:01:20', '2018-06-25 11:33:03', NULL, NULL, NULL, '', '', 2, '17851211', NULL, 0),
(20, '1', 'Partho', 'Sen', '$2a$10$WoP0lLgLsFouc1/y0zY/QejKkZPMF.xILvBkyc/GtNZNrgsFCKJU2', 'admin@coinjolt.com', NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', 1, NULL, '0000-00-00 00:00:00', '2018-11-09 08:02:24', NULL, NULL, NULL, '', '', 2, NULL, 'Inactive', 0),
(50, '2', 'Tamashree', 'Dey', '$2a$10$KS3pPhznZWl/o8XxwzLS.u8HnIckkTTt4kZpor48jAYdrUxtD9gK2', 'tamashree@gmail.com', 0, NULL, NULL, 'Dumdum', '9999999999', 'Kolkata', 'HI', 226, '777777', NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', 1, '108fd99132796a753569f31ff1f70c97fd4c8bb9fft', '2018-05-08 12:40:00', '2018-06-25 11:33:04', NULL, 1, NULL, '', '', 2, '63122287', NULL, 0),
(51, '2', 'Amir', 'Sohel', '$2a$10$usnFOpjDzpDrpZeLRluwgutz2QYnFkfK2lbI/8kQaFKMlpcPskHzq', 'amir@wrctpl.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', NULL, NULL, '2018-05-11 12:16:37', '2018-06-25 11:33:04', NULL, NULL, NULL, '', '', 2, '27321934', NULL, 0),
(58, '2', 'tamashree', 'dey', '$2a$10$DvQm7rMbtq.U6aMwxhMP0O8R1/YAp2N1lR1/D1ps82TH9PMUVVAvO', 'tamashree@wrctpl.com', 0, 'tam', NULL, 'Dumdum', '9999999999', 'Kolkata', 'WB', 340, '777777', NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/58-1533888216719.jpg', 'javascript:void(0)', 1, '108fd99132796a753569f31ff1f70c97fd4c8bb9', '2018-05-21 12:33:21', '2018-11-09 07:05:12', NULL, 2, NULL, '', '', 2, '30511176', 'Inactive', 0),
(59, '2', NULL, NULL, '$2a$10$j7yGlxLw82d9hce0ApJ/7.hcug/8hEy7e1uWOmmprkI.YEv/YMvQS', 'nasir@wrctpl.com', 0, NULL, NULL, 'Dumdum', '987678999', 'Kolkata', 'WB', 15, '777777', NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', 1, '0a8fc79933516f62335df401bce01396', '2018-05-22 07:53:13', '2018-06-25 11:33:04', NULL, 1, NULL, '', '', 2, '14610165', NULL, 0),
(61, '2', 'Malini', NULL, '$2a$10$n51CgWjSO1xXf5mk1gs3E.u5J4GUFeZqKylDVUjoZOg6JMNMhNehW', 'malini@wrctpl.com', 0, NULL, NULL, 'Dumdum', '9890999999', 'Kolkata', 'dd', 253, '777777', NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', 1, '098fd8992f785867224af01dfead1f94be', '2018-05-30 10:54:50', '2018-06-25 11:33:05', NULL, 1, NULL, '', '', 2, '63353833', NULL, 0),
(62, '2', NULL, 'Paul', '$2a$10$cbe6qi/FQCgv4P/AlgtZ0OSJTLk8ypSygOp/IlfkVMqZ2gHJ6gQ9W', 'somenath@wrctpl.com', 0, NULL, NULL, '2e3we3e', '9876567899', 'Kolkata', 'WB', 254, '777777', NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', 1, '1781d9952f706c78105ef60ee6f310d5b04089', '2018-05-31 05:33:19', '2018-06-25 11:33:05', NULL, 1, NULL, '', '', 2, '14447839', NULL, 0),
(64, '2', NULL, NULL, '$2a$10$wyKqPV.jU4O4J9E2Nt6a7eKElAfK8afAXE1Mrnn738x7Df3rx1EgO', 'abc@gmail.com', 0, NULL, NULL, 'Dumdum', '9999999999', ' Kolkata', 'AL', 226, '777777', NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', NULL, '058cd7b0267c79793c07e702ff', '2018-07-10 13:22:02', '2018-07-10 13:22:02', NULL, NULL, NULL, '', '', 2, NULL, NULL, 0),
(65, '2', NULL, NULL, '$2a$10$We8KEpH3dihXa1bb6cGp.eVs2NfApUokc.L9.y9cRIVlwNXZAvUdi', 'abcde@gmail.com', 0, NULL, NULL, 'kolkata sec v', '9999999999', ' Kolkata', 'AL', 226, '777777', NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', NULL, '058cd79424517f7d3140e843f1ec11', '2018-07-12 05:38:25', '2018-07-12 05:38:25', NULL, NULL, NULL, '', '', 2, NULL, NULL, 0),
(73, '2', NULL, NULL, '$2a$10$4s0w5MshlV47v1Lg6rO2zOwEUCJ1lPFbdo7gVP0retRCXXHgGmqMy', 'sayan@mailinator.com', 0, NULL, NULL, 'salt lake', '1234567890', 'kolkata', 'AL', 226, '700049', NULL, 'undefinedprofile/nobody.jpg', 'javascript:void(0)', 1, '178fcd912f5175713945ed03f3f71389fd4c8bb9', '2018-08-08 06:08:29', '2018-08-08 06:09:37', NULL, 2, NULL, '', '', 2, NULL, NULL, 0),
(74, '2', NULL, NULL, '$2a$10$WKvpJ6G7D4/nYWXBNubYquZfeyxrI63S.bdgantrEvE/p.tj79sQO', 'kalyanchakraborty.kalyan91@gmail.com', 0, NULL, NULL, 'Ramrajatalaa', '9988774455', 'Kolkata', 'AL', 226, '711104', NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', NULL, '0f8fd889207f7b783142f60cf0ec0e8faa018fb5dcfedc6a2df875719388706fd05b9fce', '2018-08-09 07:09:43', '2018-08-09 07:09:43', NULL, 2, NULL, '', '', 2, NULL, NULL, 0),
(78, '2', NULL, NULL, '$2a$10$aoAnkEfbzTk7QZF9dnOHx.ro5FBx8MT2ksue5889W5GFz1r86cga.', 'sayan@wrctpl.com', 0, NULL, NULL, 'salt lake', '1234567890', 'kolkata', 'AL', 226, '700049', NULL, 'undefinedprofile/nobody.jpg', 'javascript:void(0)', 1, '178fcd912f516f62335df401bce01396', '2018-08-09 09:58:36', '2018-08-09 10:02:36', NULL, 2, NULL, 'HQYWWI3GFREFI6KFJR5UOPCOFRVW4UT3JF4E4ZK2M5GHEZZQ', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAAAklEQVR4AewaftIAAAehSURBVO3BQY4kRxLAQDLR//8yd45+CiBR1SMp1s3sD9a6xMNaF3lY6yIPa13kYa2LPKx1kYe1LvKw1kUe1rrIw1oXeVjrIg9rXeRhrYs8rHWRh7Uu8rDWRX74kMrfVDGpnFRMKlPFpDJVnKhMFZPKScU3qZxUvKHyN1V84mGtizysdZGHtS7yw5dVfJPKJ1TeqDhReaPiRGWqmFSmikllqphUTlSmipOKb1L5poe1LvKw1kUe1rrID79M5Y2Kb6qYVCaVqWJSOan4JpUTlaliUvmbVN6o+E0Pa13kYa2LPKx1kR/+4yreqHij4g2VNyomlaniROWk4v/Jw1oXeVjrIg9rXeSH/ziVqWJSmSomlaliUvlNKicqU8VUcaLy/+RhrYs8rHWRh7Uu8sMvq/hNFW+ovFExqUwVk8pJxYnKicpUMamcVHxTxb/Jw1oXeVjrIg9rXeSHL1P5m1SmipOKSeVEZaqYVKaKSeVEZaqYVKaKSWWqmFROVKaKE5V/s4e1LvKw1kUe1rqI/cFFVKaKSeWk4g2VqeJEZap4Q2WqmFQ+UfFf9rDWRR7WusjDWhf54UMqU8WJym+qeKNiUjmpeEPlROUTKicVJyqTylRxojJVTCpvVHziYa2LPKx1kYe1LmJ/8AGVT1R8k8onKv4mlaliUvlExaQyVUwqJxWTyknFpDJVfNPDWhd5WOsiD2tdxP7gF6mcVEwqJxWTyknFicobFScqJxUnKicVk8pJxTepvFFxojJVfOJhrYs8rHWRh7Uu8sOHVE4qJpVJ5aRiUjmpOFGZKiaVE5WpYqr4RMWk8kbFpDJVTCpTxRsVJyp/08NaF3lY6yIPa13khw9VnKi8UTGpTBUnKp+omFSmiknlExVvqLxR8YbKb6qYVL7pYa2LPKx1kYe1LmJ/8EUqU8U/SWWqOFF5o2JSOak4UXmj4kTlpOINlZOKN1Smik88rHWRh7Uu8rDWRX74kMobKicVk8pJxaTyhspUMalMFW9UTCqfqJhUTiomlUllqviEyhsV3/Sw1kUe1rrIw1oXsT/4gMpJxTepvFFxojJVnKh8ouINlTcqJpWpYlKZKj6hclLxmx7WusjDWhd5WOsiP3yoYlL5JpWpYlKZKr5JZaqYVE4qTlROKk5UTiomlaliUjmpmFTeUJkqvulhrYs8rHWRh7UuYn/wRSpTxT9JZar4JpU3KiaVT1RMKlPFpPJGxaQyVUwqU8Xf9LDWRR7WusjDWhf54UMqU8WkclIxqZxUTCpTxRsqJxWTylRxovJGxTepTBWTylRxUjGpnKhMFZPKVPGJh7Uu8rDWRR7WusgPv6xiUjmp+E0qJxUnFW9UnFScqEwVb1S8oTJVTConFZPKpDJVfNPDWhd5WOsiD2tdxP7gi1SmijdUTipOVKaKSeWNiknlpGJSmSq+SWWqmFROKiaVk4pJZaqYVE4qvulhrYs8rHWRh7Uu8sNfpvJGxaQyVZyovFFxUvFGxaQyVUwqU8VJxW+q+KaKSWWq+MTDWhd5WOsiD2td5IcvqzipmFSmikllqphUpopvUjmpmFROKiaVqWJS+S+rmFR+08NaF3lY6yIPa13khy9TmSomlaliUpkqJpUTlaliUpkqJpWp4kTlpGJSmSomlU9UTCpTxUnFJ1ROKiaVb3pY6yIPa13kYa2L/PAhlaliUjlRmSomlaliUpkqJpUTlROVqeINlU9UTCpTxaQyVUwqU8UnKv5NHta6yMNaF3lY6yI/fJnKN1VMKn9TxRsVJyqTylTxhspU8W+iMlVMFd/0sNZFHta6yMNaF/nhl1WcqEwqn1CZKiaVk4pPqEwVn1CZKiaVE5UTlTcqJpWpYqqYVE4qPvGw1kUe1rrIw1oX+eHLKiaVT1RMKp+oOFGZKiaVqWJSOamYVE4qJpWp4hMVJyqTylQxqZxU/KaHtS7ysNZFHta6yA//sIpJZVKZKn5TxaQyVXxTxYnKVHGi8obKJ1SmikllUjmp+MTDWhd5WOsiD2tdxP7gP0zlpOJE5aTiEypvVEwqU8WkclIxqZxUvKHyiYpveljrIg9rXeRhrYv88CGVv6niDZWpYqqYVN5QmSqmikllqphUTlQ+UTGpnKhMFScVk8pU8Zse1rrIw1oXeVjrIj98WcU3qbxRMal8k8qJyknFpDJVTCpvVJyovFHxhspUMalMFd/0sNZFHta6yMNaF/nhl6m8UfEJlaniEyonFW+oTBWTyknFiconVD5RcVLxmx7WusjDWhd5WOsiP1xOZaqYVKaKSWWqmFSmikllqjipOFE5qZhUTiomlZOKN1Smit/0sNZFHta6yMNaF/nhP65iUpkqJpUTlaliUvmEyjdVnFScqJxUTCqfUDmp+MTDWhd5WOsiD2td5IdfVvFvUjGpTBWTylQxqUwqU8WkclLxhspUMalMFVPFpHJS8YbK3/Sw1kUe1rrIw1oX+eHLVP4mlaniRGWqmFSmikllqphUTiomlTdUpoo3VE4q3lCZKk4qJpVveljrIg9rXeRhrYvYH6x1iYe1LvKw1kUe1rrIw1oXeVjrIg9rXeRhrYs8rHWRh7Uu8rDWRR7WusjDWhd5WOsiD2td5GGti/wPV7jsrdN8mkQAAAAASUVORK5CYII=', 1, NULL, 'Inactive', 1),
(90, '2', NULL, NULL, '$2a$10$ycDbZOUYA7MThcMkdS2g2uy0liz.jI/s5YZg6vVucVzJyjjnBYbl6', 'tamashree.dey@gmail.com', 0, NULL, NULL, 'Arabinda Road, Ramrajatala, Howrah 4', '9975684123', 'Howrah', 'West Bengal', 340, '711104', NULL, 'https://s3.amazonaws.com/coinjoltdev2018/profile/nobody.jpg', 'javascript:void(0)', 1, '108fd99132796a753507e008ebc31b96b24688fad3e8d0', '2018-09-04 10:35:08', '2018-09-04 10:38:01', NULL, 2, NULL, '', '', 2, NULL, 'Inactive', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE IF NOT EXISTS `wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `bitgo_wallet_id` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `userkeychain_public` text,
  `userkeychain_private` text,
  `backupkeychain_private` text,
  `backupkeychain_public` text,
  `bitgokeychain_public` text,
  `bitgokeychain_private` text,
  `currency_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wallet_addresses`
--

CREATE TABLE IF NOT EXISTS `wallet_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `wallet_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wallet_transactions`
--

CREATE TABLE IF NOT EXISTS `wallet_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `wallet_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1=Send, 2=MCP Deposit',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wiretransfers`
--

CREATE TABLE IF NOT EXISTS `wiretransfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `amount_usd` int(11) DEFAULT NULL,
  `status` int(5) DEFAULT NULL COMMENT '0=unapproved,1=approved,2=rejected',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `wiretransfers`
--

INSERT INTO `wiretransfers` (`id`, `user_id`, `amount_usd`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 1, 150, 1, '2018-04-19 10:30:15', '2018-04-23 07:27:18'),
(2, 1, 120, 2, '2018-04-19 10:32:31', '2018-04-19 10:32:31'),
(3, 17, 50, 1, '2018-04-19 13:04:30', '2018-04-23 07:23:14'),
(4, 1, 80, 1, '2018-04-19 13:31:17', '2018-04-23 07:24:29'),
(5, 1, 8000, 2, '2018-04-23 08:21:59', '2018-04-23 08:36:49'),
(6, 1, 100000, 1, '2018-04-23 10:10:57', '2018-04-23 10:11:15'),
(7, 1, 900000, 1, '2018-04-23 10:12:15', '2018-04-23 10:12:26'),
(8, 1, 900000, 1, '2018-04-23 10:13:18', '2018-04-23 10:13:36'),
(9, 1, 900000, 2, '2018-04-23 10:17:01', '2018-04-23 10:17:13'),
(10, 1, 9000000, 1, '2018-04-23 10:17:42', '2018-04-23 10:21:18'),
(11, 1, 400000, 1, '2018-04-23 10:21:05', '2018-04-24 09:02:53'),
(12, 1, 10, 2, '2018-04-23 12:36:01', '2018-04-24 09:02:43'),
(13, 1, 60000, 1, '2018-04-25 07:07:34', '2018-04-25 07:08:16'),
(14, 1, 100, 1, '2018-04-25 09:55:05', '2018-04-25 09:55:26'),
(15, 1, 2000, 2, '2018-04-30 06:23:42', '2018-05-02 09:38:21'),
(16, 1, 50, 1, '2018-05-02 09:36:16', '2018-05-03 06:42:32'),
(17, 1, 500, 0, '2018-05-02 10:53:40', '2018-05-03 05:37:46'),
(18, 1, 6000, 1, '2018-05-02 13:24:51', '2018-05-03 06:43:09'),
(19, 1, 123, 0, '2018-05-03 06:48:23', '2018-05-03 06:48:23'),
(20, 1, 100, 0, '2018-05-04 07:02:32', '2018-05-04 07:02:32'),
(21, 1, 10, 0, '2018-05-28 10:44:29', '2018-05-28 10:44:29'),
(22, 1, 666, 0, '2018-06-07 06:27:41', '2018-06-07 06:27:41'),
(23, 1, 2, 1, '2018-06-07 06:38:22', '2018-06-08 07:30:19'),
(24, 1, 2, 0, '2018-06-07 06:49:04', '2018-06-07 06:49:04'),
(25, 1, 2, 0, '2018-06-07 09:46:21', '2018-06-07 09:46:21'),
(26, 1, 5, 0, '2018-06-07 13:28:57', '2018-06-07 13:28:57'),
(27, 1, 2, 0, '2018-06-08 10:20:43', '2018-06-08 10:20:43'),
(28, 1, 2, 0, '2018-06-08 10:25:48', '2018-06-08 10:25:48'),
(29, 58, 3, 0, '2018-06-08 10:39:01', '2018-06-08 10:39:01'),
(30, 58, 2, 0, '2018-06-08 10:39:14', '2018-06-08 10:39:14'),
(31, 58, 2, 0, '2018-06-08 10:50:54', '2018-06-08 10:50:54'),
(32, 58, 3, 0, '2018-06-08 11:01:07', '2018-06-08 11:01:07'),
(33, 58, 2, 0, '2018-06-08 11:24:31', '2018-06-08 11:24:31'),
(34, 58, 2, 0, '2018-06-08 11:25:58', '2018-06-08 11:25:58'),
(35, 1, 2, 0, '2018-06-08 11:36:55', '2018-06-08 11:36:55'),
(36, 63, 500, 0, '2018-06-21 09:52:34', '2018-06-21 09:52:34'),
(37, 1, 2, 0, '2018-07-03 12:02:04', '2018-07-03 12:02:04'),
(38, 58, 2, 0, '2018-07-12 07:22:31', '2018-07-12 07:22:31'),
(39, 58, 3, 0, '2018-07-12 07:23:00', '2018-07-12 07:23:00'),
(40, 58, 3, 0, '2018-07-12 07:23:52', '2018-07-12 07:23:52'),
(42, 1, 100, 0, '2018-07-12 07:53:52', '2018-07-12 07:53:52'),
(53, 58, 20, 0, '2018-07-12 10:36:27', '2018-07-12 10:36:27'),
(54, 1, 2, 0, '2018-07-19 10:27:45', '2018-07-19 10:27:45'),
(55, 1, 1, 0, '2018-07-19 10:32:05', '2018-07-19 10:32:05'),
(56, 1, 2, 0, '2018-07-19 10:34:29', '2018-07-19 10:34:29'),
(57, 1, 1, 0, '2018-07-19 10:47:49', '2018-07-19 10:47:49'),
(58, 1, 1, 0, '2018-07-19 11:35:45', '2018-07-19 11:35:45'),
(59, 1, 1, 0, '2018-07-20 07:59:10', '2018-07-20 07:59:10'),
(60, 1, 1, 0, '2018-07-20 09:55:12', '2018-07-20 09:55:12'),
(61, 1, 2, 0, '2018-07-20 10:11:25', '2018-07-20 10:11:25'),
(62, 1, 100, 0, '2018-07-25 10:57:29', '2018-07-25 10:57:29'),
(63, 1, 11, 0, '2018-07-25 10:57:52', '2018-07-25 10:57:52'),
(64, 1, 11, 1, '2018-07-25 10:59:24', '2018-07-26 07:18:33'),
(65, 1, 11, 2, '2018-07-25 11:01:16', '2018-07-26 07:18:24'),
(66, 1, 21, 0, '2018-07-27 05:09:40', '2018-07-27 05:09:40'),
(67, 1, 500, 0, '2018-07-27 07:00:43', '2018-07-27 07:00:43'),
(68, 1, 500, 0, '2018-07-27 10:32:28', '2018-07-27 10:32:28'),
(69, 1, 500, 0, '2018-07-27 10:34:29', '2018-07-27 10:34:29'),
(70, 1, 500, 0, '2018-07-27 10:51:02', '2018-07-27 10:51:02'),
(71, 1, 1, 0, '2018-08-06 06:30:45', '2018-08-06 06:30:45'),
(72, 1, 11, 0, '2018-08-06 06:36:12', '2018-08-06 06:36:12'),
(73, 1, 11, 0, '2018-08-06 06:38:12', '2018-08-06 06:38:12'),
(74, 1, 1, 0, '2018-08-06 07:11:15', '2018-08-06 07:11:15'),
(75, 1, 11, 0, '2018-08-06 07:12:51', '2018-08-06 07:12:51'),
(76, 1, 21, 0, '2018-08-06 07:14:45', '2018-08-06 07:14:45'),
(77, 1, 11, 0, '2018-08-06 07:17:15', '2018-08-06 07:17:15'),
(78, 1, 11, 0, '2018-08-06 07:19:15', '2018-08-06 07:19:15'),
(79, 1, 21, 0, '2018-08-06 07:21:56', '2018-08-06 07:21:56'),
(80, 1, 11, 0, '2018-08-06 07:39:54', '2018-08-06 07:39:54'),
(81, 1, 11, 0, '2018-08-06 07:41:54', '2018-08-06 07:41:54'),
(82, 1, 1, 0, '2018-08-06 07:44:16', '2018-08-06 07:44:16'),
(83, 1, 5, 0, '2018-08-06 07:45:39', '2018-08-06 07:45:39'),
(84, 1, 5, 0, '2018-08-06 07:46:56', '2018-08-06 07:46:56'),
(85, 1, 5, 0, '2018-08-06 07:48:56', '2018-08-06 07:48:56'),
(86, 1, 31, 0, '2018-08-06 07:54:38', '2018-08-06 07:54:38'),
(87, 1, 31, 0, '2018-08-06 07:56:38', '2018-08-06 07:56:38'),
(88, 1, 3, 0, '2018-08-06 08:32:30', '2018-08-06 08:32:30'),
(89, 1, 3, 0, '2018-08-06 08:34:30', '2018-08-06 08:34:30'),
(90, 1, 2, 0, '2018-08-06 08:36:01', '2018-08-06 08:36:01'),
(91, 1, 2, 0, '2018-08-06 08:38:01', '2018-08-06 08:38:01'),
(92, 1, 31, 0, '2018-08-06 08:51:49', '2018-08-06 08:51:49'),
(93, 1, 2, 0, '2018-08-06 08:55:08', '2018-08-06 08:55:08'),
(94, 1, 11, 0, '2018-08-06 08:58:35', '2018-08-06 08:58:35'),
(95, 1, 11, 0, '2018-08-06 09:00:39', '2018-08-06 09:00:39'),
(96, 1, 11, 0, '2018-08-06 09:02:39', '2018-08-06 09:02:39'),
(97, 1, 21, 0, '2018-08-06 09:04:58', '2018-08-06 09:04:58'),
(98, 1, 3, 0, '2018-08-06 09:07:24', '2018-08-06 09:07:24'),
(99, 1, 2, 0, '2018-08-06 09:10:54', '2018-08-06 09:10:54'),
(100, 1, 41, 0, '2018-08-06 09:12:26', '2018-08-06 09:12:26'),
(101, 1, 22, 0, '2018-08-06 09:14:48', '2018-08-06 09:14:48'),
(102, 1, 5, 0, '2018-08-06 09:20:04', '2018-08-06 09:20:04'),
(103, 1, 7, 0, '2018-08-06 09:22:58', '2018-08-06 09:22:58'),
(104, 1, 3, 0, '2018-08-06 09:29:14', '2018-08-06 09:29:14'),
(105, 1, 4, 0, '2018-08-06 09:39:11', '2018-08-06 09:39:11'),
(106, 1, 4, 0, '2018-08-06 09:41:11', '2018-08-06 09:41:11'),
(107, 1, 21, 0, '2018-08-06 09:45:50', '2018-08-06 09:45:50'),
(108, 1, 1, 0, '2018-08-06 09:47:08', '2018-08-06 09:47:08'),
(109, 1, 21, 0, '2018-08-06 09:49:24', '2018-08-06 09:49:24'),
(110, 1, 21, 0, '2018-08-06 09:51:24', '2018-08-06 09:51:24'),
(111, 1, 1, 0, '2018-08-06 10:08:50', '2018-08-06 10:08:50'),
(112, 1, 6, 0, '2018-08-06 10:10:27', '2018-08-06 10:10:27'),
(113, 1, 7, 0, '2018-08-06 10:13:23', '2018-08-06 10:13:23'),
(114, 1, 8, 0, '2018-08-06 10:19:11', '2018-08-06 10:19:11'),
(115, 1, 11, 0, '2018-08-06 10:22:42', '2018-08-06 10:22:42'),
(116, 1, 9, 0, '2018-08-06 10:24:20', '2018-08-06 10:24:20'),
(117, 1, 9, 0, '2018-08-06 10:26:21', '2018-08-06 10:26:21'),
(118, 1, 10, 0, '2018-08-06 10:39:28', '2018-08-06 10:39:28'),
(119, 1, 13, 0, '2018-08-06 10:41:34', '2018-08-06 10:41:34'),
(120, 1, 1, 0, '2018-08-06 10:42:08', '2018-08-06 10:42:08'),
(121, 1, 4, 0, '2018-08-06 10:43:51', '2018-08-06 10:43:51'),
(122, 1, 14, 0, '2018-08-06 10:46:43', '2018-08-06 10:46:43'),
(123, 1, 31, 0, '2018-08-06 10:59:09', '2018-08-06 10:59:09'),
(124, 1, 21, 0, '2018-08-06 11:00:09', '2018-08-06 11:00:09'),
(125, 1, 4, 0, '2018-08-06 11:03:52', '2018-08-06 11:03:52'),
(126, 1, 1, 0, '2018-08-06 11:04:54', '2018-08-06 11:04:54'),
(127, 1, 6, 0, '2018-08-06 11:06:35', '2018-08-06 11:06:35'),
(128, 1, 2, 0, '2018-08-06 11:08:20', '2018-08-06 11:08:20'),
(129, 1, 2, 0, '2018-08-06 11:10:20', '2018-08-06 11:10:20'),
(130, 1, 4, 0, '2018-08-06 11:12:50', '2018-08-06 11:12:50'),
(131, 1, 7, 0, '2018-08-06 11:13:43', '2018-08-06 11:13:43'),
(132, 1, 3, 0, '2018-08-06 11:15:37', '2018-08-06 11:15:37'),
(133, 1, 3, 0, '2018-08-06 11:17:37', '2018-08-06 11:17:37'),
(134, 1, 41, 0, '2018-08-06 11:18:50', '2018-08-06 11:18:50'),
(135, 1, 5, 0, '2018-08-06 11:23:52', '2018-08-06 11:23:52'),
(136, 1, 4, 0, '2018-08-06 11:26:30', '2018-08-06 11:26:30'),
(137, 1, 61, 0, '2018-08-06 11:27:33', '2018-08-06 11:27:33'),
(138, 1, 61, 0, '2018-08-06 11:29:33', '2018-08-06 11:29:33'),
(139, 1, 7, 0, '2018-08-06 11:31:14', '2018-08-06 11:31:14'),
(140, 1, 61, 0, '2018-08-06 11:33:04', '2018-08-06 11:33:04'),
(141, 1, 6, 0, '2018-08-06 11:40:06', '2018-08-06 11:40:06'),
(142, 1, 2, 0, '2018-08-06 11:44:44', '2018-08-06 11:44:44'),
(143, 1, 1, 0, '2018-08-06 11:46:11', '2018-08-06 11:46:11'),
(144, 68, 22, 0, '2018-08-06 12:39:19', '2018-08-06 12:39:19'),
(145, 68, 2, 0, '2018-08-06 12:48:31', '2018-08-06 12:48:31'),
(146, 68, 2, 0, '2018-08-06 12:49:17', '2018-08-06 12:49:17'),
(147, 68, 2, 0, '2018-08-06 12:50:47', '2018-08-06 12:50:47'),
(148, 68, 2, 0, '2018-08-06 12:51:17', '2018-08-06 12:51:17'),
(149, 1, 2, 0, '2018-08-06 13:59:38', '2018-08-06 13:59:38'),
(150, 1, 2, 0, '2018-08-07 05:10:09', '2018-08-07 05:10:09'),
(151, 1, 4, 0, '2018-08-07 05:15:38', '2018-08-07 05:15:38'),
(152, 1, 4, 0, '2018-08-07 05:20:56', '2018-08-07 05:20:56'),
(153, 1, 5, 0, '2018-08-07 05:22:44', '2018-08-07 05:22:44'),
(154, 1, 500, 0, '2018-08-07 05:37:10', '2018-08-07 05:37:10'),
(155, 1, 500, 0, '2018-08-07 05:39:57', '2018-08-07 05:39:57'),
(156, 1, 100, 0, '2018-08-07 05:42:02', '2018-08-07 05:42:02'),
(157, 1, 3, 0, '2018-08-07 05:45:04', '2018-08-07 05:45:04'),
(158, 1, 4, 0, '2018-08-07 05:46:06', '2018-08-07 05:46:06'),
(159, 1, 4, 0, '2018-08-07 05:48:06', '2018-08-07 05:48:06'),
(160, 1, 6, 0, '2018-08-07 05:48:41', '2018-08-07 05:48:41'),
(161, 1, 100, 0, '2018-08-07 05:51:05', '2018-08-07 05:51:05'),
(162, 1, 7, 0, '2018-08-07 05:51:53', '2018-08-07 05:51:53'),
(163, 1, 8, 0, '2018-08-07 05:52:42', '2018-08-07 05:52:42'),
(164, 1, 2, 0, '2018-08-07 05:53:42', '2018-08-07 05:53:42'),
(165, 1, 100, 0, '2018-08-07 05:54:09', '2018-08-07 05:54:09'),
(166, 1, 80, 0, '2018-08-07 05:55:20', '2018-08-07 05:55:20'),
(167, 1, 500, 0, '2018-08-07 05:56:27', '2018-08-07 05:56:27'),
(168, 1, 100, 0, '2018-08-07 05:58:47', '2018-08-07 05:58:47'),
(169, 1, 80, 0, '2018-08-07 06:00:06', '2018-08-07 06:00:06'),
(170, 1, 500, 0, '2018-08-07 06:01:31', '2018-08-07 06:01:31'),
(171, 1, 120, 0, '2018-08-07 06:02:46', '2018-08-07 06:02:46'),
(172, 1, 89, 0, '2018-08-07 06:04:44', '2018-08-07 06:04:44'),
(173, 1, 500, 0, '2018-08-07 06:05:49', '2018-08-07 06:05:49'),
(174, 1, 100, 0, '2018-08-07 06:07:00', '2018-08-07 06:07:00'),
(175, 1, 7, 0, '2018-08-07 06:07:27', '2018-08-07 06:07:27'),
(176, 1, 4, 0, '2018-08-07 06:07:53', '2018-08-07 06:07:53'),
(177, 1, 500, 0, '2018-08-07 06:08:28', '2018-08-07 06:08:28'),
(178, 1, 100, 0, '2018-08-07 06:12:19', '2018-08-07 06:12:19'),
(179, 1, 9, 0, '2018-08-07 06:15:13', '2018-08-07 06:15:13'),
(180, 1, 500, 0, '2018-08-07 06:20:07', '2018-08-07 06:20:07'),
(181, 1, 500, 0, '2018-08-07 06:24:04', '2018-08-07 06:24:04'),
(182, 1, 6, 0, '2018-08-07 06:27:06', '2018-08-07 06:27:06'),
(183, 1, 11, 0, '2018-08-07 06:28:42', '2018-08-07 06:28:42'),
(184, 1, 3, 0, '2018-08-07 06:30:32', '2018-08-07 06:30:32'),
(185, 1, 500, 0, '2018-08-07 06:30:45', '2018-08-07 06:30:45'),
(186, 1, 6, 0, '2018-08-07 06:31:41', '2018-08-07 06:31:41'),
(187, 1, 500, 0, '2018-08-07 06:34:42', '2018-08-07 06:34:42'),
(188, 1, 500, 0, '2018-08-07 06:35:43', '2018-08-07 06:35:43'),
(189, 1, 100, 0, '2018-08-07 06:37:05', '2018-08-07 06:37:05'),
(190, 1, 100, 0, '2018-08-07 06:45:55', '2018-08-07 06:45:55'),
(191, 1, 100, 0, '2018-08-07 06:47:30', '2018-08-07 06:47:30'),
(192, 1, 100, 0, '2018-08-07 06:48:53', '2018-08-07 06:48:53'),
(193, 1, 100, 0, '2018-08-07 06:50:31', '2018-08-07 06:50:31'),
(194, 1, 500, 0, '2018-08-07 06:59:12', '2018-08-07 06:59:12'),
(195, 1, 89, 0, '2018-08-07 07:06:17', '2018-08-07 07:06:17'),
(196, 1, 2, 0, '2018-08-07 08:50:46', '2018-08-07 08:50:46'),
(197, 1, 5, 0, '2018-08-07 08:52:58', '2018-08-07 08:52:58'),
(198, 58, 500, 1, '2018-08-07 09:52:05', '2018-08-07 09:52:36'),
(199, 1, 31, 0, '2018-08-07 10:16:30', '2018-08-07 10:16:30'),
(200, 1, 4, 0, '2018-08-07 10:18:00', '2018-08-07 10:18:00'),
(201, 1, 7, 0, '2018-08-07 10:23:32', '2018-08-07 10:23:32'),
(202, 1, 7, 0, '2018-08-07 10:25:32', '2018-08-07 10:25:32'),
(203, 1, 4, 0, '2018-08-07 10:38:05', '2018-08-07 10:38:05'),
(204, 1, 5, 0, '2018-08-07 10:41:20', '2018-08-07 10:41:20'),
(205, 1, 5, 0, '2018-08-07 10:43:20', '2018-08-07 10:43:20'),
(206, 1, 8, 0, '2018-08-07 10:45:09', '2018-08-07 10:45:09'),
(207, 1, 5, 0, '2018-08-07 10:46:17', '2018-08-07 10:46:17'),
(208, 1, 5, 0, '2018-08-07 10:48:17', '2018-08-07 10:48:17'),
(209, 1, 6, 0, '2018-08-07 10:50:59', '2018-08-07 10:50:59'),
(210, 1, 6, 0, '2018-08-07 10:52:59', '2018-08-07 10:52:59'),
(211, 1, 8, 0, '2018-08-07 10:57:15', '2018-08-07 10:57:15'),
(212, 1, 8, 0, '2018-08-07 10:59:15', '2018-08-07 10:59:15'),
(213, 1, 8, 0, '2018-08-07 11:11:27', '2018-08-07 11:11:27'),
(214, 1, 8, 0, '2018-08-07 11:13:27', '2018-08-07 11:13:27'),
(215, 1, 3, 0, '2018-08-07 11:22:46', '2018-08-07 11:22:46'),
(216, 1, 6, 0, '2018-08-07 11:26:44', '2018-08-07 11:26:44'),
(217, 1, 99, 0, '2018-08-07 11:29:19', '2018-08-07 11:29:19'),
(218, 1, 99, 0, '2018-08-07 11:31:19', '2018-08-07 11:31:19'),
(219, 1, 6, 0, '2018-08-07 11:38:15', '2018-08-07 11:38:15'),
(220, 1, 5, 0, '2018-08-07 11:40:39', '2018-08-07 11:40:39'),
(221, 1, 10, 0, '2018-08-07 11:42:48', '2018-08-07 11:42:48'),
(222, 1, 20, 0, '2018-08-07 11:45:56', '2018-08-07 11:45:56'),
(223, 1, 10, 0, '2018-08-07 11:48:00', '2018-08-07 11:48:00'),
(224, 1, 100, 0, '2018-08-07 11:50:27', '2018-08-07 11:50:27'),
(225, 1, 100, 0, '2018-08-07 11:52:49', '2018-08-07 11:52:49'),
(226, 1, 96, 0, '2018-08-07 11:57:59', '2018-08-07 11:57:59'),
(227, 1, 96, 0, '2018-08-07 11:59:59', '2018-08-07 11:59:59'),
(228, 1, 87, 0, '2018-08-07 12:01:02', '2018-08-07 12:01:02'),
(229, 1, 69, 0, '2018-08-07 12:04:28', '2018-08-07 12:04:28'),
(230, 1, 31, 0, '2018-08-07 12:05:48', '2018-08-07 12:05:48'),
(231, 1, 31, 0, '2018-08-07 12:07:48', '2018-08-07 12:07:48'),
(232, 1, 17, 0, '2018-08-07 12:10:16', '2018-08-07 12:10:16'),
(233, 1, 10, 0, '2018-08-07 12:19:01', '2018-08-07 12:19:01'),
(234, 1, 12, 0, '2018-08-07 12:20:03', '2018-08-07 12:20:03'),
(235, 1, 13, 0, '2018-08-07 12:20:27', '2018-08-07 12:20:27'),
(236, 1, 10, 0, '2018-08-07 12:21:01', '2018-08-07 12:21:01'),
(237, 1, 13, 0, '2018-08-07 12:21:43', '2018-08-07 12:21:43'),
(238, 1, 13, 0, '2018-08-07 12:26:20', '2018-08-07 12:26:20'),
(239, 1, 9, 0, '2018-08-07 12:28:17', '2018-08-07 12:28:17'),
(240, 1, 9, 0, '2018-08-07 12:28:33', '2018-08-07 12:28:33'),
(241, 1, 3, 0, '2018-08-07 12:29:42', '2018-08-07 12:29:42'),
(242, 1, 49, 0, '2018-08-07 12:36:47', '2018-08-07 12:36:47'),
(243, 1, 49, 0, '2018-08-07 12:38:47', '2018-08-07 12:38:47'),
(244, 1, 20, 0, '2018-08-07 12:40:25', '2018-08-07 12:40:25'),
(245, 1, 22, 0, '2018-08-07 12:41:38', '2018-08-07 12:41:38'),
(246, 1, 20, 0, '2018-08-07 12:48:21', '2018-08-07 12:48:21'),
(247, 1, 33, 0, '2018-08-07 12:50:52', '2018-08-07 12:50:52'),
(248, 1, 30, 0, '2018-08-07 12:52:56', '2018-08-07 12:52:56'),
(249, 1, 60, 0, '2018-08-07 12:55:35', '2018-08-07 12:55:35'),
(250, 1, 1, 0, '2018-09-05 06:00:27', '2018-09-05 06:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `withdraws`
--

CREATE TABLE IF NOT EXISTS `withdraws` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `amount_usd` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0=unapproved,1=approved,2=rejected',
  `withdraw_type` int(11) DEFAULT NULL COMMENT '1=''bank wire transfer'',2=''check by mail'',3=''MCP Withdraw''',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `withdraws`
--

INSERT INTO `withdraws` (`id`, `user_id`, `amount_usd`, `status`, `withdraw_type`, `createdAt`, `updatedAt`) VALUES
(1, 1, 150, 1, 2, '2018-04-25 13:42:36', '2018-05-03 07:25:33'),
(3, 1, 80, 1, 1, '2018-05-02 06:08:27', '2018-05-02 13:42:51'),
(4, 1, 453, 1, 1, '2018-05-02 13:46:04', '2018-05-03 07:26:38'),
(5, 1, 1000, 1, 3, '2018-05-03 06:08:49', '2018-05-03 07:27:08'),
(33, 1, 450, 1, 1, '2018-05-28 09:15:53', '2018-06-08 07:31:11'),
(46, 1, 6000, 1, 1, '2018-05-28 10:24:26', '2018-06-08 07:30:33'),
(55, 1, 2000, 1, 3, '2018-07-03 12:01:10', '2018-07-06 11:10:39'),
(56, 1, 2000, 2, 1, '2018-07-03 12:04:21', '2018-07-04 10:59:28'),
(57, 1, 3000, 1, 2, '2018-07-03 12:04:37', '2018-07-04 10:58:19'),
(58, 1, 100, 1, 1, '2018-07-25 11:17:03', '2018-07-26 07:25:32'),
(59, 1, 100, 2, 2, '2018-07-25 11:17:22', '2018-07-26 07:25:18'),
(60, 1, 100, 2, 2, '2018-08-14 09:37:52', '2018-08-14 09:38:41'),
(61, 1, 2000, 2, 1, '2018-11-09 07:03:54', '2018-11-09 07:19:55'),
(62, 1, 3000, 1, 2, '2018-11-09 07:11:08', '2018-11-09 07:21:14'),
(64, 1, 6000, 1, 2, '2018-11-09 07:44:24', '2018-11-09 08:18:53'),
(65, 1, 2000, 2, 3, '2018-11-09 07:44:57', '2018-11-09 08:00:18');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
